# DO NOT MODIFY THIS FILE.
#
# When you submit your project, an alternate version of this file will be used
# to test your code against the sample Raven's problems in this zip file, as
# well as other problems from the Raven's Test and former students.
#
# Any modifications to this file will not be used when grading your project.
# If you have any questions, please email the TAs.
#
#

import re
from RavensAttribute import RavensAttribute
from RavensFigure import RavensFigure
from RavensObject import RavensObject
from RavensProblem import RavensProblem

# A list of RavensProblems within one set.
#
# Your agent does not need to use this class explicitly.


class ProblemSet:
    # Initializes a new ProblemSet with the given name, an empty set of
    # problems, and a new random number generator.
    #
    # Your agent does not need to use this method.
    #
    # @param name The name of the problem set.
    def __init__(self, name):
        self.name = name
        self.problems = []

    # Returns the name of the problem set.
    #
    # Your agent does not need to use this method.
    #
    # @return the name of the problem set as a String
    def getName(self):
        return self.name

    # Returns an ArrayList of the RavensProblems in this problem set.
    #
    # Your agent does not need to use this method.
    #
    # @return the RavensProblems in this set as an ArrayList.
    def getProblems(self):
        return self.problems

    # Adds a new problem to the problem set, read from an external file.
    #
    # Your agent does not need to use this method.
    #
    # @param problem the File containing the new problem.
    def addProblem(self, problem):
        name = get_next_line(problem)
        problem_type = get_next_line(problem)
        current_answer = get_next_line(problem)
        answer = current_answer

        figures = []
        current_figure = None
        current_object = None

        line = get_next_line(problem)
        while not line == "":
            if not line.startswith("\t"):
                new_figure = RavensFigure(line)
                figures.append(new_figure)
                current_figure = new_figure
            elif not line.startswith("\t\t"):
                line = line.replace("\t", "")
                new_object = RavensObject(line)
                current_figure.getObjects().append(new_object)
                current_object = new_object
            elif line.startswith("\t\t"):
                line = line.replace("\t", "")
                split = re.split(":", line)
                new_attribute = RavensAttribute(split[0], split[1])
                current_object.getAttributes().append(new_attribute)
            line = get_next_line(problem)
        new_problem = RavensProblem(name, problem_type, answer)
        for figure in figures:
            new_problem.getFigures()[figure.getName()] = figure
        self.problems.append(new_problem)


# noinspection PyBroadException
def try_parse_int(i):
    try:
        int(i)
        return True
    except:
        return False


def get_next_line(r):
    return r.readline().rstrip()