My contribution should consist of these files:
Agent.py
Common.py
Generator.py
Tester.py
README.txt (this file)
Design report.pdf
AppendixA.pdf
AppendixB.pdf

These depends of the following standard libraries:
copy
timeit
itertools

In a console not supporting Escape codes, the printout will look a bit weird.
In a console supporting this, the printouts will have color-coding.
This will not affect the Result.txt as all

The code runs slow on 2x2 Basic Problem 17 (~8s) and 2x2 Basic Problem 19 (~25s).
Total runtime I experienced to be ~30s

WARNING: This code does not complete "2x1 Challenge Problem 05" in a reasonable time.
In the current implementation it needs hours to run, and fails after trying.
For the agent to complete, this must be left out. My code tries to ignore this test, but looks for the hardcoded name.




The code works for python 2 as well, but the library enum34 needs to be imported first:
pip install enum34