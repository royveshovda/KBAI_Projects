import timeit
from Common import convert, UseRequirements, Strategy, is_2x1_strategy, is_2x2_strategy, get_slot_name_shape
from Generator import calculate_transformations_2x1, calculate_transformations_2x2
from Tester import test_2x1, test_2x2


class Agent:
    def __init__(self):
        self._debug_mode = False

    def set_debug_mode(self):
        self._debug_mode = True

    # noinspection PyMethodMayBeStatic,PyBroadException,PyPep8Naming
    def Solve(self, problem):
        try:
            result = run(problem, self._debug_mode)
        except Exception:
            print("Something went wrong running the agent")
        return result


# noinspection PyListCreation
def run(problem, debug_mode):
    start = timeit.default_timer()

    if not debug_mode:
        print("")
        print("Trying to solve: {0}".format(problem.getName()))

        # Not able to solve this one yet
        to_skip = ["2x1 Challenge Problem 05"]
        if problem.getName() in to_skip:
            print("Skipping")
            return "0"
    else:
        # Use while developing to limit the code running
        solving = []
        # 2x1
        # solving.append("2x1 Challenge Problem 02")  # FAILS
        # solving.append("2x1 Challenge Problem 03")  # FAILS
        # solving.append("2x1 Challenge Problem 04")  # FAILS
        # solving.append("2x1 Challenge Problem 05")  # FAILS

        if problem.getName() not in solving:
            return "0"
        else:
            print("")
            print("Trying to solve: {0}".format(problem.getName()))

    fig_a = convert(problem.figures['A'])
    fig_b = convert(problem.figures['B'])
    fig_c = convert(problem.figures['C'])
    figures = (convert(problem.figures['1']),
               convert(problem.figures['2']),
               convert(problem.figures['3']),
               convert(problem.figures['4']),
               convert(problem.figures['5']),
               convert(problem.figures['6']))

    problem_type = problem.getProblemType()

    solutions = run_strategies(fig_a, fig_b, fig_c, figures, problem_type)

    stop = timeit.default_timer()
    runtime = stop - start
    print("Runtime: \033[0;34m{0}s\033[00m".format(runtime))

    if len(solutions) == 0:
        # TODO: Log: No answer found
        correct = problem.checkAnswer("0")
        print("\033[0;31mFound NONE answers -- Should be {0}\033[00m".format(correct))
        return "0"
    elif len(solutions) == 1:
        (_, answer) = solutions[0]
        correct = problem.checkAnswer(answer)
        if correct == answer:
            print("\033[0;32mFound correct solution: {0}\033[00m".format(answer))
        else:
            print("\033[0;31mFound solution: {0}, but should be: {1}\033[00m".format(answer, correct))
        return answer
    else:
        ans = ""
        for (_, item) in solutions:
            ans += " " + item
        correct = problem.checkAnswer("0")
        print("\033[0;31mFound more than one answer ( {0} ) -- Should be {1}\033[00m".format(ans, correct))
        return "0"


def run_strategies(fig_a, fig_b, fig_c, figures, problem_type):
    # Pick different strategies
    # First try using requirements. If not finding any, loosen up

    solutions = []
    strategies = []
    if problem_type == "2x1":
        strategies = strategies + [Strategy.S2X1_FIXED_POSITIONS,
                                   Strategy.S2X1_USE_REQUIREMENTS,
                                   Strategy.S2X1_USE_SIZE_REQUIREMENTS]

        strategies = strategies + [Strategy.S2X1_NORMAL,
                                   Strategy.S2X1_STICK_FILL,
                                   Strategy.S2X1_IGNORE_POSITIONAL]
    elif problem_type == "2x2":
        strategies = strategies + [Strategy.S2X2_FIXED_POSITIONS,
                                   Strategy.S2X2_USE_REQUIREMENTS,
                                   Strategy.S2X2_USE_SIZE_REQUIREMENTS]
        strategies = strategies + [Strategy.S2X2_NORMAL,
                                   Strategy.S2X2_STICK_FILL,
                                   Strategy.S2X2_IGNORE_POSITIONAL]

    for s in strategies:
        solutions = run_strategy(fig_a, fig_b, fig_c, figures, s)
        if len(solutions) > 0:
            return solutions
    return solutions


def is_same_shape_in_all_figures(figures):
    shape = figures[0].objects()[0].get_slot_filler(get_slot_name_shape())
    for f in figures:
        for o in f.objects():
            if o.get_slot_filler(get_slot_name_shape()) != shape:
                return False
    return True


def run_strategy(fig_a, fig_b, fig_c, figures, strategy):
    solutions = []
    results = []
    if strategy == Strategy.S2X1_FIXED_POSITIONS:
        transformations = calculate_transformations_2x1(fig_a, fig_b, fix_positions=True)
        results = test_2x1(fig_c, transformations, figures,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=True,
                           stick_fill=False)
    elif strategy == Strategy.S2X1_USE_REQUIREMENTS:
        transformations = calculate_transformations_2x1(fig_a, fig_b, fix_positions=False)
        results = test_2x1(fig_c, transformations, figures,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X1_USE_SIZE_REQUIREMENTS:
        transformations = calculate_transformations_2x1(fig_a, fig_b, fix_positions=False)
        results = test_2x1(fig_c, transformations, figures,
                           use_requirements=UseRequirements.SIZE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X1_NORMAL:
        transformations = calculate_transformations_2x1(fig_a, fig_b, fix_positions=False)
        results = test_2x1(fig_c, transformations, figures,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X1_IGNORE_POSITIONAL:
        transformations = calculate_transformations_2x1(fig_a, fig_b, fix_positions=False)
        results = test_2x1(fig_c, transformations, figures,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=True,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X1_STICK_FILL:
        transformations = calculate_transformations_2x1(fig_a, fig_b, fix_positions=False, fix_shape=True)
        results = test_2x1(fig_c, transformations, figures,
                           use_requirements=UseRequirements.SHAPE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=True)
    elif strategy == Strategy.S2X2_FIXED_POSITIONS:
        transformations = calculate_transformations_2x2(fig_a, fig_b, fig_c, fix_positions=True)
        results = test_2x2(fig_b, fig_c, transformations, figures,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=True,
                           stick_fill=False)
    elif strategy == Strategy.S2X2_USE_REQUIREMENTS:
        transformations = calculate_transformations_2x2(fig_a, fig_b, fig_c, fix_positions=False)
        results = test_2x2(fig_b, fig_c, transformations, figures,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X2_USE_SIZE_REQUIREMENTS:
        transformations = calculate_transformations_2x2(fig_a, fig_b, fig_c, fix_positions=False)
        results = test_2x2(fig_b, fig_c, transformations, figures,
                           use_requirements=UseRequirements.SIZE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X2_NORMAL:
        transformations = calculate_transformations_2x2(fig_a, fig_b, fig_c, fix_positions=False)
        results = test_2x2(fig_b, fig_c, transformations, figures,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X2_IGNORE_POSITIONAL:
        transformations = calculate_transformations_2x2(fig_a, fig_b, fig_c, fix_positions=False)
        results = test_2x2(fig_b, fig_c, transformations, figures,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=True,
                           fix_positions=False,
                           stick_fill=False)
    elif strategy == Strategy.S2X2_STICK_FILL:
        transformations = calculate_transformations_2x2(fig_a, fig_b, fig_c, fix_positions=False, fix_shape=True)
        results = test_2x2(fig_b, fig_c, transformations, figures,
                           use_requirements=UseRequirements.SHAPE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=True)

    if len(results) >= 1:
        if all_the_same(results):
            solutions = [results[0]]
        else:
            solutions = order_results(results, strategy)

    return solutions


def find_best(list_of_transformations):
    max_value = 0
    for t in list_of_transformations:
        w = calculate_weight(t)
        if w > max_value:
            max_value = w
    return max


def all_the_same(solutions):
    (_, potential) = solutions[0]
    for (_, solution) in solutions:
        if solution != potential:
            return False
    return True


def order_results(results, strategy):
    if is_2x1_strategy(strategy):
        return order_results_2x1(results)
    elif is_2x2_strategy(strategy):
        return order_results_2x2(results)
    else:
        return []


def order_results_2x2(results):
    weighted = []
    for ((a_to_b, a_to_c), r) in results:
        w1 = calculate_weight(a_to_b)
        w2 = calculate_weight(a_to_c)
        w = w1 + w2
        weighted.append(((a_to_b, a_to_c), r, w))

    max_value = 0
    for (transformation, r, w) in weighted:
        if w > max_value:
            max_value = w

    best = []
    for (transformation, r, w) in weighted:
        if w == max_value:
            best.append((transformation, r))

    if all_the_same(best):
        return [best[0]]
    else:
        return best


def order_results_2x1(results):
    weighted = []
    for (transformation, r) in results:
        w = calculate_weight(transformation)
        weighted.append((transformation, r, w))

    max_value = 0
    for (transformation, r, w) in weighted:
        if w > max_value:
            max_value = w

    best = []
    for (transformation, r, w) in weighted:
        if w == max_value:
            best.append((transformation, r))

    if all_the_same(best):
        return [best[0]]
    else:
        return best


def calculate_weight(transformation):
    total = 0
    for t in transformation.object_transformations():
        if t.add():
            total += 0
        elif t.is_shape_change():
            total += 0
        elif t.delete():
            total += 1
        elif t.is_size_change():
            total += 2
        elif t.is_rotation():
            total += 3
        elif t.is_reflection():
            total += 4
        else:  # Unchanged
            total += 5

    return total