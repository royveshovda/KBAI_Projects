import cv2
import numpy as np

img = cv2.imread('Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem07/A.png', 0)
size = np.size(img)
skel = np.zeros(img.shape, np.uint8)

# ret, img = cv2.threshold(img, 127, 255, 0)
ret, img = cv2.threshold(img, thresh=127, maxval=255, type=cv2.THRESH_BINARY_INV)


cv2.imshow("img", img)
cv2.waitKey(0)

element = cv2.getStructuringElement(cv2.MORPH_CROSS, (2, 2))
#element = cv2.getStructuringElement(cv2.MORPH_CROSS, (3, 3))
done = False


while( not done):
    eroded = cv2.erode(img, element)
    temp = cv2.dilate(eroded, element)
    temp = cv2.subtract(img, temp)
    skel = cv2.bitwise_or(skel, temp)
    img = eroded.copy()

    cv2.imshow("img", img)
    cv2.waitKey(0)

    zeros = size - cv2.countNonZero(img)
    if zeros == size:
        done = True

cv2.imshow("skel", skel)
cv2.waitKey(0)
cv2.destroyAllWindows()