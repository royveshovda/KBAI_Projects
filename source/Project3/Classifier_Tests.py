import Classifier
from Common import Slot, Object, Figure


def ok():
    print("\033[0;31m")


def failed():
    print("\033[0;31mFAILED\033[00m")

number_of_failures = 0


def test_figure(test_path, test_text):
    objects = Classifier.visual_to_textual(test_path)
    converted_objects = []
    for obj_name in objects.keys():
        features = objects[obj_name]
        slots = []
        for k in features.keys():
            val = features[k]
            s = Slot(k, val)
            slots.append(s)
        o = Object(obj_name, slots)
        converted_objects.append(o)

    figure = Figure(test_text, converted_objects)
    Classifier.calculate_size_levels([figure])
    Classifier.remove_extra_info([figure])
    return figure


def get_object(figure, name):
    for obj in figure.objects():
        if obj.name() == name:
            return obj

    return None


def test_2x1_01():
    global number_of_failures
    try:
        test_text = "2x1 BP 01 - A: Circle - no fill"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem01/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        z = get_object(figure, "Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 01 - B: Circle - fill"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem01/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 01 - C: Square - no fill"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem01/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 01 - 5: Square - fill"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem01/5.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 01 - 2: Triangle - no fill"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem01/2.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 01 - 4: Triangle - fill"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem01/4.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_02():
    global number_of_failures

    try:
        test_text = "2x1 BP 02 - 1: small triangle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem02/1.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 02 - 2: Large triangle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem02/2.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 02 - A: small circle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem02/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 02 - 5: Large circle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem02/5.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_04():
    global number_of_failures

    try:
        test_text = "2x1 BP 04 - A: Circle inside"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem04/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 04 - C: Square inside"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem04/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if y.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_06():
    global number_of_failures

    try:
        test_text = "2x1 BP 06 - A: Two plus"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem06/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "plus":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 6:
            raise Exception("Wrong number of slots for Z")
        if y.get_slot_filler("shape") != "plus":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_13():
    global number_of_failures

    try:
        test_text = "2x1 BP 13 - A: Triangle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem13/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "90":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 13 - B: Triangle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem13/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "270":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_14():
    global number_of_failures

    try:
        test_text = "2x1 BP 14 - A: Circle left-of square"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem14/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 14 - B: Circle left-of triangle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem14/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 14 - 1: Circle above circle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem14/1.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 14 - C: Square above circle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem14/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_15():
    global number_of_failures

    try:
        test_text = "2x1 BP 15 - C: Three triangles"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem15/C.png"
        figure = test_figure(test_path, test_text)

        z = get_object(figure, "Z")
        if len(figure.objects()) != 5:
            raise Exception("Wrong number of objects")

        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_17():
    global number_of_failures

    try:
        test_text = "2x1 BP 17 - C: Two rectangles"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem17/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "rectangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 6:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "rectangle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 17 - 1: Two rectangles with angle"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem17/1.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "rectangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "315":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 6:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "rectangle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "45":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 17 - B: Two plus"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem17/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "plus":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "45":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 6:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "plus":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "45":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 17 - A: Two plus"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem17/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "plus":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 6:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "plus":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_20():
    global number_of_failures

    try:
        test_text = "2x1 BP 20 - C: Three shapes inside (without fill)"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem20/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "270":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("inside") != "Y,Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 20 - 5: Three shapes inside (with fill)"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem20/5.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "90":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("inside") != "Y,Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 20 - B: Three shapes inside (with fill)"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem20/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "180":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("inside") != "Y,Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 BP 20 - A: Three shapes inside (without fill)"
        test_path = "Problems (Image Data)/2x1 Basic Problems/2x1BasicProblem20/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("inside") != "Y,Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_01():
    global number_of_failures

    try:
        test_text = "2x2 BP 01 - B: Pac-man (135)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem01/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "135":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 01 - 4: Pac-man (0)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem01/4.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 01 - A: Pac-man (45)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem01/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "45":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 01 - C: Pac-man (315)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem01/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "315":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 01 - 5: Pac-man (225)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem01/5.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "225":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 01 - 6: Pac-man (180)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem01/6.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "Pac-Man":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "180":
            raise Exception("Angle not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_07():
    global number_of_failures

    try:
        test_text = "2x2 BP 07 - B: Small circle inside square"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem07/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_11():
    global number_of_failures

    try:
        test_text = "2x2 BP 11 - B: Small plus"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem11/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "plus":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_13():
    global number_of_failures

    try:
        test_text = "2x2 BP 13 - C: Circle left-of circle"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem13/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 2:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_14():
    global number_of_failures

    try:
        test_text = "2x2 BP 14 - B: Three small shapes (1)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem14/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("left-of") != "Y,Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 14 - A: Three small shapes (2)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem14/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "circle":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("left-of") != "Y,Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_17():
    global number_of_failures

    try:
        test_text = "2x2 BP 17 - B: Many squares - with some fill (5)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem17/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 5:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if x.get_slot_filler("inside") != "Y,Z":
            raise Exception("Inside not as expected")

        w = get_object(figure, "W")
        if len(w.slots()) != 5:
            raise Exception("Wrong number of slots for W")
        if w.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if w.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if w.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if w.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if w.get_slot_filler("inside") != "X,Y,Z":
            raise Exception("Inside not as expected")

        v = get_object(figure, "V")
        if len(v.slots()) != 5:
            raise Exception("Wrong number of slots for V")
        if v.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if v.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if v.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if v.get_slot_filler("size") != "tiny":
            raise Exception("Size not as expected")
        if v.get_slot_filler("inside") != "W,X,Y,Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 17 - C: Many squares (3)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem17/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("inside") != "Y,Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 17 - A: Many squares (5)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem17/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 5:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-large":
            raise Exception("Size not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
        if y.get_slot_filler("inside") != "Z":
            raise Exception("Inside not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 5:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "medium":
            raise Exception("Size not as expected")
        if x.get_slot_filler("inside") != "Y,Z":
            raise Exception("Inside not as expected")

        w = get_object(figure, "W")
        if len(w.slots()) != 5:
            raise Exception("Wrong number of slots for W")
        if w.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if w.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if w.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if w.get_slot_filler("size") != "small":
            raise Exception("Size not as expected")
        if w.get_slot_filler("inside") != "X,Y,Z":
            raise Exception("Inside not as expected")

        v = get_object(figure, "V")
        if len(v.slots()) != 5:
            raise Exception("Wrong number of slots for V")
        if v.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if v.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if v.get_slot_filler("shape") != "square":
            raise Exception("Shape not as expected")
        if v.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if v.get_slot_filler("inside") != "W,X,Y,Z":
            raise Exception("Inside not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_19():
    global number_of_failures

    try:
        test_text = "2x2 BP 19 - 6: Four triangles"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem19/6.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 4:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "180":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "180":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("left-of") != "Z,W":
            raise Exception("Left-of not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 6:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "yes":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "180":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("left-of") != "Z,W":
            raise Exception("Left-of not as expected")
        if x.get_slot_filler("above") != "Y,Z":
            raise Exception("Above not as expected")

        w = get_object(figure, "W")
        if len(w.slots()) != 5:
            raise Exception("Wrong number of slots for W")
        if w.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if w.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if w.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if w.get_slot_filler("angle") != "180":
            raise Exception("Angle not as expected")
        if w.get_slot_filler("above") != "Y,Z":
            raise Exception("Above not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x2_20():
    global number_of_failures

    try:
        test_text = "2x2 BP 20 - A: Three triangles"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem20/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 4:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("left-of") != "X,Z":
            raise Exception("Left-of not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 6:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("above") != "Y,Z":
            raise Exception("Above not as expected")
        if x.get_slot_filler("left-of") != "Z":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x2 BP 20 - B: Three triangles (90 deg)"
        test_path = "Problems (Image Data)/2x2 Basic Problems/2x2BasicProblem20/B.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 3:
            raise Exception("Wrong number of objects")

        z = get_object(figure, "Z")
        if len(z.slots()) != 5:
            raise Exception("Wrong number of slots for Z")
        if z.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("angle") != "90":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("left-of") != "Y":
            raise Exception("Left-of not as expected")

        y = get_object(figure, "Y")
        if len(y.slots()) != 5:
            raise Exception("Wrong number of slots for Y")
        if y.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if y.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if y.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if y.get_slot_filler("angle") != "90":
            raise Exception("Angle not as expected")
        if y.get_slot_filler("above") != "Z":
            raise Exception("Above not as expected")

        x = get_object(figure, "X")
        if len(x.slots()) != 6:
            raise Exception("Wrong number of slots for X")
        if x.get_slot_filler("shape") != "triangle":
            raise Exception("Shape not as expected")
        if x.get_slot_filler("size") != "very-small":
            raise Exception("Size not as expected")
        if x.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if x.get_slot_filler("angle") != "90":
            raise Exception("Angle not as expected")
        if x.get_slot_filler("above") != "Y,Z":
            raise Exception("Above not as expected")
        if x.get_slot_filler("left-of") != "Y":
            raise Exception("Left-of not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


def test_2x1_challenge_01():
    global number_of_failures

    try:
        test_text = "2x1 CHP 03 - A: Pentagon"
        test_path = "Problems (Image Data)/2x1 Challenge Problems/2x1ChallengeProblem01/3.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "pentagon":
            raise Exception("Shape not as expected")
        # if z.get_slot_filler("angle") != "0":
        #     raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 CHP 01 - C: Hexagon"
        test_path = "Problems (Image Data)/2x1 Challenge Problems/2x1ChallengeProblem01/C.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("shape") != "hexagon":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1

    try:
        test_text = "2x1 CHP 01 - A: Octagon"
        test_path = "Problems (Image Data)/2x1 Challenge Problems/2x1ChallengeProblem01/A.png"
        figure = test_figure(test_path, test_text)

        if len(figure.objects()) != 1:
            raise Exception("Wrong number of objects")
        
        z = get_object(figure, "Z")
        if z.get_slot_filler("angle") != "0":
            raise Exception("Angle not as expected")
        if z.get_slot_filler("fill") != "no":
            raise Exception("Fill not as expected")
        if z.get_slot_filler("shape") != "octagon":
            raise Exception("Shape not as expected")
        if z.get_slot_filler("size") != "large":
            raise Exception("Size not as expected")
    except Exception as err:
        print("\nTesting: " + test_text)
        print(str(err))
        failed()
        number_of_failures += 1


test_2x1_01()  # OK
test_2x1_02()  # OK
test_2x1_04()  # OK
test_2x1_06()  # OK
test_2x1_13()  # OK
test_2x1_14()  # OK
test_2x1_15()  # OK
test_2x1_17()  # OK
test_2x1_20()

test_2x2_01()  # OK
test_2x2_07()  # OK
test_2x2_11()  # OK
test_2x2_13()  # OK
test_2x2_14()  # OK
# test_2x2_17()  # FAILS - does not create fill correctly to objects inside
test_2x2_19()  # OK
test_2x2_20()  # OK

test_2x1_challenge_01()  # OK





print("\n\nNumber of failures: " + str(number_of_failures))
