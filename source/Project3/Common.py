import itertools
import copy
from enum import Enum


def get_all_shapes_with_sides():
    shapes = [("triangle", 3),
              ("square", 4),
              ("pentagon", 5),
              ("hexagon", 6),
              ("heptagon", 7),
              ("octagon", 8),
              ("enneagon", 9),
              ("decagon", 10)]
    return shapes


def get_shape_for_number_of_sides(sides):
    shapes = get_all_shapes_with_sides()
    for shape, side in shapes:
        if side == sides:
            return shape
    return ""


def get_number_of_sides_for_shape(shape_name):
    shapes = get_all_shapes_with_sides()
    for shape, side in shapes:
        if shape == shape_name:
            return side
    return 0


def get_all_shapes_with_sides_only_names():
    shapes = get_all_shapes_with_sides()
    only_names = []
    for s, _ in shapes:
        only_names.append(s)
    return only_names


def replace_shape_type(obj):
    for a in obj.slots():
        if a.name() == get_slot_name_shape():
            shape = a.filler()
            sides = get_number_of_sides_for_shape(shape)
            a.new_filler("side-object:{0}".format(sides))


def get_common_figure_structure(figure_a, figure_b):
    if len(figure_a.objects()) != len(figure_b.objects()):
        return []

    structure_a = get_positional_figure(figure_a)
    structure_b = get_positional_figure(figure_b)

    if not compare_figures(structure_a, structure_b, ignore_positional_slots=False):
        return []

    order_a = []
    order_b = []

    for idx_a, o_a in enumerate(structure_a.objects()):
        for idx_b, o_b in enumerate(structure_b.objects()):
            if is_same_positional_objects(o_a, o_b):
                order_a.append(idx_a)
                order_b.append(idx_b)

    if len(order_a) != len(figure_a.objects()):
        return []

    if len(order_b) != len(figure_b.objects()):
        return []

    return [(order_a, order_b)]


def get_common_shape_structure(figure_a, figure_b):
    if len(figure_a.objects()) != len(figure_b.objects()):
        return []

    order_a = []
    order_b = []

    for idx_a, o_a in enumerate(figure_a.objects()):
        for idx_b, o_b in enumerate(figure_b.objects()):
            if is_same_size_and_shape_objects(o_a, o_b):
                order_a.append(idx_a)
                order_b.append(idx_b)

    if len(order_a) != len(figure_a.objects()):
        return []

    if len(order_b) != len(figure_b.objects()):
        return []

    return [(order_a, order_b)]


def get_common_figure_size_and_shape(figure_a, figure_b):
    order_a = []
    order_b = []

    for idx_a, o_a in enumerate(figure_a.objects()):
        for idx_b, o_b in enumerate(figure_b.objects()):
            if is_same_size_and_shape_objects(o_a, o_b):
                order_a.append(idx_a)
                order_b.append(idx_b)

    if len(order_a) != len(figure_a.objects()):
        return []

    if len(order_b) != len(figure_b.objects()):
        return []

    return [(order_a, order_b)]


def is_same_positional_objects(object_a, object_b):
    if len(object_a.slots()) != len(object_b.slots()):
        return False

    for a_a in object_a.slots():
        found = False
        for a_b in object_b.slots():
            if a_a.name() == a_b.name():
                if len(a_a.filler().split(",")) == len(a_b.filler().split(",")):
                    found = True
        if not found:
            return False

    return True


def is_same_size_and_shape_objects(object_a, object_b):

    shape_a = object_a.get_slot_filler(get_slot_name_shape())
    if len(shape_a) > 0:
        shape_b = object_b.get_slot_filler(get_slot_name_shape())
        if shape_a != shape_b:
            return False

    size_a = object_a.get_slot_filler(get_slot_name_size())
    if len(size_a) > 0:
        size_b = object_b.get_slot_filler(get_slot_name_size())
        if size_a != size_b:
            return False

    return True


def get_positional_figure(figure):
    f = copy.deepcopy(figure)
    pos_slot_names = get_positional_slot_names()
    for o in f.objects():
        pos_slots = []
        for a in o.slots():
            if a.name() in pos_slot_names:
                pos_slots.append(a)
        o.set_new_slots(pos_slots)
    return f


def generate_combinations(a, b):
    a_s = list(range(a))
    b_s = list(range(b))

    while len(a_s) < len(b_s):
        a_s.append(False)

    while len(a_s) > len(b_s):
        b_s.append(False)

    b_lists = list(itertools.permutations(b_s))

    for y in b_lists:
        yield (a_s, list(y))


def generate_figure_combinations(figure_a, figure_b):
    len_a = len(figure_a.objects())
    len_b = len(figure_b.objects())
    return generate_combinations(len_a, len_b)


def get_slot_name_shape():
    return "shape"


def get_slot_name_angle():
    return "angle"


def get_slot_name_size():
    return "size"


def get_slot_name_fill():
    return "fill"


def get_slot_filler_circle():
    return "circle"


def get_slot_filler_square():
    return "square"


def get_positional_slot_names():
    slot_names = ["above", "inside", "overlaps", "left-of"]
    return slot_names


def is_positional_slot(slot_name):
    return slot_name in get_positional_slot_names()


def find_shape(obj):
    return find_attribute_value(obj, get_slot_name_shape())


def find_fill(obj):
    fill = find_attribute_value(obj, get_slot_name_fill())
    if "," in fill:
        values = fill.split(",")
        sorted_list = sorted(values, key=str.lower)
        string = ""
        for s in sorted_list:
            if len(string) > 0:
                string += ","
            string += s
        fill = string

    return fill


def find_size(obj):
    return find_attribute_value(obj, get_slot_name_size())


def find_attribute_value(obj, attribute_name):
    for a in obj.slots():
        if a.name() == attribute_name:
            return a.filler()
    return ""


def generate_object_names(prefix=""):
    base = "OBJECT_"
    if len(prefix) > 0:
        base = prefix + "_"
    counter = 0
    while True:
        counter += 1
        yield base + str(counter)


def rename_object_in_object_transformation(object_transformation_list, from_name, to_name):
    for o in object_transformation_list:
        if o.name() == from_name:
            o.change_name(to_name)

        for a_t in o.slot_transformations():
            if from_name in a_t.filler_from():
                new_value = str.replace(a_t.filler_from(), from_name, to_name)
                a_t.set_new_filler_from(new_value)

            if from_name in a_t.filler_to():
                new_value = str.replace(a_t.filler_to(), from_name, to_name)
                a_t.set_new_filler_to(new_value)

        for a in o.to_add():
            if from_name in a.filler_from():
                new_value = str.replace(a.filler_from(), from_name, to_name)
                a.set_new_filler_from(new_value)

            if from_name in a.filler_to():
                new_value = str.replace(a.filler_to(), from_name, to_name)
                a.set_new_filler_to(new_value)

        for a_r in o.required():
            if from_name in a_r.filler():
                new_value = str.replace(a_r.filler(), from_name, to_name)
                a_r.new_filler(new_value)


def rename_object_in_figure(figure, from_name, to_name):
    rename_object_reference_in_object_list(figure.objects(), from_name, to_name)


def rename_object_reference_in_object_list(objects, from_name, to_name):
    for obj in objects:
        if obj is not False:
            if obj.name() == from_name:
                obj.new_name(to_name)
            for attribute in obj.slots():
                rename_object_reference_in_attribute(attribute, from_name, to_name)


def rename_object_reference_in_attribute(attribute, from_name, to_name):
    if from_name in attribute.filler():
        new_value = rename_reference_in_value(attribute.filler(), from_name, to_name)
        attribute.new_filler(new_value)


def rename_reference_in_value(value, from_name, to_name):
    if "," in value:
        new_value = []
        old_value = value.split(",")
        for v in old_value:
            if v == from_name:
                new_value.append(to_name)
            else:
                new_value.append(v)
        string = ""
        for s in new_value:
            if len(string) > 0:
                string += ","
            string += s
        value = string
    else:
        if value == from_name:
            value = to_name

    return value


def generate_permutations(c):
    c_s = list(range(c))
    c_lists = list(itertools.permutations(c_s))
    return [list(x) for x in c_lists]


def convert_attributes_to_dictionary(attributes):
    d = {}
    for attribute in attributes:
        d[attribute.name()] = attribute.filler()
    return d


def get_objects_in_order(objects, order):
    ordered_objects = []
    for o in order:
        if o is not False:
            ordered_objects.append(objects[o])
        else:
            ordered_objects.append(False)
    return ordered_objects


def convert(raven_figure):
    name = raven_figure.getName()
    objects = raven_figure.getObjects()
    converted_objects = []
    for obj in objects:
        o = convert_object(obj)
        converted_objects.append(o)
    return Figure(name, converted_objects)


def convert_object(raven_object):
    name = raven_object.getName()
    attributes = raven_object.getAttributes()
    converted_attributes = []
    for attr in attributes:
        a = convert_attribute(attr)
        converted_attributes.append(a)

    return Object(name, converted_attributes)


def convert_attribute(attribute):
    name = attribute.getName()
    value = attribute.getValue()
    return Slot(name, value)


def compare_figures(figure_a, figure_b, ignore_positional_slots):
    a_s = get_all_compatible_figures(figure_a)
    b_s = get_all_compatible_figures(figure_b)
    for a in a_s:
        for b in b_s:
            if compare_figures_core(a, b, ignore_positional_slots):
                return True

    return False


def get_all_compatible_figures(figure):
    figures = [figure]
    if len(figure.objects()) == 1:
        obj = figure.objects()[0]
        if obj.get_slot_filler(get_slot_name_shape()) == get_slot_filler_square():
            if obj.get_slot_filler(get_slot_name_fill()) == "yes":
                size = obj.get_slot_filler(get_slot_name_size())
                slots = get_slots_to_duplicate(obj)
                filled_squares = generate_all_filled_squares(size, slots)
                for f in filled_squares:
                    figures.append(f)
    return figures


def get_slots_to_duplicate(obj):
    slots = []
    for slot in obj.slots():
        if slot.name() != get_slot_name_shape() \
                and slot.name() != get_slot_name_size() \
                and slot.name() != get_slot_name_fill():
            slots.append(slot)
    return slots


def generate_all_filled_squares(size, slots):
    if size == "very-large":
        return generate_all_filled_squares_for_very_large(slots)
    elif size == "large":
        return generate_all_filled_squares_for_large(slots)
    elif size == "medium":
        return generate_all_filled_squares_for_medium(slots)
    elif size == "small":
        return generate_all_filled_squares_for_small(slots)
    else:
        return []


def generate_all_filled_squares_for_very_large(slots):
    figures = []
    s_square = Slot(get_slot_name_shape(), get_slot_filler_square())
    s_fill = Slot(get_slot_name_fill(), "yes")
    s_size_very_large = Slot(get_slot_name_size(), "very-large")
    s_size_large = Slot(get_slot_name_size(), "large")
    s_size_medium = Slot(get_slot_name_size(), "medium")
    s_size_small = Slot(get_slot_name_size(), "small")
    s_size_very_small = Slot(get_slot_name_size(), "very-small")

    f1_o1 = Object("1", [s_square, s_fill, s_size_very_large] + slots)
    f1_o2 = Object("2", [s_square, s_fill, s_size_large, Slot("inside", "1")] + slots)
    f1 = Figure("GENERATED", [f1_o1, f1_o2])
    figures.append(f1)

    f2_o1 = Object("1", [s_square, s_fill, s_size_very_large] + slots)
    f2_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f2 = Figure("GENERATED", [f2_o1, f2_o2])
    figures.append(f2)

    f3_o1 = Object("1", [s_square, s_fill, s_size_very_large] + slots)
    f3_o2 = Object("2", [s_square, s_fill, s_size_small, Slot("inside", "1")] + slots)
    f3 = Figure("GENERATED", [f3_o1, f3_o2])
    figures.append(f3)

    f4_o1 = Object("1", [s_square, s_fill, s_size_very_large] + slots)
    f4_o2 = Object("2", [s_square, s_fill, s_size_very_small, Slot("inside", "1")] + slots)
    f4 = Figure("GENERATED", [f4_o1, f4_o2])
    figures.append(f4)

    f5_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f5_o2 = Object("2", [s_square, s_fill, s_size_large, Slot("inside", "1")] + slots)
    f5_o3 = Object("3", [s_square, s_fill, s_size_medium, Slot("inside", "1,2")] + slots)
    f5 = Figure("GENERATED", [f5_o1, f5_o2, f5_o3])
    figures.append(f5)

    f6_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f6_o2 = Object("2", [s_square, s_fill, s_size_large, Slot("inside", "1")] + slots)
    f6_o3 = Object("3", [s_square, s_fill, s_size_small, Slot("inside", "1,2")] + slots)
    f6 = Figure("GENERATED", [f6_o1, f6_o2, f6_o3])
    figures.append(f6)

    f7_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f7_o2 = Object("2", [s_square, s_fill, s_size_large, Slot("inside", "1")] + slots)
    f7_o3 = Object("3", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2")] + slots)
    f7 = Figure("GENERATED", [f7_o1, f7_o2, f7_o3])
    figures.append(f7)

    f8_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f8_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f8_o3 = Object("3", [s_square, s_fill, s_size_small, Slot("inside", "1,2")] + slots)
    f8 = Figure("GENERATED", [f8_o1, f8_o2, f8_o3])
    figures.append(f8)

    f9_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f9_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f9_o3 = Object("3", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2")] + slots)
    f9 = Figure("GENERATED", [f9_o1, f9_o2, f9_o3])
    figures.append(f9)

    f10_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f10_o2 = Object("2", [s_square, s_fill, s_size_small, Slot("inside", "1")] + slots)
    f10_o3 = Object("3", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2")] + slots)
    f10 = Figure("GENERATED", [f10_o1, f10_o2, f10_o3])
    figures.append(f10)

    f11_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f11_o2 = Object("2", [s_square, s_fill, s_size_large, Slot("inside", "1")] + slots)
    f11_o3 = Object("3", [s_square, s_fill, s_size_medium, Slot("inside", "1,2")] + slots)
    f11_o4 = Object("4", [s_square, s_fill, s_size_small, Slot("inside", "1,2,3")] + slots)
    f11 = Figure("GENERATED", [f11_o1, f11_o2, f11_o3, f11_o4])
    figures.append(f11)

    f12_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f12_o2 = Object("2", [s_square, s_fill, s_size_large, Slot("inside", "1")] + slots)
    f12_o3 = Object("3", [s_square, s_fill, s_size_medium, Slot("inside", "1,2")] + slots)
    f12_o4 = Object("4", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2,3")] + slots)
    f12 = Figure("GENERATED", [f12_o1, f12_o2, f12_o3, f12_o4])
    figures.append(f12)

    f13_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f13_o2 = Object("2", [s_square, s_fill, s_size_large, Slot("inside", "1")] + slots)
    f13_o3 = Object("3", [s_square, s_fill, s_size_small, Slot("inside", "1,2")] + slots)
    f13_o4 = Object("4", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2,3")] + slots)
    f13 = Figure("GENERATED", [f13_o1, f13_o2, f13_o3, f13_o4])
    figures.append(f13)

    f14_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f14_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f14_o3 = Object("3", [s_square, s_fill, s_size_small, Slot("inside", "1,2")] + slots)
    f14_o4 = Object("4", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2,3")] + slots)
    f14 = Figure("GENERATED", [f14_o1, f14_o2, f14_o3, f14_o4])
    figures.append(f14)

    f15_o1 = Object("1", [s_square, s_fill, s_size_very_large])
    f15_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f15_o3 = Object("3", [s_square, s_fill, s_size_small, Slot("inside", "1,2")] + slots)
    f15_o4 = Object("4", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2,3")] + slots)
    f15_o5 = Object("5", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2,3,4")] + slots)
    f15 = Figure("GENERATED", [f15_o1, f15_o2, f15_o3, f15_o4, f15_o5])
    figures.append(f15)

    return figures


def generate_all_filled_squares_for_large(slots):
    figures = []
    s_square = Slot(get_slot_name_shape(), get_slot_filler_square())
    s_fill = Slot(get_slot_name_fill(), "yes")
    s_size_large = Slot(get_slot_name_size(), "large")
    s_size_medium = Slot(get_slot_name_size(), "medium")
    s_size_small = Slot(get_slot_name_size(), "small")
    s_size_very_small = Slot(get_slot_name_size(), "very-small")

    f1_o1 = Object("1", [s_square, s_fill, s_size_large] + slots)
    f1_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f1 = Figure("GENERATED", [f1_o1, f1_o2])
    figures.append(f1)

    f2_o1 = Object("1", [s_square, s_fill, s_size_large] + slots)
    f2_o2 = Object("2", [s_square, s_fill, s_size_small, Slot("inside", "1")] + slots)
    f2 = Figure("GENERATED", [f2_o1, f2_o2])
    figures.append(f2)

    f3_o1 = Object("1", [s_square, s_fill, s_size_large] + slots)
    f3_o2 = Object("2", [s_square, s_fill, s_size_very_small, Slot("inside", "1")] + slots)
    f3 = Figure("GENERATED", [f3_o1, f3_o2])
    figures.append(f3)

    f4_o1 = Object("1", [s_square, s_fill, s_size_large] + slots)
    f4_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f4_o3 = Object("3", [s_square, s_fill, s_size_small, Slot("inside", "1,2")] + slots)
    f4 = Figure("GENERATED", [f4_o1, f4_o2, f4_o3])
    figures.append(f4)

    f5_o1 = Object("1", [s_square, s_fill, s_size_large])
    f5_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f5_o3 = Object("3", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2")] + slots)
    f5 = Figure("GENERATED", [f5_o1, f5_o2, f5_o3])
    figures.append(f5)

    f6_o1 = Object("1", [s_square, s_fill, s_size_large])
    f6_o2 = Object("2", [s_square, s_fill, s_size_small, Slot("inside", "1")] + slots)
    f6_o3 = Object("3", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2")] + slots)
    f6 = Figure("GENERATED", [f6_o1, f6_o2, f6_o3])
    figures.append(f6)

    f7_o1 = Object("1", [s_square, s_fill, s_size_large])
    f7_o2 = Object("2", [s_square, s_fill, s_size_medium, Slot("inside", "1")] + slots)
    f7_o3 = Object("3", [s_square, s_fill, s_size_small, Slot("inside", "1,2")] + slots)
    f7_o4 = Object("4", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2,3")] + slots)
    f7 = Figure("GENERATED", [f7_o1, f7_o2, f7_o3, f7_o4])
    figures.append(f7)

    return figures


def generate_all_filled_squares_for_medium(slots):
    figures = []
    s_square = Slot(get_slot_name_shape(), get_slot_filler_square())
    s_fill = Slot(get_slot_name_fill(), "yes")
    s_size_medium = Slot(get_slot_name_size(), "medium")
    s_size_small = Slot(get_slot_name_size(), "small")
    s_size_very_small = Slot(get_slot_name_size(), "very-small")

    f1_o1 = Object("1", [s_square, s_fill, s_size_medium] + slots)
    f1_o2 = Object("2", [s_square, s_fill, s_size_small, Slot("inside", "1")] + slots)
    f1 = Figure("GENERATED", [f1_o1, f1_o2])
    figures.append(f1)

    f2_o1 = Object("1", [s_square, s_fill, s_size_medium] + slots)
    f2_o2 = Object("2", [s_square, s_fill, s_size_very_small, Slot("inside", "1")] + slots)
    f2 = Figure("GENERATED", [f2_o1, f2_o2])
    figures.append(f2)

    f3_o1 = Object("1", [s_square, s_fill, s_size_medium] + slots)
    f3_o2 = Object("2", [s_square, s_fill, s_size_small, Slot("inside", "1")] + slots)
    f3_o3 = Object("3", [s_square, s_fill, s_size_very_small, Slot("inside", "1,2")] + slots)
    f3 = Figure("GENERATED", [f3_o1, f3_o2, f3_o3])
    figures.append(f3)

    return figures


def generate_all_filled_squares_for_small(slots):
    figures = []
    s_square = Slot(get_slot_name_shape(), get_slot_filler_square())
    s_fill = Slot(get_slot_name_fill(), "yes")
    s_size_small = Slot(get_slot_name_size(), "small")
    s_size_very_small = Slot(get_slot_name_size(), "very-small")

    f1_o1 = Object("1", [s_square, s_fill, s_size_small] + slots)
    f1_o2 = Object("2", [s_square, s_fill, s_size_very_small, Slot("inside", "1")] + slots)
    f1 = Figure("GENERATED", [f1_o1, f1_o2])
    figures.append(f1)

    return figures


def compare_figures_core(figure_a, figure_b, ignore_positional_slots):
    # TODO: Try to align objects in figures to speed up comparing

    objects_in_a = figure_a.objects()
    objects_in_b = figure_b.objects()

    if not quick_check(objects_in_a, objects_in_b):
        return False

    length = len(objects_in_a)

    combinations = generate_combinations(length, length)

    # Candidate for improvements
    # Maybe hints on where to start?

    for (order_a, order_b) in combinations:
        a_s = get_objects_in_order(figure_a.objects(), order_a)
        b_s_temp = get_objects_in_order(figure_b.objects(), order_b)
        # for i in range(length):
        # a = a_s[i]
        # b = b_s_temp[i]
        # if len(a.slots()) != len(b.slots()):
        #         return False

        b_s = copy.deepcopy(b_s_temp)

        for i in range(length):
            a = a_s[i]
            b = b_s[i]
            rename_object_reference_in_object_list(b_s, b.name(), a.name())

        same = True
        for i in range(length):
            res = compare_objects(a_s[i], b_s[i], ignore_positional_slots)
            if not res:
                same = False
        if same is True:
            return True

    return False


def quick_check(objects_in_a, objects_in_b):
    if len(objects_in_a) != len(objects_in_b):
        return False

    equal = True

    for a in objects_in_a:
        a_shape = find_shape(a)
        a_size = find_size(a)
        a_fill = find_fill(a)
        found = False
        for b in objects_in_b:
            b_shape = find_shape(b)
            b_size = find_size(b)
            b_fill = find_fill(b)
            if a_shape == b_shape and a_size == b_size:
                if "," in a_fill or "," in b_fill:
                    if compare_as_lists(a_fill, b_fill):
                        found = True
                else:
                    if a_fill == b_fill:
                        found = True
        if not found:
            equal = False
    return equal


def compare_objects(object_a, object_b, ignore_positional_slots):
    attributes_in_a = convert_attributes_to_dictionary(object_a.slots())
    attributes_in_b = convert_attributes_to_dictionary(object_b.slots())

    attributes_to_ignore = []

    slots_to_treat_specially = []
    shape_a = object_a.get_slot_filler(get_slot_name_shape())
    shape_b = object_a.get_slot_filler(get_slot_name_shape())
    if shape_a != shape_b:
        return False

    # TODO: Generalize to work with more shapes
    if shape_a == "octagon" and shape_b == "octagon":
        slots_to_treat_specially.append(get_slot_name_angle())

    if object_a.is_any_rotation() or object_b.is_any_rotation():
        attributes_to_ignore.append(get_slot_name_angle())

    if get_slot_name_shape() in attributes_in_a.keys():
        if attributes_in_a[get_slot_name_shape()] == get_slot_filler_circle():
            attributes_to_ignore.append(get_slot_name_angle())

    if ignore_positional_slots:
        for a in get_positional_slot_names():
            attributes_to_ignore.append(a)

    if get_attribute_list_length(attributes_in_a,
                                 attributes_to_ignore) != get_attribute_list_length(attributes_in_b,
                                                                                    attributes_to_ignore):
        return False

    keys = list(attributes_in_a.keys())

    for key in keys:
        if key not in attributes_to_ignore:
            if key not in attributes_in_b:
                return False
            else:
                if "," in attributes_in_a[key] or "," in attributes_in_b[key]:
                    if not compare_as_lists(attributes_in_a[key], attributes_in_b[key]):
                        return False
                else:
                    if key not in slots_to_treat_specially:
                        if attributes_in_a[key] != attributes_in_b[key]:
                            return False
                    else:
                        # TODO: Generalize to work with more shapes
                        if shape_a == "octagon":
                            angle_a = int(attributes_in_a[key])
                            angle_b = int(attributes_in_b[key])
                            diff = angle_b - angle_a
                            if diff % 90 != 0:
                                return False

    return True


def get_attribute_list_length(attributes, attributes_to_ignore):
    count = 0
    for a in attributes:
        if a not in attributes_to_ignore:
            count += 1

    return count


def compare_as_lists(value_a, value_b):
    a_s = sorted(value_a.split(","), key=str.lower)
    b_s = sorted(value_b.split(","), key=str.lower)

    if len(a_s) != len(b_s):
        return False

    length = len(a_s)

    for i in range(length):
        if a_s[i] != b_s[i]:
            return False

    return True


class Figure:
    def __init__(self, name, objects):
        self._name = name
        self._objects = objects

    def name(self):
        return self._name

    def objects(self):
        return self._objects


class Object:
    def __init__(self, name, slots):
        self._name = name
        self._slots = slots
        self._has_any_rotation = False

    def name(self):
        return self._name

    def slots(self):
        return self._slots

    def new_name(self, name):
        self._name = name

    def is_any_rotation(self):
        return self._has_any_rotation

    def set_any_rotation(self):
        self._has_any_rotation = True

    def set_new_slots(self, new_attributes):
        self._slots = new_attributes

    def get_slot_filler(self, slot_name):
        for a in self._slots:
            if a.name() == slot_name:
                return a.filler()
        return ""

    def set_slot_filler(self, slot_name, filler):
        found = False
        for a in self._slots:
            if a.name() == slot_name:
                found = True
                a.new_filler(filler)

        if not found:
            s = Slot(slot_name, filler)
            self._slots.append(s)

    def remove_slot(self, slot_name):
        to_delete = None
        for s in self._slots:
            if s.name() == slot_name:
                to_delete = s

        if to_delete is not None:
            self._slots.remove(to_delete)


class Slot:
    def __init__(self, name, filler):
        self._name = name
        self._filler = filler

    def name(self):
        return self._name

    def filler(self):
        return self._filler

    def new_filler(self, value):
        self._filler = value

    def append_values(self, values):
        old_values = self._filler.split(",")
        old_values = [i for i in old_values if i != "no"]

        new_values = values.split(",")
        combined = old_values + new_values
        distinct = list(set(combined))
        sorted_list = sorted(distinct, key=str.lower)
        string = ""
        for s in sorted_list:
            if len(string) > 0:
                string += ","
            string += s
        self._filler = string


class FigureTransformation:
    def __init__(self, object_transformations, structure):
        self._object_transformations = object_transformations
        self._is_general = False
        self._structure = structure

    def is_general(self):
        return self._is_general

    def object_transformations(self):
        return self._object_transformations

    def get_structure(self):
        return self._structure

    def set_general(self):
        self._is_general = True
        if len(self.object_transformations()) > 0:
            while len(self.object_transformations()) > 1:
                del self.object_transformations()[-1]
        self.object_transformations()[0].delete_positional_attributes()

    def check_general(self):
        verification = self.object_transformations()[0]
        for t in self.object_transformations():
            if t.delete() is not verification.delete():
                return False
            if t.add() is not verification.add():
                return False

            if len(t.slot_transformations()) != len(verification.slot_transformations()):
                return False

            for i in range(len(t.slot_transformations())):
                a = t.slot_transformations()[i]
                b = verification.slot_transformations()[i]
                if a.name() != b.name():
                    return False
                if a.filler_from() != b.filler_from():
                    return False
                if a.filler_to() != b.filler_to():
                    return False

            if len(t.to_add()) != len(verification.to_add()):
                return False

            for i in range(len(t.to_add())):
                a = t.to_add()[i]
                b = verification.to_add()[i]
                if a.name() != b.name():
                    return False
                if a.filler_from() != b.filler_from():
                    return False
                if a.filler_to() != b.filler_to():
                    return False

            if len(t.to_delete()) != len(verification.to_delete()):
                return False

            for i in range(len(t.to_delete())):
                a = t.to_delete()[i]
                b = verification.to_delete()[i]
                if a != b:
                    return False

            positional_attributes = get_positional_slot_names()

            for a in t.required():
                if a.name() not in positional_attributes:
                    found = False
                    for b in verification.required():
                        if a.name() == b.name():
                            if a.filler() != b.filler():
                                return False
                            else:
                                found = True
                    if not found:
                        return False

            for b in verification.required():
                if b.name() not in positional_attributes:
                    found = False
                    for a in t.required():
                        if a.name() == b.name():
                            if a.filler() != b.filler():
                                return False
                            else:
                                found = True

                    if not found:
                        return False

            if t.is_vertical_reflection() is not verification.is_vertical_reflection():
                return False

            if t.is_horizontal_reflection() is not verification.is_horizontal_reflection():
                return False

            if t.rotation() != verification.rotation():
                return False
        return True


class ObjectTransformation:
    def __init__(self, name, delete, add, slot_transformations, to_add, to_delete, required, based_on_shape):
        self._name = name
        self._delete = delete
        self._add = add
        self._slot_transformations = slot_transformations
        self._to_add = to_add
        self._to_delete = to_delete
        self._is_general = False
        self._required = required
        self._is_vertical_reflection = False
        self._is_horizontal_reflection = False
        self._rotation = 0
        self._is_rotation = False
        self._is_any_rotation = False
        self._is_fill_add = False
        self._is_fill_rotate = False
        self._fill_add = 0
        self._fill_rotate = 0
        self._is_side_type = False
        self._sides_transformation = ""
        self._based_on_shape = based_on_shape

    def name(self):
        return self._name

    def delete(self):
        return self._delete

    def add(self):
        return self._add

    def slot_transformations(self):
        return self._slot_transformations

    def to_add(self):
        return self._to_add

    def to_delete(self):
        return self._to_delete

    def is_general(self):
        return self._is_general

    def required(self):
        return self._required

    def is_reflection(self):
        return self._is_vertical_reflection or self._is_horizontal_reflection

    def is_vertical_reflection(self):
        return self._is_vertical_reflection

    def is_horizontal_reflection(self):
        return self._is_horizontal_reflection

    def set_vertical_reflection(self):
        self._is_vertical_reflection = True

    def set_horizontal_reflection(self):
        self._is_horizontal_reflection = True

    def remove_slot_transformation(self, transformation):
        self._slot_transformations.remove(transformation)

    def remove_required_attribute(self, attribute):
        self._required.remove(attribute)

    def is_rotation(self):
        return self._is_rotation

    def is_any_rotation(self):
        return self._is_any_rotation

    def rotation(self):
        return self._rotation

    def set_rotation(self, value):
        self._is_rotation = True
        self._rotation = value

    def set_any_rotation(self):
        self._is_any_rotation = True

    def is_shape_change(self):
        for a in self._slot_transformations:
            if a.name() == get_slot_name_shape():
                return True
        return False

    def is_size_change(self):
        for a in self._slot_transformations:
            if a.name() == get_slot_name_size():
                return True
        return False

    def change_name(self, new_name):
        self._name = new_name

    def delete_positional_attributes(self):
        self._slot_transformations = \
            [a for a in self._slot_transformations if not is_positional_slot(a.name())]
        self._required = [a for a in self._required if not is_positional_slot(a.name())]

    def set_slot_to_be_added_from(self, slot_name, object_name):
        for a in self._to_add:
            if a.name() == slot_name:
                a.set_filler_to_be_added_from_object(object_name)

    def is_fill_add(self):
        return self._is_fill_add

    def set_fill_add(self, value):
        self._is_fill_add = True
        self._fill_add = value

    def fill_add(self):
        return self._fill_add

    def fill_rotate(self):
        return self._fill_rotate

    def is_fill_rotate(self):
        return self._is_fill_rotate

    def set_fill_rotate(self, value):
        self._is_fill_rotate = True
        self._fill_rotate = value

    def is_side_type(self):
        return self._is_side_type

    def sides_transformation(self):
        return self._sides_transformation

    def set_sides_transformation(self, transformation):
        self._sides_transformation = transformation
        self._is_side_type = True

    def based_on_shape(self):
        return self._based_on_shape


class SlotTransformation:
    def __init__(self, name, filler_from, filler_to):
        self._name = name
        self._filler_from = filler_from
        self._filler_to = filler_to
        self._is_append_values = False
        if filler_from == "yes" and filler_to == "no":
            self._is_bool = True
        elif filler_from == "no" and filler_to == "yes":
            self._is_bool = True
        else:
            self._is_bool = False
        self._is_relative_add_value = False
        self._add_filler_from_object = ""

    def name(self):
        return self._name

    def filler_from(self):
        return self._filler_from

    def filler_to(self):
        return self._filler_to

    def is_append_values(self):
        return self._is_append_values

    def set_append_value(self):
        self._is_append_values = True

    def is_bool(self):
        return self._is_bool

    def is_relative_add_value(self):
        return self._is_relative_add_value

    def set_new_filler_from(self, new_filler_from):
        self._filler_from = new_filler_from

    def set_new_filler_to(self, new_filler_to):
        self._filler_to = new_filler_to

    def get_filler_to_be_added_from_object(self):
        return self._add_filler_from_object

    def set_filler_to_be_added_from_object(self, object_name):
        self._add_filler_from_object = object_name
        self._is_relative_add_value = True


class UseRequirements(Enum):
    NONE = 0
    SIZE = 1
    SHAPE = 2
    ALL = 9


class Strategy(Enum):
    # 2x1
    S2X1_FIXED_POSITIONS = 11
    S2X1_USE_REQUIREMENTS = 12
    S2X1_USE_SIZE_REQUIREMENTS = 13
    S2X1_NORMAL = 14
    S2X1_IGNORE_POSITIONAL = 15
    S2X1_STICK_FILL = 16

    # 2x2
    S2X2_FIXED_POSITIONS = 21
    S2X2_USE_REQUIREMENTS = 22
    S2X2_USE_SIZE_REQUIREMENTS = 23
    S2X2_NORMAL = 24
    S2X2_IGNORE_POSITIONAL = 25
    S2X2_STICK_FILL = 26


def is_2x1_strategy(strategy):
    if strategy == Strategy.S2X1_FIXED_POSITIONS:
        return True
    elif strategy == Strategy.S2X1_USE_REQUIREMENTS:
        return True
    elif strategy == Strategy.S2X1_USE_SIZE_REQUIREMENTS:
        return True
    elif strategy == Strategy.S2X1_NORMAL:
        return True
    elif strategy == Strategy.S2X1_IGNORE_POSITIONAL:
        return True
    elif strategy == Strategy.S2X1_STICK_FILL:
        return True
    else:
        return False


def is_2x2_strategy(strategy):
    if strategy == Strategy.S2X2_FIXED_POSITIONS:
        return True
    elif strategy == Strategy.S2X2_USE_REQUIREMENTS:
        return True
    elif strategy == Strategy.S2X2_USE_SIZE_REQUIREMENTS:
        return True
    elif strategy == Strategy.S2X2_NORMAL:
        return True
    elif strategy == Strategy.S2X2_IGNORE_POSITIONAL:
        return True
    elif strategy == Strategy.S2X2_STICK_FILL:
        return True
    else:
        return False