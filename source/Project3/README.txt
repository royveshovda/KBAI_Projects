My contribution should consist of these files:
Agent.py
Common.py
Generator.py
Tester.py
Classifier.py
Classifier_Tests.py
README.txt (this file)
Design report.pdf
Design report for project 2.pdf
AppendixA.pdf
AppendixB.pdf

These depends of the following standard libraries:
copy
timeit
itertools
math

The code is developed for python 2.7.9 and needs the following external dependencies:
Enum34 (to install: pip install enum34)
OpenCV 2.4.10 (to install: see web page got opencv.org)
NumPy (to install: pip install numpy)

In a console not supporting Escape codes, the printout will look a bit weird.
In a console supporting this, the printouts will have color-coding.
This will not affect the Result.txt as all

WARNING: This code does not complete "2x1 Challenge Problem 05" in a reasonable time.
In the current implementation it needs hours to run, and fails after trying.
For the agent to complete, this must be left out. My code tries to ignore this test, but looks for the hardcoded name.





