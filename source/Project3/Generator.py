import copy
from Common import get_objects_in_order, FigureTransformation, generate_object_names, \
    rename_object_reference_in_object_list, convert_attributes_to_dictionary, SlotTransformation, Slot, \
    ObjectTransformation, get_slot_name_shape, get_slot_name_angle, find_shape, get_slot_filler_circle, \
    generate_figure_combinations, get_common_figure_structure, Figure, get_positional_figure, get_slot_name_fill, \
    get_all_shapes_with_sides_only_names, replace_shape_type, get_common_shape_structure


def calculate_transformations_2x2(figure_a, figure_b, figure_c, fix_positions=False, fix_shape=False):
    if fix_positions:
        combinations_a_to_b = get_common_figure_structure(figure_a, figure_b)
        combinations_a_to_c = get_common_figure_structure(figure_a, figure_c)
    elif fix_shape:
        combinations_a_to_b = get_common_shape_structure(figure_a, figure_b)
        combinations_a_to_c = get_common_shape_structure(figure_a, figure_c)
    else:
        combinations_a_to_b = generate_figure_combinations(figure_a, figure_b)
        combinations_a_to_c = generate_figure_combinations(figure_a, figure_c)

    a_to_b = calculate_transformations(figure_a, figure_b, combinations_a_to_b)
    a_to_c = calculate_transformations(figure_a, figure_c, combinations_a_to_c)

    transformations = []
    for t_b in a_to_b:
        for t_c in a_to_c:
            transformations.append((t_b, t_c))
    return transformations


def calculate_transformations_2x1(figure_a, figure_b, fix_positions=False, fix_shape=False):
    if fix_positions:
        combinations = get_common_figure_structure(figure_a, figure_b)
    elif fix_shape:
        combinations = get_common_shape_structure(figure_a, figure_b)
    else:
        combinations = generate_figure_combinations(figure_a, figure_b)
    return calculate_transformations(figure_a, figure_b, combinations)


def calculate_transformations(figure_a, figure_b, combinations):
    transformations = []

    for (order_a, order_b) in combinations:
        a_s = copy.deepcopy(get_objects_in_order(figure_a.objects(), order_a))
        b_s = copy.deepcopy(get_objects_in_order(figure_b.objects(), order_b))
        trans, structure_figure = calculate_transformation(a_s, b_s)

        for t in trans:
            transformation = FigureTransformation(t, structure_figure)
            if transformation.check_general():
                transformation.set_general()
            transformations.append(transformation)
    return transformations


def has_all_objects_of_side_type(ordered_objects_from_a, ordered_objects_from_b):
    shape_names_to_detect = get_all_shapes_with_sides_only_names()
    shapes_found = []
    for l in [ordered_objects_from_a, ordered_objects_from_b]:
        for o in l:

            if o is False:
                return False
            else:
                shape = o.get_slot_filler(get_slot_name_shape())
                if shape not in shape_names_to_detect:
                    return False
                else:
                    if shape not in shapes_found:
                        shapes_found.append(shape)
    if len(shapes_found) > 1:
        return True
    else:
        return False


def calculate_transformation(ordered_objects_from_a, ordered_objects_from_b):
    if has_all_objects_of_side_type(ordered_objects_from_a, ordered_objects_from_b):
        for l in [ordered_objects_from_a, ordered_objects_from_b]:
            for o in l:
                replace_shape_type(o)

    object_transformations = []
    length = len(ordered_objects_from_a)

    # Rename objects
    object_names = generate_object_names()
    for i in range(length):
        object_in_a = ordered_objects_from_a[i]
        object_in_b = ordered_objects_from_b[i]
        name = next(object_names)

        if object_in_a is not False:
            rename_object_reference_in_object_list(ordered_objects_from_a, object_in_a.name(), name)

        if object_in_b is not False:
            rename_object_reference_in_object_list(ordered_objects_from_b, object_in_b.name(), name)

    structural_figure = get_positional_figure(Figure("Structure",
                                                     [o for o in ordered_objects_from_a if o is not False]))

    for i in range(length):
        object_in_a = ordered_objects_from_a[i]
        object_in_b = ordered_objects_from_b[i]

        if object_in_a is False:
            o = add_object(object_in_b)
            object_transformations = add_one_to_list(object_transformations, o)
        elif object_in_b is False:
            o = delete_object(object_in_a)
            object_transformations = add_one_to_list(object_transformations, o)
        else:
            o_s = transform_object(object_in_a, object_in_b)
            object_transformations = add_choices_to_list(object_transformations, o_s)

    new_relative_add_transformations = generate_relative_value_add(object_transformations, ordered_objects_from_a)

    if len(new_relative_add_transformations) > 0:
        for t in new_relative_add_transformations:
            object_transformations.append(t)

    return object_transformations, structural_figure


def generate_relative_value_add(object_transformations, ordered_objects_from_a):
    new_transformations = []
    for trans in object_transformations:
        adds = []
        for t in trans:
            if t.add():
                adds.append(t)

        if len(adds) > 0:
            new_trans = copy.deepcopy(trans)
            found = False
            for t in new_trans:
                if t.add():
                    for add_slot in t.to_add():
                        for o in ordered_objects_from_a:
                            if o is not False:
                                if o.get_slot_filler(add_slot.name()) == add_slot.filler_to():
                                    add_slot.set_filler_to_be_added_from_object(o.name())
                                    found = True
            if found:
                new_transformations.append(new_trans)
    return new_transformations


def add_choices_to_list(list_of_transformations, choices):
    new_list = []
    if len(list_of_transformations) == 0:
        for c in choices:
            new_list.append([c])
    else:
        for c in choices:
            for t in list_of_transformations:
                combination = copy.deepcopy(t)
                combination.append(c)
                new_list.append(combination)
    return new_list


def add_one_to_list(list_of_transformations, transformation):
    if len(list_of_transformations) == 0:
        list_of_transformations.append([transformation])
    else:
        for t in list_of_transformations:
            t.append(transformation)

    return list_of_transformations


def has_angle_attributes(objects_in_a, objects_in_b):
    length = len(objects_in_a)
    for i in range(length):
        object_in_a = objects_in_a[i]
        object_in_b = objects_in_b[i]
        if has_angle_attribute(object_in_a, object_in_b):
            return True
    return False


def has_angle_attribute(object_in_a, object_in_b):
    if object_in_a is False:
        pass
    elif object_in_b is False:
        pass
    else:
        attributes_in_a = convert_attributes_to_dictionary(object_in_a.slots())
        attributes_in_b = convert_attributes_to_dictionary(object_in_b.slots())

        angle_attr = get_slot_name_angle()

        if angle_attr in attributes_in_a and angle_attr in attributes_in_b:
            return True
        else:
            return False


def has_reflection(object_in_a, object_in_b):
    if object_in_a is False:
        pass
    elif object_in_b is False:
        pass
    else:
        attributes_in_a = convert_attributes_to_dictionary(object_in_a.slots())
        attributes_in_b = convert_attributes_to_dictionary(object_in_b.slots())

        angle_attr = get_slot_name_angle()
        shape = attributes_in_a[get_slot_name_shape()]

        if angle_attr in attributes_in_a and angle_attr in attributes_in_b:
            from_angle = attributes_in_a[angle_attr]
            to_angle = attributes_in_b[angle_attr]
            return (has_horizontal_reflection(shape, from_angle, to_angle),
                    has_vertical_reflection(shape, from_angle, to_angle))

    return False, False


def calculate_angle_diff(a, b):
    attributes_in_a = convert_attributes_to_dictionary(a.slots())
    attributes_in_b = convert_attributes_to_dictionary(b.slots())

    angle_attr = get_slot_name_angle()

    from_angle = int(float(attributes_in_a[angle_attr]))
    to_angle = int(float(attributes_in_b[angle_attr]))

    return to_angle - from_angle


def has_horizontal_reflection(shape, from_angle, to_angle):
    f = int(float(from_angle))
    t = int(float(to_angle))
    # C = 180 + 2B - A  -> B = 0 for hor, B = 90 for ver

    # Add more shapes here
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if (0 - f) % 360 == t:
            return True
        else:
            return False
    elif shape in ["right-triangle"]:
        if (90 - f) % 360 == t:
            return True
        else:
            return False
    else:
        if (180 - f) % 360 == t:
            return True
        else:
            return False


def has_vertical_reflection(shape, from_angle, to_angle):
    f = int(float(from_angle))
    t = int(float(to_angle))
    # C = 180 + 2B - A  -> B = 0 for hor, B = 90 for ver

    # Add more shapes here
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if (180 - f) % 360 == t:
            return True
        else:
            return False
    elif shape in ["right-triangle"]:
        if (270 - f) % 360 == t:
            return True
        else:
            return False
    else:
        if (0 - f) % 360 == t:
            return True
        else:
            return False


def generate_reflections(transformation, objects_in_a, objects_in_b):
    transformations = []
    t = copy.deepcopy(transformation)

    length = len(t.object_transformations())

    for i in range(length):
        a = objects_in_a[i]
        b = objects_in_b[i]
        hor, ver = has_reflection(a, b)

        if hor or ver:
            trans = t.object_transformations()[i]
            if hor:
                trans.set_horizontal_reflection()
            if ver:
                trans.set_vertical_reflection()

            to_remove = SlotTransformation("", "", "")
            for attr in trans.slot_transformations():
                if attr.name() == get_slot_name_angle():
                    to_remove = attr

            if to_remove.name() == get_slot_name_angle():
                trans.remove_slot_transformation(to_remove)

            to_remove_att = Slot("", "")
            for attr in trans.required():
                if attr.name() == get_slot_name_angle():
                    to_remove_att = attr

            if to_remove_att.name() == get_slot_name_angle():
                trans.remove_required_attribute(to_remove_att)

    transformations.append(t)
    return transformations


def remove_attribute(transformation, attribute):
    to_remove = SlotTransformation("", "", "")
    for attr in transformation.slot_transformations():
        if attr.name() == attribute:
            to_remove = attr
    if to_remove.name() == attribute:
        transformation.remove_slot_transformation(to_remove)
    to_remove_att = Slot("", "")
    for attr in transformation.required():
        if attr.name() == attribute:
            to_remove_att = attr
    if to_remove_att.name() == attribute:
        transformation.remove_required_attribute(to_remove_att)


def generate_reflection(transformation, a, b):
    t = copy.deepcopy(transformation)
    hor, ver = has_reflection(a, b)

    if hor:
        t.set_horizontal_reflection()
    if ver:
        t.set_vertical_reflection()

    return t


def generate_any_rotation(transformation):
    t = copy.deepcopy(transformation)
    t.set_any_rotation()
    return t


def generate_rotations(transformation, a, b, shape):
    result = []
    t1 = copy.deepcopy(transformation)
    t2 = copy.deepcopy(transformation)
    diff = calculate_angle_diff(a, b)

    # Rotate both ways
    d1 = diff % 360
    t1.set_rotation(d1)
    result.append(t1)

    if shape not in ["triangle", "Pac-Man", "half-arrow", "arrow"]:
        d2 = (-diff) % 360
        t2.set_rotation(d2)
        result.append(t2)

    return result


def add_object(obj):
    attr = []
    for a in obj.slots():
        a_t = SlotTransformation(a.name(), "", a.filler())
        attr.append(a_t)
    return ObjectTransformation(obj.name(), False, True, [], attr, [], [], "")


def delete_object(obj):
    requirements = get_required_slots(obj)
    shape = obj.get_slot_filler(get_slot_name_shape())
    return ObjectTransformation(obj.name(), True, False, [], [], [], requirements, shape)


def get_required_slots(obj):
    requirements = []
    for attribute_a in obj.slots():
        if attribute_a.name() != get_slot_name_shape():
            requirements.append(attribute_a)
    return requirements


def has_multi_valued_fill_slots(object_a, object_b):
    filler_a = object_a.get_slot_filler(get_slot_name_fill())
    filler_b = object_b.get_slot_filler(get_slot_name_fill())

    if len(filler_a) > 0 and len(filler_b) > 0 and filler_a != filler_b:
        values = ["yes", "no"]
        if (filler_a not in values) or (filler_b not in values):
            return True

    return False


def calculate_fill_add_and_rotate(fill_a, fill_b):
    a = sorted(fill_a.split(","), key=str.lower)
    b = sorted(fill_b.split(","), key=str.lower)
    add = 0
    rotate = 0
    if a == ["top-left"]:
        if b == ["top-left", "top-right"]:
            add = 1
        elif b == ["bottom-left", "top-left"]:
            add = -1
        elif b == ["top-right"]:
            rotate = 1
        elif b == ["bottom-left"]:
            rotate = -1
    elif a == ["top-right"]:
        if b == ["bottom-right", "top-right"]:
            add = 1
        elif b == ["top-left", "top-right"]:
            add = -1
        elif b == ["bottom-right"]:
            rotate = 1
        elif b == ["top-left"]:
            rotate = -1
    elif a == ["bottom-right"]:
        if b == ["bottom-left", "bottom-right"]:
            add = 1
        elif b == ["bottom-right", "top-right"]:
            add = -1
        elif b == ["bottom-left"]:
            rotate = 1
        elif b == ["top-right"]:
            rotate = -1
    elif a == ["bottom-left"]:
        if b == ["bottom-left", "top-left"]:
            add = 1
        elif b == ["bottom-left", "bottom-right"]:
            add = -1
        elif b == ["top-left"]:
            rotate = 1
        elif b == ["bottom-right"]:
            rotate = -1
    elif a == ["top-left", "top-right"]:
        if b == ["bottom-right", "top-left", "top-right"]:
            add = 1
        elif b == ["bottom-left", "top-left", "top-right"]:
            add = -1
        elif b == ["bottom-right", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "top-left"]:
            rotate = -1
    elif a == ["bottom-right", "top-right"]:
        if b == ["bottom-left", "bottom-right", "top-right"]:
            add = 1
        elif b == ["bottom-right", "top-left", "top-right"]:
            add = -1
        elif b == ["top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right"]:
            rotate = -1
    elif a == ["bottom-left", "bottom-right"]:
        if b == ["bottom-left", "bottom-right", "top-left"]:
            add = 1
        elif b == ["bottom-left", "bottom-right", "top-right"]:
            add = -1
        elif b == ["bottom-left", "top-left"]:
            rotate = 1
        elif b == ["bottom-right", "top-right"]:
            rotate = -1
    elif a == ["bottom-left", "top-left"]:
        if b == ["bottom-left", "top-left", "top-right"]:
            add = 1
        elif b == ["bottom-left", "bottom-right", "top-left"]:
            add = -1
        elif b == ["top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right"]:
            rotate = -1

    elif a == ["bottom-right", "top-left", "top-right"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-left", "bottom-right", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "top-left", "top-right"]:
            rotate = -1
    elif a == ["bottom-left", "bottom-right", "top-right"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-left", "bottom-right", "top-left"]:
            rotate = 1
        elif b == ["bottom-right", "top-left", "top-right"]:
            rotate = -1

    elif a == ["bottom-left", "bottom-right", "top-left"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-left", "top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right", "top-right"]:
            rotate = -1

    elif a == ["bottom-left", "top-left", "top-right"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-right", "top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right", "top-left"]:
            rotate = -1
    return add, rotate


def has_side_attributes(object_a, object_b):
    shape_a = object_a.get_slot_filler(get_slot_name_shape())
    shape_b = object_b.get_slot_filler(get_slot_name_shape())
    if "side-object" in shape_a and "side-object" in shape_b:
        return True
    else:
        return False


def transform_object(object_in_a, object_in_b):
    attribute_transformations = []

    shape = object_in_a.get_slot_filler(get_slot_name_shape())

    requirements = get_required_slots(object_in_a)

    for attribute_a in object_in_a.slots():
        for attribute_b in object_in_b.slots():
            if attribute_a.name() == attribute_b.name():
                if attribute_a.filler() != attribute_b.filler():
                    if attribute_a.filler() == "no" and "," in attribute_b.filler():
                        at = SlotTransformation(attribute_a.name(),
                                                "",
                                                attribute_b.filler())
                        at.set_append_value()
                    else:
                        at = SlotTransformation(attribute_a.name(),
                                                attribute_a.filler(),
                                                attribute_b.filler())
                    attribute_transformations.append(at)
    attributes_to_add = []
    for attribute_b in object_in_b.slots():
        found = False
        for attribute_a in object_in_a.slots():
            if attribute_a.name() == attribute_b.name():
                found = True

        if not found:
            at = SlotTransformation(attribute_b.name(),
                                    "",
                                    attribute_b.filler())
            attributes_to_add.append(at)
    attributes_to_remove = []
    for attribute_a in object_in_a.slots():
        found = False
        for attribute_b in object_in_b.slots():
            if attribute_a.name() == attribute_b.name():
                found = True

        if not found:
            attributes_to_remove.append(attribute_a.name())

    o = ObjectTransformation(object_in_a.name(), False, False, attribute_transformations, attributes_to_add,
                             attributes_to_remove, requirements, shape)

    trans = []

    added = False
    if has_angle_attribute(object_in_a, object_in_b):
        remove_attribute(o, get_slot_name_angle())

        if find_shape(object_in_a) == get_slot_filler_circle() and find_shape(object_in_b) == get_slot_filler_circle():
            r = generate_any_rotation(o)
            trans.append(r)
            added = True
        else:
            hor, ver = has_reflection(object_in_a, object_in_b)

            if hor or ver:
                r = generate_reflection(o, object_in_a, object_in_b)
                trans.append(r)
                added = True

            ts = generate_rotations(o, object_in_a, object_in_b, shape)
            for t in ts:
                trans.append(t)
                added = True
    if has_multi_valued_fill_slots(object_in_a, object_in_b):
        fill_a = object_in_a.get_slot_filler(get_slot_name_fill())
        fill_b = object_in_b.get_slot_filler(get_slot_name_fill())
        add, rotate = calculate_fill_add_and_rotate(fill_a, fill_b)
        if add != 0 or rotate != 0:
            if add != 0:
                t = copy.deepcopy(o)
                remove_attribute(t, get_slot_name_fill())
                t.set_fill_add(add)
                trans.append(t)
            if rotate != 0:
                t = copy.deepcopy(o)
                remove_attribute(t, get_slot_name_fill())
                t.set_fill_rotate(rotate)
                trans.append(t)
        else:
            trans.append(o)
        added = True
    if has_side_attributes(object_in_a, object_in_b):
        t = copy.deepcopy(o)
        remove_attribute(t, get_slot_name_shape())

        shape_a = object_in_a.get_slot_filler(get_slot_name_shape())
        [_, sides_a] = shape_a.split(":")

        shape_b = object_in_b.get_slot_filler(get_slot_name_shape())
        [_, sides_b] = shape_b.split(":")

        i_sides_b = int(sides_b)
        i_sides_a = int(sides_a)

        if i_sides_a % i_sides_b == 0:
            divider = int(i_sides_a / i_sides_b)
            if divider > 1:
                t_div = copy.deepcopy(t)
                t_div.set_sides_transformation("div:{0}".format(divider))
                trans.append(t_div)

        if i_sides_b % i_sides_a == 0:
            multiplier = int(i_sides_b / i_sides_a)
            if multiplier > 1:
                t_div = copy.deepcopy(t)
                t_div.set_sides_transformation("mult:{0}".format(multiplier))
                trans.append(t_div)

        diff = i_sides_b - i_sides_a
        t.set_sides_transformation("add:{0}".format(diff))
        trans.append(t)
        added = True

    if not added:
        trans.append(o)
    return trans