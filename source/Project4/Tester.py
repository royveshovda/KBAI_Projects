import copy
from Common import generate_permutations, get_objects_in_order, rename_object_reference_in_object_list, Object, \
    Slot, Figure, generate_object_names, get_slot_name_shape, get_slot_name_angle, compare_figures, \
    UseRequirements, get_slot_name_size, get_slot_name_fill, get_shape_for_number_of_sides, replace_shape_type, \
    get_positional_slot_names, is_same_sizes, grow_size, shrink_size, SlotTransformation, ObjectTransformation, \
    FigureTransformation, has_symmetrically_shaped_number_of_objects, is_all_shapes_of_same_shape_and_size_and_angle


def test_3x3(figures, transformations, potential_solutions, alignments,
             use_requirements, ignore_positional_attributes, fix_positions, stick_fill, use_exact):
    # TODO: Implement
    # figure_a = figures["A"]
    # figure_b = figures["B"]
    figure_c = figures["C"]
    # figure_d = figures["D"]
    # figure_e = figures["E"]
    figure_f = figures["F"]
    figure_g = figures["G"]
    figure_h = figures["H"]

    combinations_to_solve_the_problem = []

    trans_horizontal = transformations[alignments[0]]
    trans_vertical = transformations[alignments[1]]

    if len(trans_horizontal) == 1 and trans_horizontal[0].is_3x_angle_sum():
        trans_horizontal = calculate_angle_sum(figure_g, figure_h)

    if len(trans_vertical) == 1 and trans_vertical[0].is_3x_angle_sum():
        trans_vertical = calculate_angle_sum(figure_c, figure_f)

    for t_vert in trans_vertical:
        solutions_vertical = transform(figure_f, t_vert, use_requirements, fix_positions, stick_fill)
        if len(solutions_vertical) > 0:
            for t_hor in trans_horizontal:
                solutions_horizontal = transform(figure_h, t_hor, use_requirements, fix_positions, stick_fill)
                for sol_hor in solutions_horizontal:
                    for sol_ver in solutions_vertical:
                        if compare_figures(sol_hor, sol_ver, ignore_positional_attributes, use_exact):
                            for figure_name in potential_solutions:
                                if compare_figures(sol_ver, potential_solutions[figure_name],
                                                   ignore_positional_attributes, use_exact):
                                    combinations_to_solve_the_problem.append(((t_hor, t_vert), figure_name))

    return combinations_to_solve_the_problem


def calculate_angle_sum(figure_1, figure_2):
    angle1 = int(figure_1.objects()[0].get_slot_filler(get_slot_name_angle()))
    angle2 = int(figure_2.objects()[0].get_slot_filler(get_slot_name_angle()))
    shape2 = figure_2.objects()[0].get_slot_filler(get_slot_name_shape())
    size2 = figure_2.objects()[0].get_slot_filler(get_slot_name_size())
    new_angle = (angle1 + angle2) % 360
    slot_transform = SlotTransformation(get_slot_name_angle(), str(angle2), str(new_angle))
    obj_trans = ObjectTransformation("OBJECT_ANGLE_SUM", [], [], [slot_transform], [], [], [], shape2, size2)
    return [FigureTransformation([obj_trans], None)]


def test_2x2(figures, transformations, potential_solutions,
             use_requirements, ignore_positional_attributes, fix_positions, stick_fill, use_exact):
    figure_b = figures["B"]
    figure_c = figures["C"]
    combinations_to_solve_the_problem = []
    for idx, (a_to_b, a_to_c) in enumerate(transformations):
        solutions_c = transform(figure_c, a_to_b, use_requirements, fix_positions, stick_fill)
        if len(solutions_c) > 0:
            solutions_b = transform(figure_b, a_to_c, use_requirements, fix_positions, stick_fill)

            for solution_b in solutions_b:
                for solution_c in solutions_c:
                    if compare_figures(solution_b, solution_c, ignore_positional_attributes, use_requirements):
                        for figure_name in potential_solutions:
                            if compare_figures(solution_b, potential_solutions[figure_name],
                                               ignore_positional_attributes, use_exact):
                                combinations_to_solve_the_problem.append(((a_to_b, a_to_c), figure_name))

    return combinations_to_solve_the_problem


def test_2x1(figures, transformations, potential_solutions,
             use_requirements, ignore_positional_attributes, fix_positions, stick_fill, use_exact):
    figure_c = figures["C"]
    combinations_to_solve_the_problem = []
    for idx, transformation in enumerate(transformations):
        solutions = transform(figure_c, transformation, use_requirements, fix_positions, stick_fill)
        for solution in solutions:
            for figure_name in potential_solutions:
                if compare_figures(solution, potential_solutions[figure_name],
                                   ignore_positional_attributes, use_exact):
                    combinations_to_solve_the_problem.append((transformation, figure_name))

    return combinations_to_solve_the_problem


def add_row(figure_base):
    results = []
    if has_symmetrically_shaped_number_of_objects(figure_base):
        if is_all_shapes_of_same_shape_and_size_and_angle(figure_base):
            if len(figure_base.objects()) in [0, 1, 2, 3, 4, 6]:
                # TODO: Implement
                pass
    return results


def remove_row(figure_base):
    results = []
    if has_symmetrically_shaped_number_of_objects(figure_base):
        if is_all_shapes_of_same_shape_and_size_and_angle(figure_base):
            if len(figure_base.objects()) in [1, 2, 3, 4, 5, 6, 9]:
                # TODO: Implement
                pass
    return results
    pass


def add_column(figure_base):
    results = []
    if has_symmetrically_shaped_number_of_objects(figure_base):
        if is_all_shapes_of_same_shape_and_size_and_angle(figure_base):
            if len(figure_base.objects()) in [0, 1, 2, 3, 4, 6]:
                # TODO: Implement
                pass
    return results
    pass


def remove_column(figure_base):
    results = []
    if has_symmetrically_shaped_number_of_objects(figure_base):
        if is_all_shapes_of_same_shape_and_size_and_angle(figure_base):
            if len(figure_base.objects()) in [1, 2, 3, 4, 5, 6, 9]:
                # TODO: Implement
                pass
    return results
    pass


def transform(figure_c, figure_transformation, use_requirements, fix_positions, stick_fill):
    results = []

    if figure_transformation.is_general():
        c_s = figure_c.objects()
        transformation = []
        object_names = generate_object_names()
        for i in range(len(c_s)):
            t = copy.deepcopy(figure_transformation.object_transformations()[0])
            name = next(object_names)
            t.change_name(name)
            transformation.append(t)

        try:
            res = transform_ordered(c_s, transformation, use_requirements, stick_fill)
            results.append(res)
        except TypeError:
            pass
    elif figure_transformation.is_add_row():
        new_results = add_row(figure_c)
        if len(new_results) > 0:
            results.append(new_results[0])
    elif figure_transformation.is_remove_row():
        new_results = remove_row(figure_c)
        if len(new_results) > 0:
            results.append(new_results[0])
    elif figure_transformation.is_add_column():
        new_results = add_column(figure_c)
        if len(new_results) > 0:
            results.append(new_results[0])
    elif figure_transformation.is_remove_column():
        new_results = remove_column(figure_c)
        if len(new_results) > 0:
            results.append(new_results[0])
    else:
        combinations_c, combinations_t = generate_transform_combinations(figure_c,
                                                                         figure_transformation,
                                                                         use_requirements,
                                                                         fix_positions)
        for order_t in combinations_t:
            for order_c in combinations_c:
                t_s = get_objects_in_order(figure_transformation.object_transformations(), order_t)
                c_s = get_objects_in_order(figure_c.objects(), order_c)
                try:
                    res = transform_ordered(c_s, t_s, use_requirements, stick_fill)
                    results.append(res)
                except TypeError as error:
                    pass
    return results


def generate_transform_combinations(figure_c, figure_transformation, use_requirements, fix_positions):
    if fix_positions:
        use_requirements = UseRequirements.NONE
        # TODO: Improve: Can be made faster

    if use_requirements in [UseRequirements.SIZE_EXACT, UseRequirements.SIZE_APPROX]:
        builders = []
        for idx, obj in enumerate(figure_c.objects()):
            matches = find_matching_size_transformations(obj, figure_transformation, use_requirements)
            builders.append((idx, matches))

        last_objects = []
        order_o = []
        order_t = []

        found_any = False
        for obj_idx, matches in builders:
            if len(matches) == 1:
                if count_matches(matches[0], builders) == 1:
                    candidate = matches[0]
                    if candidate not in order_t:
                        order_o.append(obj_idx)
                        order_t.append(candidate)
                        found_any = True
                    else:
                        last_objects.append(obj_idx)
                else:
                    last_objects.append(obj_idx)
            else:
                last_objects.append(obj_idx)
        for o in last_objects:
            order_o.append(o)
        for idx, o in enumerate(figure_transformation.object_transformations()):
            if o.add():
                order_t.append(idx)

        if found_any and len(order_o) > 0 and len(order_t) > 0:
            return [order_o], [order_t]
        else:
            return [], []
    elif use_requirements == UseRequirements.ALL:
        builders = []
        for idx, obj in enumerate(figure_c.objects()):
            matches = find_matching_requirements_transformations(obj, figure_transformation)
            builders.append((idx, matches))

        last_objects = []
        order_o = []
        order_t = []

        found_any = False
        for obj_idx, matches in builders:
            if len(matches) == 1:
                if count_matches(matches[0], builders) == 1:
                    candidate = matches[0]
                    if candidate not in order_t:
                        order_o.append(obj_idx)
                        order_t.append(candidate)
                        found_any = True
                    else:
                        last_objects.append(obj_idx)
                else:
                    last_objects.append(obj_idx)
            else:
                last_objects.append(obj_idx)
        for o in last_objects:
            order_o.append(o)

        if found_any and len(order_o) > 0 and len(order_t) > 0:
            return [order_o], [order_t]
        else:
            return [], []

    len_t = len(figure_transformation.object_transformations())
    len_c = len(figure_c.objects())
    if len_t > len_c:
        combinations_t = generate_permutations(len_t)
        combinations_c = [list(range(len_c))]
    else:
        combinations_t = [list(range(len_t))]
        combinations_c = generate_permutations(len_c)
    return combinations_c, combinations_t


def count_matches(idx, builders):
    count = 0
    for _, matches in builders:
        for m in matches:
            if m == idx:
                count += 1
    return count


def find_matching_size_transformations(obj, figure_transformation, use_requirements):
    size = obj.get_slot_filler(get_slot_name_size())

    if size == "":
        return list(range(len(figure_transformation.object_transformations())))
    else:
        matches = []
        for idx, trans in enumerate(figure_transformation.object_transformations()):
            for r in trans.required():
                if r.name() == get_slot_name_size():
                    if is_same_sizes(size, r.filler(), use_requirements):
                        matches.append(idx)
        return matches


def find_matching_requirements_transformations(obj, figure_transformation):
    matches = []
    for idx, trans in enumerate(figure_transformation.object_transformations()):
        match = True
        for r in trans.required():
            if r.filler() != obj.get_slot_filler(r.name):
                match = False
        if match:
            matches.append(idx)
    return matches


def transform_ordered(objects, transformation, use_requirements, stick_fill):
    local_objects = copy.deepcopy(objects)

    length_o = len(local_objects)
    length_t = len(transformation)
    length = max(length_o, length_t)

    # rename objects
    object_index = 0
    for i in range(length):
        if i < len(transformation):
            t = transformation[i]
            if not t.add():
                if object_index < len(objects):
                    o = local_objects[object_index]
                    object_index += 1
                    rename_object_reference_in_object_list(local_objects, o.name(), t.name())

    positions_to_delete = []
    to_add = []
    pos_to_stick_fill = []

    object_index = 0
    for i in range(length):

        if i < len(transformation):
            t = transformation[i]
            if t.add():
                r = Object(t.name(), [])
                for a in t.to_add():
                    if a.is_relative_add_value():
                        value = get_slot_filler_from_object(local_objects,
                                                            a.name(),
                                                            a.get_filler_to_be_added_from_object())
                        r.slots().append(Slot(a.name(), value))
                    else:
                        r.slots().append(Slot(a.name(), a.filler_to()))
                to_add.append(r)
            else:
                if object_index < len(objects):
                    o = local_objects[object_index]
                    object_index += 1
                    if t.delete():
                        positions_to_delete.append(i)
                    else:
                        if stick_fill and o.get_slot_filler(get_slot_name_fill()) != "no":
                            fill = o.get_slot_filler(get_slot_name_fill())
                            pos = get_pos_for_object(o.slots())
                            pos_to_stick_fill.append((pos, fill))
                        if t.is_side_type():
                            replace_shape_type(o)
                        if use_requirements == UseRequirements.ALL:
                            if not verify_object(o, t.required()):
                                raise TypeError("Not correct object to transform (ALL)")
                        elif use_requirements in [UseRequirements.SIZE_APPROX, UseRequirements.SIZE_EXACT]:
                            if not verify_object_size(o, t.required(), use_requirements):
                                raise TypeError("Not correct object to transform (SIZE)")
                        if t.is_any_rotation():
                            o.set_any_rotation()

                        if t.is_grow_type():
                            size = o.get_slot_filler(get_slot_name_size())
                            new_size = grow_size(size)
                            o.set_slot_filler(get_slot_name_size(), new_size)

                        if t.is_shrink_type():
                            size = o.get_slot_filler(get_slot_name_size())
                            new_size = shrink_size(size)
                            o.set_slot_filler(get_slot_name_size(), new_size)

                        for trans in t.slot_transformations():
                            if stick_fill and trans.name() == get_slot_name_fill() and trans.filler_to() != "no":
                                fill = trans.filler_to()
                                pos = get_pos_for_object(t.required())
                                pos_to_stick_fill.append((pos, fill))
                            found_attribute_to_transform = False
                            for attribute in o.slots():
                                if attribute.name() == trans.name():
                                    if trans.is_bool():
                                        found_attribute_to_transform = True
                                        if attribute.filler() == "yes":
                                            attribute.new_filler("no")
                                        elif attribute.filler() == "no":
                                            attribute.new_filler("yes")
                                        else:
                                            raise TypeError("Trying to transform boolean"
                                                            "attribute, but found wrong value")
                                    elif trans.is_append_values():
                                        found_attribute_to_transform = True
                                        attribute.append_values(trans.filler_to())
                                    else:
                                        if attribute.filler() == trans.filler_from():
                                            found_attribute_to_transform = True
                                            attribute.new_filler(trans.filler_to())
                            if found_attribute_to_transform is False:
                                raise TypeError("Not able find attribute to transform: {0}".format(trans.name()))
                        for add in t.to_add():
                            o.slots().append(Slot(add.name(), add.filler_to()))
                        for remove in t.to_delete():
                            found = False
                            to_remove = Slot("", "")
                            for a in o.slots():
                                if a.name() == remove:
                                    to_remove = a
                                    found = True

                            if found is True:
                                o.slots().remove(to_remove)
                            else:
                                raise TypeError("Not able to remove attribute: {0}".format(remove))
                        add_reflection(t, o)
                        add_rotation(t, o)
                        add_fill_adder(t, o)
                        add_fill_rotation(t, o)
                        add_shape_sides_transformation(t, o)
                        convert_shape_type(o)

    positions_to_delete.sort(reverse=True)
    for p in positions_to_delete:
        obj = local_objects[p]
        local_objects.remove(obj)

    for o in to_add:
        local_objects.append(o)

    for o in local_objects:
        for position, fill in pos_to_stick_fill:
            apply_fill_to_position(o, position, fill)

    f = Figure("X", local_objects)
    return f


def apply_fill_to_position(obj, position, fill):
    for name, length in position:
        if len(obj.get_slot_filler(name).split(",")) == length:
            if obj.get_slot_filler(get_slot_name_fill()) == "no":
                obj.set_slot_filler(get_slot_name_fill(), fill)


def get_pos_for_object(slots):
    positional = get_positional_slot_names()
    positional_slots = []
    for a in slots:
        if a.name() in positional:
            positional_slots.append((a.name(), len(a.filler().split(","))))
    return positional_slots


def get_slot_filler_from_object(object_list, slot_name, object_name):
    for o in object_list:
        if o.name() == object_name:
            return o.get_slot_filler(slot_name)
    return ""


def get_shape_from_object(object_list, object_name):
    for o in object_list:
        if o.name() == object_name:
            return o.get_shape()
    return ""


def add_reflection(transformation, obj):
    if transformation.is_vertical_reflection() or transformation.is_horizontal_reflection():
        shape = ""
        for a in obj.slots():
            if a.name() == get_slot_name_shape():
                shape = a.filler()

        for a in obj.slots():
            if a.name() == get_slot_name_angle():
                if transformation.is_horizontal_reflection():
                    apply_horizontal_reflection(shape, a)
                elif transformation.is_vertical_reflection():
                    apply_vertical_reflection(shape, a)

        if transformation.is_vertical_reflection():
            for a in obj.slots():
                if a.name() == "vertical-flip":
                    if a.filler() == "yes":
                        a.new_filler("no")
                    else:
                        a.new_filler("yes")

        if transformation.is_horizontal_reflection():
            for a in obj.slots():
                if a.name() == "horizontal-flip":
                    if a.filler() == "yes":
                        a.new_filler("no")
                    else:
                        a.new_filler("yes")


def add_rotation(transformation, obj):
    if transformation.is_rotation():
        for a in obj.slots():
            if a.name() == get_slot_name_angle():
                v = int(float(a.filler()))
                new_v = (v + transformation.rotation()) % 360
                a.new_filler(str(new_v))


def add_shape_sides_transformation(transformation, obj):
    if transformation.is_side_type():
        shape = obj.get_slot_filler(get_slot_name_shape())
        [_, sides] = shape.split(":")
        s = int(sides)
        transformer = transformation.sides_transformation()
        [_, part] = transformer.split(":")
        if "add" in transformer:
            s += int(part)
        elif "div" in transformer:
            s = int(s / int(part))
        elif "mult" in transformer:
            s = int(s * int(part))
        new_filler = "side-object:{0}".format(s)
        obj.set_slot_filler(get_slot_name_shape(), new_filler)


def convert_shape_type(obj):
    shape = obj.get_slot_filler(get_slot_name_shape())
    if "side-object" in shape:
        [_, sides] = shape.split(":")
        new_shape = get_shape_for_number_of_sides(int(sides))
        obj.set_slot_filler(get_slot_name_shape(), new_shape)


def add_to_fill(add, fill):
    # TODO: Maybe improve this ugly code?
    f = sorted(fill.split(","), key=str.lower)
    if f == ["top-left"]:
        if add == 1:
            return "top-left,top-right"
        if add == 2:
            return "top-left,top-right,bottom-right"
        if add == 3:
            return "yes"
        elif add == -1:
            return "top-left,bottom-left"
        elif add == -2:
            return "top-left,bottom-left,bottom-right"
        elif add == -3:
            return "yes"
    elif f == ["top-right"]:
        if add == 1:
            return "top-right,bottom-right"
        if add == 2:
            return "top-right,bottom-right,bottom-left"
        if add == 3:
            return "yes"
        elif add == -1:
            return "top-left,top-right"
        elif add == -2:
            return "top-left,bottom-left,top-right"
        elif add == -3:
            return "yes"
    elif f == ["bottom-right"]:
        if add == 1:
            return "bottom-left,bottom-right"
        if add == 2:
            return "top-left,bottom-right,bottom-left"
        if add == 3:
            return "yes"
        elif add == -1:
            return "bottom-right,top-right"
        elif add == -2:
            return "bottom-right,top-left,top-right"
        elif add == -3:
            return "yes"
    elif f == ["bottom-left"]:
        if add == 1:
            return "bottom-left,top-left"
        if add == 2:
            return "top-left,top-right,bottom-left"
        if add == 3:
            return "yes"
        elif add == -1:
            return "bottom-left,bottom-right"
        elif add == -2:
            return "bottom-left,bottom-right,top-right"
        elif add == -3:
            return "yes"

    elif f == ["top-left", "top-right"]:
        if add == 1:
            return "top-left,top-right,bottom-right"
        if add == 2:
            return "yes"
        if add == -1:
            return "top-left,top-right,bottom-left"
        if add == -2:
            return "yes"
    elif f == ["bottom-right", "top-right"]:
        if add == 1:
            return "bottom-right,top-right,bottom-left"
        if add == 2:
            return "yes"
        if add == -1:
            return "bottom-right,top-right,top-left"
        if add == -2:
            return "yes"
    elif f == ["bottom-left", "bottom-right"]:
        if add == 1:
            return "bottom-left,bottom-right,top-left"
        if add == 2:
            return "yes"
        if add == -1:
            return "bottom-left,bottom-right,top-right"
        if add == -2:
            return "yes"
    elif f == ["bottom-left", "top-left"]:
        if add == 1:
            return "bottom-left,top-left,top-right"
        if add == 2:
            return "yes"
        if add == -1:
            return "bottom-left,top-left,bottom-right"
        if add == -2:
            return "yes"

    elif f == ["bottom-right", "top-left", "top-right"]:
        if add == 1:
            return "yes"
        if add == -1:
            return "yes"
    elif f == ["bottom-left", "bottom-right", "top-right"]:
        if add == 1:
            return "yes"
        if add == -1:
            return "yes"
    elif f == ["bottom-left", "bottom-right", "top-left"]:
        if add == 1:
            return "yes"
        if add == -1:
            return "yes"
    elif f == ["bottom-left", "top-left", "top-right"]:
        if add == 1:
            return "yes"
        if add == -1:
            return "yes"

    return ""


def add_fill_adder(transformation, obj):
    if transformation.is_fill_add():
        fill = obj.get_slot_filler(get_slot_name_fill())
        new_fill = add_to_fill(transformation.fill_add(), fill)
        obj.set_slot_filler(get_slot_name_fill(), new_fill)


def rotate_fill(rotate, fill):
    f = sorted(fill.split(","), key=str.lower)
    if f == ["top-left"]:
        if rotate == 1:
            return "top-right"
        elif rotate == -1:
            return "bottom-left"
    elif f == ["top-right"]:
        if rotate == 1:
            return "bottom-right"
        elif rotate == -1:
            return "top-left"
    elif f == ["bottom-right"]:
        if rotate == 1:
            return "bottom-left"
        elif rotate == -1:
            return "top-right"
    elif f == ["bottom-left"]:
        if rotate == 1:
            return "top-left"
        elif rotate == -1:
            return "bottom-right"
    elif f == ["top-left", "top-right"]:
        if rotate == 1:
            return "top-right,bottom-right"
        elif rotate == -1:
            return "top-left,bottom-left"
    elif f == ["bottom-right", "top-right"]:
        if rotate == 1:
            return "bottom-right,bottom-left"
        elif rotate == -1:
            return "top-left,top-right"
    elif f == ["bottom-left", "bottom-right"]:
        if rotate == 1:
            return "bottom-left,top-left"
        elif rotate == -1:
            return "bottom-right,top-right"
    elif f == ["bottom-left", "top-left"]:
        if rotate == 1:
            return "top-left,top-right"
        elif rotate == -1:
            return "bottom-left,bottom-right"
    elif f == ["bottom-right", "top-left", "top-right"]:
        if rotate == 1:
            return "top-right,bottom-right,bottom-left"
        elif rotate == -1:
            return "top-left,top-right,bottom-left"
    elif f == ["bottom-left", "bottom-right", "top-right"]:
        if rotate == 1:
            return "top-left,bottom-right,bottom-left"
        elif rotate == -1:
            return "top-left,top-right,bottom-right"
    elif f == ["bottom-left", "bottom-right", "top-left"]:
        if rotate == 1:
            return "top-left,top-right,bottom-left"
        elif rotate == -1:
            return "top-right,bottom-right,bottom-left"
    elif f == ["bottom-left", "top-left", "top-right"]:
        if rotate == 1:
            return "top-left,top-right,bottom-right"
        elif rotate == -1:
            return "top-left,bottom-right,bottom-left"
    return ""


def add_fill_rotation(transformation, obj):
    if transformation.is_fill_rotate():
        fill = obj.get_slot_filler(get_slot_name_fill())
        new_fill = rotate_fill(transformation.fill_rotate(), fill)
        obj.set_slot_filler(get_slot_name_fill(), new_fill)


def apply_horizontal_reflection(shape, attribute):
    # C = 180 + 2B - A  -> B = 0 for hor, B = 90 for ver
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        v = (0 - int(attribute.filler())) % 360
        attribute.new_filler(str(v))
    elif shape in ["right-triangle"]:
        v = (90 - int(attribute.filler())) % 360
        attribute.new_filler(str(v))
    else:
        v = (180 - int(attribute.filler())) % 360
        attribute.new_filler(str(v))


def apply_vertical_reflection(shape, attribute):
    # C = 180 + 2B - A  -> B = 0 for hor, B = 90 for ver
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        v = (180 - int(attribute.filler())) % 360
        attribute.new_filler(str(v))
    elif shape in ["right-triangle"]:
        v = (270 - int(attribute.filler())) % 360
        attribute.new_filler(str(v))
    else:
        v = (0 - int(attribute.filler())) % 360
        attribute.new_filler(str(v))


def verify_object(obj, attributes):
    for a1 in attributes:
        found = False
        for a2 in obj.slots():
            if a1.name() == a2.name() and a1.filler() == a2.filler():
                found = True
        if not found:
            return False

    return True


def verify_object_size(obj, attributes, use_requirements):
    for a1 in attributes:
        if a1.name() == get_slot_name_size():
            found = False
            for a2 in obj.slots():
                if a1.name() == a2.name() and is_same_sizes(a1.filler(), a2.filler(), use_requirements):
                    found = True
            if not found:
                return False
    return True