import timeit
from Classifier import visual_to_textual, calculate_size_levels, remove_extra_info
from Common import convert, UseRequirements, Strategy, is_2x1_strategy, is_2x2_strategy, get_slot_name_shape, Slot, \
    Object, Figure, get_number_of_objects, get_given_figures
from Generator import calculate_transformations_2x1, calculate_transformations_2x2, calculate_transformations_3x3
from Tester import test_2x1, test_2x2, test_3x3


class Agent:
    def __init__(self):
        self._debug_mode = False

    def set_debug_mode(self):
        self._debug_mode = True

    # noinspection PyMethodMayBeStatic,PyBroadException,PyPep8Naming
    def Solve(self, problem):
        result = "0"
        result = run(problem, self._debug_mode)
        # try:
        #     result = run(problem, self._debug_mode)
        # except Exception:
        #     print("Something went wrong running the agent")
        return result


# noinspection PyListCreation
def classify_figure(figure):
    name = figure.getName()
    path = figure.getPath()
    objects = visual_to_textual(path)
    converted_objects = []
    for obj_name in objects.keys():
        features = objects[obj_name]
        slots = []
        for k in features.keys():
            val = features[k]
            s = Slot(k, val)
            slots.append(s)
        o = Object(obj_name, slots)
        converted_objects.append(o)

    f = Figure(name, converted_objects)
    return f


def run(problem, debug_mode):
    start = timeit.default_timer()

    if not debug_mode:
        print("")
        print("Trying to solve: {0}".format(problem.getName()))

        # Not able to solve this one yet
        to_skip = []
        if problem.getName() in to_skip:
            print("Skipping")
            return "0"
    else:
        # Use while developing to limit the code running
        solving = []

        # solving.append("2x1 Basic Problem 05")
        # solving.append("2x1 Basic Problem 08")
        # solving.append("2x1 Basic Problem 10")
        # solving.append("2x1 Basic Problem 11")
        # solving.append("2x1 Basic Problem 12")
        # solving.append("2x1 Basic Problem 16")
        # solving.append("2x1 Basic Problem 18")
        # solving.append("2x1 Basic Problem 20")
        #
        # solving.append("2x2 Basic Problem 03")
        # solving.append("2x2 Basic Problem 06")  # SLOW: 50s
        # solving.append("2x2 Basic Problem 15")  # SLOW: 30s
        # solving.append("2x2 Basic Problem 17")
        # solving.append("2x2 Basic Problem 18")

        solving.append("3x3 Basic Problem 06")
        # solving.append("3x3 Basic Problem 09")
        # solving.append("3x3 Basic Problem 11")
        # solving.append("3x3 Basic Problem 12")
        # solving.append("3x3 Basic Problem 16")
        # solving.append("3x3 Basic Problem 17")
        # solving.append("3x3 Basic Problem 18")
        # solving.append("3x3 Basic Problem 20")

        if problem.getName() not in solving:
            return "0"
        else:
            print("")
            print("Trying to solve: {0}".format(problem.getName()))

    problem_type = problem.getProblemType()

    figures = {}
    if "Image" in problem_type:
        for figure_name in problem.figures:
            figure = problem.figures[figure_name]
            figures[figure_name] = classify_figure(figure)

        calculate_size_levels(figures)
        remove_extra_info(figures)
    else:
        for figure_name in problem.figures:
            figure = problem.figures[figure_name]
            figures[figure_name] = convert(figure)

    solutions = run_strategies(figures, problem_type)

    stop = timeit.default_timer()
    runtime = stop - start
    print("Runtime: \033[0;34m{0}s\033[00m".format(runtime))

    if len(solutions) == 0:
        # TODO: Log: No answer found
        correct = problem.checkAnswer("0")
        print("\033[0;31mFound NONE answers -- Should be {0}\033[00m".format(correct))
        return "0"
    elif len(solutions) == 1:
        (_, answer) = solutions[0]
        correct = problem.checkAnswer(answer)
        if correct == answer:
            print("\033[0;32mFound correct solution: {0}\033[00m".format(answer))
        else:
            print("\033[0;31mFound solution: {0}, but should be: {1}\033[00m".format(answer, correct))
        return answer
    else:
        ans = ""
        for (_, item) in solutions:
            ans += " " + item
        correct = problem.checkAnswer("0")
        print("\033[0;31mFound more than one answer ( {0} ) -- Should be {1}\033[00m".format(ans, correct))
        return "0"


def run_strategies(figures, problem_type):
    average_number_of_objects = get_number_of_objects(figures)
    max_number_of_objects = get_max_number_of_objects(figures)
    average_number_of_objects_in_given_figures = get_number_of_objects(get_given_figures(figures))

    run_any = False
    run_all = False
    if max_number_of_objects <= 4.01 and average_number_of_objects <= 4.01:
        run_any = True

    if ((float(max_number_of_objects) * 0.25) + (float(average_number_of_objects) * 0.75)) < 3.35:
        run_any = True
        run_all = True

    # Pick different strategies
    # First try using requirements. If not finding any, loosen up

    solutions = []
    strategies = []
    if "2x1" in problem_type:
        strategies = strategies + [Strategy.S2X1_FIXED_POSITIONS]
        if run_any:
            strategies = strategies + [Strategy.S2X1_USE_REQUIREMENTS]
        if run_all:
            strategies = strategies + [Strategy.S2X1_USE_SIZE_EXACT_REQUIREMENTS,
                                       Strategy.S2X1_USE_SIZE_APPROX_REQUIREMENTS]
            strategies = strategies + [Strategy.S2X1_NORMAL_EXACT,
                                       Strategy.S2X1_NORMAL_APPROX,
                                       Strategy.S2X1_STICK_FILL,
                                       Strategy.S2X1_IGNORE_POSITIONAL]
    elif "2x2" in problem_type:
        strategies = strategies + [Strategy.S2X2_FIXED_POSITIONS]
        if run_any:
            strategies = strategies + [Strategy.S2X2_USE_REQUIREMENTS]
        if run_all:
            strategies = strategies + [Strategy.S2X2_USE_SIZE_EXACT_REQUIREMENTS,
                                       Strategy.S2X2_USE_SIZE_APPROX_REQUIREMENTS]
            strategies = strategies + [Strategy.S2X2_NORMAL_EXACT,
                                       Strategy.S2X2_NORMAL_APPROX,
                                       Strategy.S2X2_STICK_FILL,
                                       Strategy.S2X2_IGNORE_POSITIONAL]

    elif "3x3" in problem_type:
        strategies = strategies + [Strategy.S3X3_FIXED_POSITIONS]
        if max_number_of_objects == 2 and float(average_number_of_objects_in_given_figures) == 2.0:
            strategies = strategies + [Strategy.S3X3_SPLIT_OBJECT_TRANSFORMATIONS]
        if run_any:
            strategies = strategies + [Strategy.S3X3_USE_REQUIREMENTS]
        if run_all:
            # strategies = strategies + [Strategy.S3X3_USE_SIZE_EXACT_REQUIREMENTS]
            strategies = strategies + [Strategy.S3X3_NORMAL_EXACT,
                                       Strategy.S3X3_STICK_FILL,
                                       Strategy.S3X3_IGNORE_POSITIONAL,
                                       Strategy.S3X3_SEARCH_FOR_ANGLE_SUMS]

    for s in strategies:
        solutions = run_strategy(figures, s)
        if len(solutions) > 0:
            return solutions
    return solutions


def get_max_number_of_objects(figures):
    max_objects = 0
    for figure_name in figures:
        num = len(figures[figure_name].objects())
        if num > max_objects:
            max_objects = num

    return max_objects


def get_potential_solutions(figures):
    potential_solutions = figures.copy()
    structures = ["A", "B", "C", "D", "E", "F", "G", "H"]
    for to_remove in structures:
        if to_remove in potential_solutions:
            del potential_solutions[to_remove]
    return potential_solutions


def run_strategy(figures, strategy):
    solutions = []
    results = []
    potential_solutions = get_potential_solutions(figures)

    if strategy == Strategy.S2X1_FIXED_POSITIONS:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=True,
                                                        use_requirements=UseRequirements.ALL)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=True,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X1_USE_REQUIREMENTS:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.ALL)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X1_USE_SIZE_EXACT_REQUIREMENTS:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.SIZE_EXACT)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.SIZE_APPROX,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=True)
    elif strategy == Strategy.S2X1_USE_SIZE_APPROX_REQUIREMENTS:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.SIZE_APPROX)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.SIZE_APPROX,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X1_NORMAL_EXACT:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.NONE,
                                                        use_exact=True)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=True)
    elif strategy == Strategy.S2X1_NORMAL_APPROX:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.NONE)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X1_IGNORE_POSITIONAL:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.NONE)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=True,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X1_STICK_FILL:
        transformations = calculate_transformations_2x1(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.SHAPE)
        results = test_2x1(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.SHAPE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=True,
                           use_exact=False)
    elif strategy == Strategy.S2X2_FIXED_POSITIONS:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=True,
                                                        use_requirements=UseRequirements.ALL)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=True,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X2_USE_REQUIREMENTS:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.ALL)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X2_USE_SIZE_EXACT_REQUIREMENTS:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.SIZE_EXACT)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.SIZE_EXACT,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=True)
    elif strategy == Strategy.S2X2_USE_SIZE_APPROX_REQUIREMENTS:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.SIZE_APPROX)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.SIZE_APPROX,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X2_NORMAL_EXACT:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.NONE)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=True)
    elif strategy == Strategy.S2X2_NORMAL_APPROX:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.NONE)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X2_IGNORE_POSITIONAL:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.NONE)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=True,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S2X2_STICK_FILL:
        transformations = calculate_transformations_2x2(figures,
                                                        fix_positions=False,
                                                        use_requirements=UseRequirements.SHAPE)
        results = test_2x2(figures, transformations, potential_solutions,
                           use_requirements=UseRequirements.SHAPE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=True,
                           use_exact=False)
    elif strategy == Strategy.S3X3_FIXED_POSITIONS:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    fix_positions=True,
                                                                    use_requirements=UseRequirements.ALL,
                                                                    use_wrap=False)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=True,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S3X3_USE_REQUIREMENTS:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.ALL,
                                                                    use_wrap=False)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S3X3_IGNORE_POSITIONAL:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.NONE,
                                                                    use_wrap=False)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=True,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S3X3_STICK_FILL:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.SHAPE,
                                                                    use_wrap=False)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.SHAPE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=True,
                           use_exact=False)

    elif strategy == Strategy.S3X3_USE_SIZE_EXACT_REQUIREMENTS:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.SIZE_EXACT,
                                                                    use_wrap=False,
                                                                    use_exact=True)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.SIZE_EXACT,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=True)
    elif strategy == Strategy.S3X3_USE_SIZE_APPROX_REQUIREMENTS:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.SIZE_APPROX,
                                                                    use_wrap=False)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.SIZE_APPROX,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S3X3_NORMAL_EXACT:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.NONE,
                                                                    use_wrap=False)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.NONE,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=True)

    elif strategy == Strategy.S3X3_SPLIT_OBJECT_TRANSFORMATIONS:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.SIZE_APPROX,
                                                                    use_wrap=False,
                                                                    split_transformations=True)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.SIZE_APPROX,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=False)
    elif strategy == Strategy.S3X3_SEARCH_FOR_ANGLE_SUMS:
        transformations, alignments = calculate_transformations_3x3(figures,
                                                                    use_requirements=UseRequirements.ALL,
                                                                    use_wrap=False,
                                                                    use_angle_sum=True)
        results = test_3x3(figures, transformations, potential_solutions, alignments,
                           use_requirements=UseRequirements.ALL,
                           ignore_positional_attributes=False,
                           fix_positions=False,
                           stick_fill=False,
                           use_exact=True)

    if len(results) >= 1:
        if all_the_same(results):
            solutions = [results[0]]
        else:
            solutions = order_results(results, strategy)

    return solutions


def is_same_shape_in_all_figures(figures):
    shape = figures[0].objects()[0].get_slot_filler(get_slot_name_shape())
    for f in figures:
        for o in f.objects():
            if o.get_slot_filler(get_slot_name_shape()) != shape:
                return False
    return True


def find_best(list_of_transformations):
    max_value = 0
    for t in list_of_transformations:
        w = calculate_weight(t)
        if w > max_value:
            max_value = w
    return max


def all_the_same(solutions):
    (_, potential) = solutions[0]
    for (_, solution) in solutions:
        if solution != potential:
            return False
    return True


def order_results(results, strategy):
    if is_2x1_strategy(strategy):
        return order_results_2x1(results)
    elif is_2x2_strategy(strategy):
        return order_results_2x2(results)
    else:
        return []


def order_results_2x2(results):
    weighted = []
    for ((a_to_b, a_to_c), r) in results:
        w1 = calculate_weight(a_to_b)
        w2 = calculate_weight(a_to_c)
        w = w1 + w2
        weighted.append(((a_to_b, a_to_c), r, w))

    max_value = 0
    for (transformation, r, w) in weighted:
        if w > max_value:
            max_value = w

    best = []
    for (transformation, r, w) in weighted:
        if w == max_value:
            best.append((transformation, r))

    if all_the_same(best):
        return [best[0]]
    else:
        return best


def order_results_2x1(results):
    weighted = []
    for (transformation, r) in results:
        w = calculate_weight(transformation)
        weighted.append((transformation, r, w))

    max_value = 0
    for (transformation, r, w) in weighted:
        if w > max_value:
            max_value = w

    best = []
    for (transformation, r, w) in weighted:
        if w == max_value:
            best.append((transformation, r))

    if all_the_same(best):
        return [best[0]]
    else:
        return best


def calculate_weight(transformation):
    total = 0
    for t in transformation.object_transformations():
        if t.add():
            total += 0
        elif t.is_shape_change():
            total += 0
        elif t.delete():
            total += 1
        elif t.is_size_change():
            total += 2
        elif t.is_rotation():
            total += 3
        elif t.is_reflection():
            total += 4
        else:  # Unchanged
            total += 5

    return total