# DO NOT MODIFY THIS FILE.
#
# When you submit your project, an alternate version of this file will be used
# to test your code against the sample Raven's problems in this zip file, as
# well as other problems from the Raven's Test and former students.
#
# Any modifications to this file will not be used when grading your project.
# If you have any questions, please email the TAs.
#
#

import os
import sys
import timeit
from Agent import Agent
from VisualProblemSet import VisualProblemSet


def get_core_problems():
    problems = ["2x1 Basic Problem 01",
                "2x1 Basic Problem 02",
                "2x1 Basic Problem 03",
                "2x1 Basic Problem 04",
                "2x1 Basic Problem 05",
                "2x1 Basic Problem 06",
                "2x1 Basic Problem 07",
                "2x1 Basic Problem 08",
                "2x1 Basic Problem 09",
                "2x1 Basic Problem 10",
                "2x1 Basic Problem 11",
                "2x1 Basic Problem 12",
                "2x1 Basic Problem 13",
                "2x1 Basic Problem 14",
                "2x1 Basic Problem 15",
                "2x1 Basic Problem 16",
                "2x1 Basic Problem 17",
                "2x1 Basic Problem 18",
                "2x1 Basic Problem 19",
                "2x1 Basic Problem 20",
                "2x2 Basic Problem 01",
                "2x2 Basic Problem 02",
                "2x2 Basic Problem 03",
                "2x2 Basic Problem 04",
                "2x2 Basic Problem 05",
                "2x2 Basic Problem 06",
                "2x2 Basic Problem 07",
                "2x2 Basic Problem 08",
                "2x2 Basic Problem 09",
                "2x2 Basic Problem 10",
                "2x2 Basic Problem 11",
                "2x2 Basic Problem 12",
                "2x2 Basic Problem 13",
                "2x2 Basic Problem 14",
                "2x2 Basic Problem 15",
                "2x2 Basic Problem 16",
                "2x2 Basic Problem 17",
                "2x2 Basic Problem 18",
                "2x2 Basic Problem 19",
                "2x2 Basic Problem 20",
                "3x3 Basic Problem 01",
                "3x3 Basic Problem 02",
                "3x3 Basic Problem 03",
                "3x3 Basic Problem 04",
                "3x3 Basic Problem 05",
                "3x3 Basic Problem 06",
                "3x3 Basic Problem 07",
                "3x3 Basic Problem 08",
                "3x3 Basic Problem 09",
                "3x3 Basic Problem 10",
                "3x3 Basic Problem 11",
                "3x3 Basic Problem 12",
                "3x3 Basic Problem 13",
                "3x3 Basic Problem 14",
                "3x3 Basic Problem 15",
                "3x3 Basic Problem 16",
                "3x3 Basic Problem 17",
                "3x3 Basic Problem 18",
                "3x3 Basic Problem 19",
                "3x3 Basic Problem 20"
                ]
    return problems


def get_expected_success_problems():
    problems = ["2x1 Basic Problem 01",
                "2x1 Basic Problem 02",
                "2x1 Basic Problem 03",
                "2x1 Basic Problem 04",
                # "2x1 Basic Problem 05",
                "2x1 Basic Problem 06",
                "2x1 Basic Problem 07",
                # "2x1 Basic Problem 08",
                "2x1 Basic Problem 09",
                # "2x1 Basic Problem 10",
                # "2x1 Basic Problem 11",
                # "2x1 Basic Problem 12",
                "2x1 Basic Problem 13",
                "2x1 Basic Problem 14",
                "2x1 Basic Problem 15",
                "2x1 Basic Problem 16",
                "2x1 Basic Problem 17",
                # "2x1 Basic Problem 18",
                "2x1 Basic Problem 19",
                # "2x1 Basic Problem 20",
                "2x1 Challenge Problem 01",
                "2x1 Challenge Problem 02",
                # "2x1 Challenge Problem 03",
                # "2x1 Challenge Problem 04",
                # "2x1 Challenge Problem 05",
                "2x1 Challenge Problem 06",
                # "2x1 Classmates' Problem 01",
                "2x1 Classmates' Problem 02",
                # "2x1 Classmates' Problem 03",
                # "2x1 Classmates' Problem 04",

                "2x2 Basic Problem 01",
                "2x2 Basic Problem 02",
                # "2x2 Basic Problem 03",
                "2x2 Basic Problem 04",
                "2x2 Basic Problem 05",
                # "2x2 Basic Problem 06",
                "2x2 Basic Problem 07",
                "2x2 Basic Problem 08",
                "2x2 Basic Problem 09",
                "2x2 Basic Problem 10",
                "2x2 Basic Problem 11",
                "2x2 Basic Problem 12",
                "2x2 Basic Problem 13",
                "2x2 Basic Problem 14",
                # "2x2 Basic Problem 15",
                "2x2 Basic Problem 16",
                # "2x2 Basic Problem 17",
                "2x2 Basic Problem 18",
                "2x2 Basic Problem 19",
                "2x2 Basic Problem 20",
                # "2x2 Challenge Problem 01",
                # "2x2 Challenge Problem 02",
                "2x2 Challenge Problem 03",
                # "2x2 Challenge Problem 04",
                "2x2 Challenge Problem 05",
                "2x2 Classmates' Problem 01",
                "2x2 Classmates' Problem 02",
                "2x2 Classmates' Problem 03",
                # "2x2 Classmates' Problem 04"
                "3x3 Basic Problem 01",
                "3x3 Basic Problem 02",
                "3x3 Basic Problem 03",
                "3x3 Basic Problem 04",
                "3x3 Basic Problem 05",
                # "3x3 Basic Problem 06",
                "3x3 Basic Problem 07",
                "3x3 Basic Problem 08",
                # "3x3 Basic Problem 09",
                "3x3 Basic Problem 10",
                # "3x3 Basic Problem 11",
                # "3x3 Basic Problem 12",
                "3x3 Basic Problem 13",
                "3x3 Basic Problem 14",
                "3x3 Basic Problem 15",
                # "3x3 Basic Problem 16",
                # "3x3 Basic Problem 17",
                # "3x3 Basic Problem 18",
                "3x3 Basic Problem 19",
                # "3x3 Basic Problem 20",
                # "3x3 Classmates' Problem 01",
                "3x3 Classmates' Problem 02",
                # "3x3 Classmates' Problem 03",
                ]

    return problems


# The main driver file for Project3. You may edit this file to change which
# problems your Agent addresses while debugging and designing, but you should
# not depend on changes to this file for final execution of your project. Your
# project will be graded using our own version of this file.
def main():
    start = timeit.default_timer()
    debug_mode = False

    # noinspection PyBroadException
    try:
        if sys.argv[1] == "debug":
            debug_mode = True
    except:
        pass

    if debug_mode:
        print("")
        print("\033[0;34mDEBUG-SESSION\033[00m")

    sets=[] # The variable 'sets' stores multiple problem sets.
            # Each problem set comes from a different folder in /Problems/
            # Additional sets of problems will be used when grading projects.
            # You may also write your own problems.

    for file in os.listdir("Problems (Image Data)"): # One problem set per folder in /Problems/
        if file != '.DS_Store':
            newSet = VisualProblemSet(file)       # Each problem set is named after the folder in /Problems/
            sets.append(newSet)
            for problem in os.listdir("Problems (Image Data)" + os.sep + file):  # Each file in the problem set folder becomes a problem in that set.
                if problem != '.DS_Store':
                    newSet.addProblem(file, problem)

    # Initializing problem-solving agent from Agent.java
    agent=Agent()   # Your agent will be initialized with its default constructor.
                    # You may modify the default constructor in Agent.java

    # Running agent against each problem set
    results=open("Results.txt","w")     # Results will be written to Results.txt.
                                        # Note that each run of the program will overwrite the previous results.
                                        # Do not write anything else to Results.txt during execution of the program.

    expected_successes = get_expected_success_problems()
    core_problems = get_core_problems()
    unexpected_failures = []
    unexpected_successes = []
    total_problems = 0
    total_successes = 0
    total_core_successes = 0

    if debug_mode:
        agent.set_debug_mode()

    for set in sets:
        results.write("%s\n" % set.getName())   # Your agent will solve one problem set at a time.
        results.write("%s\n" % "-----------")   # Problem sets will be individually categorized in the results file.

        for problem in set.getProblems():   # Your agent will solve one problem at a time.
            problem.setAnswerReceived(agent.Solve(problem))     # The problem will be passed to your agent as a RavensProblem object as a parameter to the Solve method
                                                                # Your agent should return its answer at the conclusion of the execution of Solve.
                                                                # Note that if your agent makes use of RavensProblem.check to check its answer, the answer passed to check() will be used.
                                                                # Your agent cannot change its answer once it has checked its answer.

            result=problem.getName() + ": " + problem.getGivenAnswer() + " " + problem.getCorrect() + " (Correct Answer: " + problem.checkAnswer("") + ")"

            # Metrics
            if not debug_mode:
                total_problems += 1
                correct = "Correct" == problem.getCorrect()
                if correct:
                    total_successes += 1

                if problem.getName() in core_problems:
                    if correct:
                        total_core_successes += 1

                if problem.getName() in expected_successes:
                    if not correct:
                        unexpected_failures.append(problem.getName())
                else:
                    if correct:
                        unexpected_successes.append(problem.getName())

            results.write("%s\n" % result)
        results.write("\n")

    if not debug_mode:
        stop = timeit.default_timer()
        runtime = stop - start
        print("Total Runtime: \033[0;34m{0}s\033[00m".format(runtime))
        print("")
        print("")
        if len(unexpected_failures) == 0 and len(unexpected_successes) == 0:

            print("\033[0;32m*****************************")
            print("**   RESULT: As Expected   **")
            print("**                         **")
            print("**   Core correct: {0}      **".format(total_core_successes))
            print("**   Core problems: {0}     **".format(len(core_problems)))
            print("**   Percentage: {0}        **".format(int(float(total_core_successes) / len(core_problems) * 100)))
            print("**                         **")
            print("**   Total correct: {0}     **".format(total_successes))
            print("**   Total problems: {0}    **".format(total_problems))
            print("**   Percentage: {0}        **".format(int(float(total_successes) / total_problems * 100)))
            print("**                         **")
            print("*****************************\033[00m")
        else:
            print("\033[0;31mUNEXPECTED RESULTS:")
            if len(unexpected_failures) > 0:
                print("Unexpected failures:")
                for f in unexpected_failures:
                    print(f)
            if len(unexpected_successes) > 0:
                print("Unexpected successes:")
                for f in unexpected_successes:
                    print(f)
            print("\033[00m")

if __name__ == "__main__":
    main()