import copy
from Common import get_objects_in_order, FigureTransformation, generate_object_names, \
    rename_object_reference_in_object_list, convert_attributes_to_dictionary, SlotTransformation, Slot, \
    ObjectTransformation, get_slot_name_shape, get_slot_name_angle, find_shape, get_slot_filler_circle, \
    generate_figure_combinations, get_common_figure_structure, Figure, get_positional_figure, get_slot_name_fill, \
    get_all_shapes_with_sides_only_names, replace_shape_type, get_common_shape_structure, is_transformations_identical, \
    UseRequirements, is_any_transformation_identical, get_common_figure_size, get_slot_name_size, get_number_of_objects, \
    get_given_figures, grow_size, has_symmetrically_shaped_number_of_objects, \
    is_all_shapes_of_same_shape_and_size_and_angle


def calculate_transformations_3x3(figures,
                                  fix_positions=False,
                                  use_requirements=UseRequirements.ALL,
                                  use_wrap=True,
                                  split_transformations=False,
                                  use_exact=False,
                                  use_angle_sum=False):
    figure_a = figures["A"]
    figure_b = figures["B"]
    figure_c = figures["C"]
    figure_d = figures["D"]
    figure_e = figures["E"]
    figure_f = figures["F"]
    figure_g = figures["G"]
    figure_h = figures["H"]

    if fix_positions:
        combinations_a_to_b = get_common_figure_structure(figure_a, figure_b, use_exact)
        combinations_b_to_c = get_common_figure_structure(figure_b, figure_c, use_exact)
        combinations_c_to_a = get_common_figure_structure(figure_c, figure_a, use_exact)

        combinations_d_to_e = get_common_figure_structure(figure_d, figure_e, use_exact)
        combinations_e_to_f = get_common_figure_structure(figure_e, figure_f, use_exact)
        combinations_f_to_d = get_common_figure_structure(figure_f, figure_d, use_exact)

        combinations_g_to_h = get_common_figure_structure(figure_g, figure_h, use_exact)

        combinations_a_to_d = get_common_figure_structure(figure_a, figure_d, use_exact)
        combinations_d_to_g = get_common_figure_structure(figure_d, figure_g, use_exact)
        combinations_g_to_a = get_common_figure_structure(figure_g, figure_a, use_exact)

        combinations_b_to_e = get_common_figure_structure(figure_b, figure_e, use_exact)
        combinations_e_to_h = get_common_figure_structure(figure_e, figure_h, use_exact)
        combinations_h_to_b = get_common_figure_structure(figure_h, figure_b, use_exact)

        combinations_c_to_f = get_common_figure_structure(figure_c, figure_f, use_exact)

    elif use_requirements == UseRequirements.SHAPE:
        combinations_a_to_b = get_common_shape_structure(figure_a, figure_b)
        combinations_b_to_c = get_common_shape_structure(figure_b, figure_c)
        combinations_c_to_a = get_common_shape_structure(figure_c, figure_a)

        combinations_d_to_e = get_common_shape_structure(figure_d, figure_e)
        combinations_e_to_f = get_common_shape_structure(figure_e, figure_f)
        combinations_f_to_d = get_common_shape_structure(figure_f, figure_d)

        combinations_g_to_h = get_common_shape_structure(figure_g, figure_h)

        combinations_a_to_d = get_common_shape_structure(figure_a, figure_d)
        combinations_d_to_g = get_common_shape_structure(figure_d, figure_g)
        combinations_g_to_a = get_common_shape_structure(figure_g, figure_a)

        combinations_b_to_e = get_common_shape_structure(figure_b, figure_e)
        combinations_e_to_h = get_common_shape_structure(figure_e, figure_h)
        combinations_h_to_b = get_common_shape_structure(figure_h, figure_b)

        combinations_c_to_f = get_common_shape_structure(figure_c, figure_f)
    elif use_requirements in [UseRequirements.SIZE_APPROX, UseRequirements.SIZE_EXACT]:
        combinations_a_to_b = get_common_figure_size(figure_a, figure_b, use_exact)
        combinations_b_to_c = get_common_figure_size(figure_b, figure_c, use_exact)
        combinations_c_to_a = get_common_figure_size(figure_c, figure_a, use_exact)

        combinations_d_to_e = get_common_figure_size(figure_d, figure_e, use_exact)
        combinations_e_to_f = get_common_figure_size(figure_e, figure_f, use_exact)
        combinations_f_to_d = get_common_figure_size(figure_f, figure_d, use_exact)

        combinations_g_to_h = get_common_figure_size(figure_g, figure_h, use_exact)

        combinations_a_to_d = get_common_figure_size(figure_a, figure_d, use_exact)
        combinations_d_to_g = get_common_figure_size(figure_d, figure_g, use_exact)
        combinations_g_to_a = get_common_figure_size(figure_g, figure_a, use_exact)

        combinations_b_to_e = get_common_figure_size(figure_b, figure_e, use_exact)
        combinations_e_to_h = get_common_figure_size(figure_e, figure_h, use_exact)
        combinations_h_to_b = get_common_figure_size(figure_h, figure_b, use_exact)

        combinations_c_to_f = get_common_figure_size(figure_c, figure_f, use_exact)
    else:
        combinations_a_to_b = generate_figure_combinations(figure_a, figure_b)
        combinations_b_to_c = generate_figure_combinations(figure_b, figure_c)
        combinations_c_to_a = generate_figure_combinations(figure_c, figure_a)

        combinations_d_to_e = generate_figure_combinations(figure_d, figure_e)
        combinations_e_to_f = generate_figure_combinations(figure_e, figure_f)
        combinations_f_to_d = generate_figure_combinations(figure_f, figure_d)

        combinations_g_to_h = generate_figure_combinations(figure_g, figure_h)

        combinations_a_to_d = generate_figure_combinations(figure_a, figure_d)
        combinations_d_to_g = generate_figure_combinations(figure_d, figure_g)
        combinations_g_to_a = generate_figure_combinations(figure_g, figure_a)

        combinations_b_to_e = generate_figure_combinations(figure_b, figure_e)
        combinations_e_to_h = generate_figure_combinations(figure_e, figure_h)
        combinations_h_to_b = generate_figure_combinations(figure_h, figure_b)

        combinations_c_to_f = generate_figure_combinations(figure_c, figure_f)

    transformations = {"AB": calculate_transformations(figure_a, figure_b, combinations_a_to_b,
                                                       use_requirements, use_exact),
                       "BC": calculate_transformations(figure_b, figure_c, combinations_b_to_c,
                                                       use_requirements, use_exact),
                       "CA": calculate_transformations(figure_c, figure_a, combinations_c_to_a,
                                                       use_requirements, use_exact),
                       "DE": calculate_transformations(figure_d, figure_e, combinations_d_to_e,
                                                       use_requirements, use_exact),
                       "EF": calculate_transformations(figure_e, figure_f, combinations_e_to_f,
                                                       use_requirements, use_exact),
                       "FD": calculate_transformations(figure_f, figure_d, combinations_f_to_d,
                                                       use_requirements, use_exact),
                       "GH": calculate_transformations(figure_g, figure_h, combinations_g_to_h,
                                                       use_requirements, use_exact),
                       "AD": calculate_transformations(figure_a, figure_d, combinations_a_to_d,
                                                       use_requirements, use_exact),
                       "DG": calculate_transformations(figure_d, figure_g, combinations_d_to_g,
                                                       use_requirements, use_exact),
                       "GA": calculate_transformations(figure_g, figure_a, combinations_g_to_a,
                                                       use_requirements, use_exact),
                       "BE": calculate_transformations(figure_b, figure_e, combinations_b_to_e,
                                                       use_requirements, use_exact),
                       "EH": calculate_transformations(figure_e, figure_h, combinations_e_to_h,
                                                       use_requirements, use_exact),
                       "HB": calculate_transformations(figure_h, figure_b, combinations_h_to_b,
                                                       use_requirements, use_exact),
                       "CF": calculate_transformations(figure_c, figure_f, combinations_c_to_f,
                                                       use_requirements, use_exact)}
    alignments = calculate_aligned_transformations(transformations, use_requirements, use_exact)
    general_horizontal_transformations = find_horizontal_general_transformation(transformations,
                                                                                use_requirements,
                                                                                use_wrap,
                                                                                use_exact)
    general_vertical_transformations = find_vertical_general_transformation(transformations,
                                                                            use_requirements,
                                                                            use_wrap,
                                                                            use_exact)
    if len(general_horizontal_transformations) > 0 and len(general_vertical_transformations) > 0:
        transformations = {"HOR": general_horizontal_transformations,
                           "VER": general_vertical_transformations}
        alignments = "HOR", "VER"
    else:
        if is_grow_general(figures, ["A", "B", "C"], ["D", "E", "F"], ["G", "H"]):
            alignments = "HOR_GROW", alignments[1]
            transformations["HOR_GROW"] = get_general_grow_transformation()

        if is_grow_general(figures, ["A", "D", "G"], ["B", "E", "H"], ["C", "F"]):
            alignments = alignments[0], "VER_GROW"
            transformations["VER_GROW"] = get_general_grow_transformation()

    if split_transformations:
        # TODO: Split transformations

        # TODO: Align splitted parts

        # TODO: Merge aligned and splitted parts
        large_sets, small_sets = split_all_transformations_based_on_size(transformations, use_requirements, use_exact)

        alignments_large = calculate_aligned_transformations(large_sets, use_requirements, use_exact)
        alignments_small = calculate_aligned_transformations(small_sets, use_requirements, use_exact)

        transformations, alignments = generate_new_transformations(large_sets,
                                                                   small_sets,
                                                                   alignments_large,
                                                                   alignments_small)

    if use_angle_sum:
        if is_angle_sum(figures, ["A", "B", "C"], ["D", "E", "F"]):
            alignments = "HOR_ANGLE_SUM", alignments[1]
            transformations["HOR_ANGLE_SUM"] = get_angle_sum_transformation()

        if is_angle_sum(figures, ["A", "D", "G"], ["B", "E", "H"]):
            alignments = alignments[0], "VER_ANGLE_SUM"
            transformations["VER_ANGLE_SUM"] = get_angle_sum_transformation()

    return transformations, alignments


def is_angle_sum(figures, sequence1, sequence2):
    if float(get_number_of_objects(get_given_figures(figures))) == 1.0:
        angle11 = int(figures[sequence1[0]].objects()[0].get_slot_filler(get_slot_name_angle()))
        angle12 = int(figures[sequence1[1]].objects()[0].get_slot_filler(get_slot_name_angle()))
        angle13 = int(figures[sequence1[2]].objects()[0].get_slot_filler(get_slot_name_angle()))

        angle21 = int(figures[sequence2[0]].objects()[0].get_slot_filler(get_slot_name_angle()))
        angle22 = int(figures[sequence2[1]].objects()[0].get_slot_filler(get_slot_name_angle()))
        angle23 = int(figures[sequence2[2]].objects()[0].get_slot_filler(get_slot_name_angle()))

        if ((angle11 + angle12) == angle13) and ((angle21 + angle22) == angle23):
            return True
        else:
            return False
    else:
        return False


def get_angle_sum_transformation():
    trans = FigureTransformation([], None)
    trans.set_3x_angle_sum()
    return [trans]


def generate_new_transformations(large_sets, small_sets, alignments_large, alignments_small):
    l_hor, l_ver = alignments_large
    s_hor, s_ver = alignments_small

    small_hor = small_sets[s_hor]
    small_ver = small_sets[s_ver]
    large_hor = large_sets[l_hor]
    large_ver = large_sets[l_ver]

    ver = []
    hor = []

    for s_h in small_hor:
        objects_small = s_h.object_transformations()
        for l_h in large_hor:
            objects_large = l_h.object_transformations()
            objects = objects_small + objects_large
            hor.append(FigureTransformation(objects, None))

    for s_v in small_ver:
        objects_small = s_v.object_transformations()
        for l_v in large_ver:
            objects_large = l_v.object_transformations()
            objects = objects_small + objects_large
            ver.append(FigureTransformation(objects, None))

    return {"HOR": hor,
            "VER": ver}, ("HOR", "VER")


def split_all_transformations_based_on_size(transformation_sets, use_requirements, use_exact):
    large_sets = {}
    small_sets = {}
    for key in transformation_sets:
        transformation_set = transformation_sets[key]
        large_set, small_set = split_transformation_set(transformation_set, use_requirements, use_exact)
        large_sets[key] = large_set
        small_sets[key] = small_set

    return large_sets, small_sets


def split_transformation_set(transformation_set, use_requirements, use_exact):
    large = []
    small = []
    for transformation in transformation_set:
        l, s = split_transformation(transformation)
        large.append(l)
        small.append(s)

    large = remove_identical_transformations(large, use_requirements, use_exact)
    small = remove_identical_transformations(small, use_requirements, use_exact)

    return large, small


def split_transformation(transformation):
    large_objects = []
    small_objects = []

    for obj in transformation.object_transformations():
        if is_small_size(obj.based_on_size()):
            small_objects.append(obj)
        else:
            large_objects.append(obj)

    l = FigureTransformation(large_objects, None)
    s = FigureTransformation(small_objects, None)

    return l, s


def is_small_size(size):
    return size in ["tiny", "very-small", "small"]


def find_horizontal_general_transformation(transformation_sets, use_requirements, use_wrap, use_exact):
    horizontal_transformations = ["AB", "BC", "DE", "EF", "GH"]
    if use_wrap:
        horizontal_transformations.append("CA")
        horizontal_transformations.append("FD")

    for transformation_set_key in horizontal_transformations:
        transformation_set = transformation_sets[transformation_set_key]
        for candidate in transformation_set:
            if is_transformation_in_all_sets(candidate,
                                             transformation_sets,
                                             horizontal_transformations,
                                             use_requirements,
                                             use_exact):
                return [candidate]

    return []


def is_grow_general(figures, sequence1, sequence2, sequence3):
    if float(get_number_of_objects(get_given_figures(figures))) == 1.0:
        if not is_sequence_growing(figures, sequence1):
            return False
        if not is_sequence_growing(figures, sequence2):
            return False
        if not is_sequence_growing(figures, sequence3):
            return False
        else:
            return True
    else:
        return False


def is_sequence_growing(figures, sequence):
    for i in range(len(sequence) - 1):
        fig1 = figures[sequence[i]]
        fig2 = figures[sequence[i+1]]
        size1 = fig1.objects()[0].get_slot_filler(get_slot_name_size())
        size2 = fig2.objects()[0].get_slot_filler(get_slot_name_size())
        if size2 != grow_size(size1):
            return False
    return True


def get_general_grow_transformation():
    object_transformation = ObjectTransformation("Grow", False, False, [], [], [], [], "", "")
    object_transformation.set_is_grow_type()
    return [FigureTransformation([object_transformation], None)]


def find_vertical_general_transformation(transformation_sets, use_requirements, use_wrap, use_exact):
    vertical_transformations = get_vertical_transformation_keys(use_wrap)

    for transformation_set_key in vertical_transformations:
        transformation_set = transformation_sets[transformation_set_key]
        for candidate in transformation_set:
            if is_transformation_in_all_sets(candidate,
                                             transformation_sets,
                                             vertical_transformations,
                                             use_requirements,
                                             use_exact):
                return [candidate]

    return []


def get_horizontal_transformation_keys(use_wrap):
    horizontal_transformations = ["AD", "DG", "BE", "EH", "CF"]
    if use_wrap:
        horizontal_transformations.append("GA")
        horizontal_transformations.append("HB")
    return horizontal_transformations


def get_vertical_transformation_keys(use_wrap):
    horizontal_transformations = ["AD", "DG", "BE", "EH", "CF"]
    if use_wrap:
        horizontal_transformations.append("GA")
        horizontal_transformations.append("HB")
    return horizontal_transformations


def is_transformation_in_all_sets(candidate, transformation_sets, keys_to_use, use_requirements, use_exact):
    for transformation_set_key in keys_to_use:
        transformation_set = transformation_sets[transformation_set_key]
        found = False
        for transformation in transformation_set:
            if is_transformations_identical(candidate, transformation, use_requirements, use_exact):
                found = True
                break

        if not found:
            return False

    return True


def calculate_aligned_transformations(transformations, use_requirements, use_exact):
    vertical = "EH"
    horizontal = "EF"

    if is_any_transformation_identical(transformations["GH"], transformations["DE"], use_requirements, use_exact):
        horizontal = "EF"
    elif is_any_transformation_identical(transformations["GH"], transformations["EF"], use_requirements, use_exact):
        horizontal = "FD"
    elif is_any_transformation_identical(transformations["GH"], transformations["FD"], use_requirements, use_exact):
        horizontal = "DE"

    if is_any_transformation_identical(transformations["CF"], transformations["BE"], use_requirements, use_exact):
        vertical = "EH"
    elif is_any_transformation_identical(transformations["CF"], transformations["EH"], use_requirements, use_exact):
        vertical = "HB"
    elif is_any_transformation_identical(transformations["CF"], transformations["HB"], use_requirements, use_exact):
        vertical = "BE"

    return horizontal, vertical


def calculate_transformations_2x2(figures, fix_positions=False, use_requirements=UseRequirements.NONE, use_exact=False):
    figure_a = figures["A"]
    figure_b = figures["B"]
    figure_c = figures["C"]
    if fix_positions:
        combinations_a_to_b = get_common_figure_structure(figure_a, figure_b, use_requirements)
        combinations_a_to_c = get_common_figure_structure(figure_a, figure_c, use_requirements)
    elif use_requirements == UseRequirements.SHAPE:
        combinations_a_to_b = get_common_shape_structure(figure_a, figure_b)
        combinations_a_to_c = get_common_shape_structure(figure_a, figure_c)
    else:
        combinations_a_to_b = generate_figure_combinations(figure_a, figure_b)
        combinations_a_to_c = generate_figure_combinations(figure_a, figure_c)

    a_to_b = calculate_transformations(figure_a, figure_b, combinations_a_to_b, use_requirements, use_exact)
    a_to_c = calculate_transformations(figure_a, figure_c, combinations_a_to_c, use_requirements, use_exact)

    transformations = []
    for t_b in a_to_b:
        for t_c in a_to_c:
            transformations.append((t_b, t_c))
    return transformations


def calculate_transformations_2x1(figures, fix_positions=False, use_requirements=UseRequirements.NONE, use_exact=False):
    figure_a = figures["A"]
    figure_b = figures["B"]
    if fix_positions:
        combinations = get_common_figure_structure(figure_a, figure_b, use_exact)
    elif use_requirements == UseRequirements.SHAPE:
        combinations = get_common_shape_structure(figure_a, figure_b)
    else:
        combinations = generate_figure_combinations(figure_a, figure_b)
    return calculate_transformations(figure_a, figure_b, combinations, use_requirements, use_exact)


def calculate_transformations(figure_a, figure_b, combinations, use_requirements, use_exact):
    transformations = []

    for (order_a, order_b) in combinations:
        a_s = copy.deepcopy(get_objects_in_order(figure_a.objects(), order_a))
        b_s = copy.deepcopy(get_objects_in_order(figure_b.objects(), order_b))
        trans, structure_figure = calculate_transformation(a_s, b_s)

        for t in trans:
            transformation = FigureTransformation(t, structure_figure)
            if transformation.check_general():
                transformation.set_general()
            transformations.append(transformation)

    # TODO: Detect if row added
    if is_row_added(figure_a, figure_b):
        t = FigureTransformation([], None)
        t.set_add_row()
        transformations.append(t)
        # TODO: Detect shape changes too
    elif is_row_removed(figure_a, figure_b):
        t = FigureTransformation([], None)
        t.set_remove_row()
        transformations.append(t)
        # TODO: Detect shape changes too

    # TODO: Detect if column is added
    if is_column_added(figure_a, figure_b):
        t = FigureTransformation([], None)
        t.set_add_column()
        transformations.append(t)
        # TODO: Detect shape changes too
    elif is_column_removed(figure_a, figure_b):
        t = FigureTransformation([], None)
        t.set_remove_column()
        transformations.append(t)
        # TODO: Detect shape changes too

    transformations = remove_identical_transformations(transformations, use_requirements, use_exact)
    return transformations


def remove_identical_transformations(transformations, use_requirements, use_exact):
    output = []
    for trans in transformations:
        if not contains_transformation(trans, output, use_requirements, use_exact):
            output.append(trans)
    if len(output) != len(transformations):
        pass

    return output


def is_row_added(figure_a, figure_b):
    if not has_symmetrically_shaped_number_of_objects(figure_a):
        return False
    if not has_symmetrically_shaped_number_of_objects(figure_b):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_a):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_b):
        return False

    # TODO: Implement
    return False


def is_column_added(figure_a, figure_b):
    if not has_symmetrically_shaped_number_of_objects(figure_a):
        return False
    if not has_symmetrically_shaped_number_of_objects(figure_b):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_a):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_b):
        return False

    # TODO: Implement
    return False


def is_row_removed(figure_a, figure_b):
    if not has_symmetrically_shaped_number_of_objects(figure_a):
        return False
    if not has_symmetrically_shaped_number_of_objects(figure_b):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_a):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_b):
        return False

    # TODO: Implement
    return False


def is_column_removed(figure_a, figure_b):
    if not has_symmetrically_shaped_number_of_objects(figure_a):
        return False
    if not has_symmetrically_shaped_number_of_objects(figure_b):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_a):
        return False
    if not is_all_shapes_of_same_shape_and_size_and_angle(figure_b):
        return False

    # TODO: Implement
    return False


def contains_transformation(transformation, transformations, use_requirements, use_exact):
    for trans in transformations:
        if is_transformations_identical(trans, transformation, use_requirements, use_exact):
            return True

    return False


def has_all_objects_of_side_type(ordered_objects_from_a, ordered_objects_from_b):
    shape_names_to_detect = get_all_shapes_with_sides_only_names()
    shapes_found = []
    for l in [ordered_objects_from_a, ordered_objects_from_b]:
        for o in l:

            if o is False:
                return False
            else:
                shape = o.get_slot_filler(get_slot_name_shape())
                if shape not in shape_names_to_detect:
                    return False
                else:
                    if shape not in shapes_found:
                        shapes_found.append(shape)
    if len(shapes_found) > 1:
        return True
    else:
        return False


def calculate_transformation(ordered_objects_from_a, ordered_objects_from_b):
    if has_all_objects_of_side_type(ordered_objects_from_a, ordered_objects_from_b):
        for l in [ordered_objects_from_a, ordered_objects_from_b]:
            for o in l:
                replace_shape_type(o)

    object_transformations = []
    length = len(ordered_objects_from_a)

    # Rename objects
    object_names = generate_object_names()
    for i in range(length):
        object_in_a = ordered_objects_from_a[i]
        object_in_b = ordered_objects_from_b[i]
        name = next(object_names)

        if object_in_a is not False:
            rename_object_reference_in_object_list(ordered_objects_from_a, object_in_a.name(), name)

        if object_in_b is not False:
            rename_object_reference_in_object_list(ordered_objects_from_b, object_in_b.name(), name)

    structural_figure = get_positional_figure(Figure("Structure",
                                                     [o for o in ordered_objects_from_a if o is not False]))

    for i in range(length):
        object_in_a = ordered_objects_from_a[i]
        object_in_b = ordered_objects_from_b[i]

        if object_in_a is False:
            o = add_object(object_in_b)
            object_transformations = add_one_to_list(object_transformations, o)
        elif object_in_b is False:
            o = delete_object(object_in_a)
            object_transformations = add_one_to_list(object_transformations, o)
        else:
            o_s = transform_object(object_in_a, object_in_b)
            object_transformations = add_choices_to_list(object_transformations, o_s)

    new_relative_add_transformations = generate_relative_value_add(object_transformations, ordered_objects_from_a)

    if len(new_relative_add_transformations) > 0:
        for t in new_relative_add_transformations:
            object_transformations.append(t)

    return object_transformations, structural_figure


def generate_relative_value_add(object_transformations, ordered_objects_from_a):
    new_transformations = []
    for trans in object_transformations:
        adds = []
        for t in trans:
            if t.add():
                adds.append(t)

        if len(adds) > 0:
            new_trans = copy.deepcopy(trans)
            found = False
            for t in new_trans:
                if t.add():
                    for add_slot in t.to_add():
                        for o in ordered_objects_from_a:
                            if o is not False:
                                if o.get_slot_filler(add_slot.name()) == add_slot.filler_to():
                                    add_slot.set_filler_to_be_added_from_object(o.name())
                                    found = True
            if found:
                new_transformations.append(new_trans)
    return new_transformations


def add_choices_to_list(list_of_transformations, choices):
    new_list = []
    if len(list_of_transformations) == 0:
        for c in choices:
            new_list.append([c])
    else:
        for c in choices:
            for t in list_of_transformations:
                combination = copy.deepcopy(t)
                combination.append(c)
                new_list.append(combination)
    return new_list


def add_one_to_list(list_of_transformations, transformation):
    if len(list_of_transformations) == 0:
        list_of_transformations.append([transformation])
    else:
        for t in list_of_transformations:
            t.append(transformation)

    return list_of_transformations


def has_angle_attributes(objects_in_a, objects_in_b):
    length = len(objects_in_a)
    for i in range(length):
        object_in_a = objects_in_a[i]
        object_in_b = objects_in_b[i]
        if has_angle_attribute(object_in_a, object_in_b):
            return True
    return False


def has_angle_attribute(object_in_a, object_in_b):
    if object_in_a is False:
        pass
    elif object_in_b is False:
        pass
    else:
        attributes_in_a = convert_attributes_to_dictionary(object_in_a.slots())
        attributes_in_b = convert_attributes_to_dictionary(object_in_b.slots())

        angle_attr = get_slot_name_angle()

        if angle_attr in attributes_in_a and angle_attr in attributes_in_b:
            return True
        else:
            return False


def has_reflection(object_in_a, object_in_b):
    if object_in_a is False:
        pass
    elif object_in_b is False:
        pass
    else:
        attributes_in_a = convert_attributes_to_dictionary(object_in_a.slots())
        attributes_in_b = convert_attributes_to_dictionary(object_in_b.slots())

        angle_attr = get_slot_name_angle()
        shape_in_a = attributes_in_a[get_slot_name_shape()]

        if shape_in_a != attributes_in_b[get_slot_name_shape()]:
            return False, False

        if angle_attr in attributes_in_a and angle_attr in attributes_in_b:
            from_angle = attributes_in_a[angle_attr]
            to_angle = attributes_in_b[angle_attr]
            return (has_horizontal_reflection(shape_in_a, from_angle, to_angle),
                    has_vertical_reflection(shape_in_a, from_angle, to_angle))

    return False, False


def calculate_angle_diff(a, b):
    attributes_in_a = convert_attributes_to_dictionary(a.slots())
    attributes_in_b = convert_attributes_to_dictionary(b.slots())

    angle_attr = get_slot_name_angle()

    from_angle = int(float(attributes_in_a[angle_attr]))
    to_angle = int(float(attributes_in_b[angle_attr]))

    return to_angle - from_angle


def has_horizontal_reflection(shape, from_angle, to_angle):
    f = int(float(from_angle))
    t = int(float(to_angle))
    # C = 180 + 2B - A  -> B = 0 for hor, B = 90 for ver

    # Add more shapes here
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if (0 - f) % 360 == t:
            return True
        else:
            return False
    elif shape in ["right-triangle"]:
        if (90 - f) % 360 == t:
            return True
        else:
            return False
    else:
        if (180 - f) % 360 == t:
            return True
        else:
            return False


def has_vertical_reflection(shape, from_angle, to_angle):
    f = int(float(from_angle))
    t = int(float(to_angle))
    # C = 180 + 2B - A  -> B = 0 for hor, B = 90 for ver

    # Add more shapes here
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if (180 - f) % 360 == t:
            return True
        else:
            return False
    elif shape in ["right-triangle"]:
        if (270 - f) % 360 == t:
            return True
        else:
            return False
    else:
        if (0 - f) % 360 == t:
            return True
        else:
            return False


def generate_reflections(transformation, objects_in_a, objects_in_b):
    transformations = []
    t = copy.deepcopy(transformation)

    length = len(t.object_transformations())

    for i in range(length):
        a = objects_in_a[i]
        b = objects_in_b[i]
        hor, ver = has_reflection(a, b)

        if hor or ver:
            trans = t.object_transformations()[i]
            if hor:
                trans.set_horizontal_reflection()
            if ver:
                trans.set_vertical_reflection()

            to_remove = SlotTransformation("", "", "")
            for attr in trans.slot_transformations():
                if attr.name() == get_slot_name_angle():
                    to_remove = attr

            if to_remove.name() == get_slot_name_angle():
                trans.remove_slot_transformation(to_remove)

            to_remove_att = Slot("", "")
            for attr in trans.required():
                if attr.name() == get_slot_name_angle():
                    to_remove_att = attr

            if to_remove_att.name() == get_slot_name_angle():
                trans.remove_required_attribute(to_remove_att)

    transformations.append(t)
    return transformations


def remove_attribute(transformation, attribute):
    to_remove = SlotTransformation("", "", "")
    for attr in transformation.slot_transformations():
        if attr.name() == attribute:
            to_remove = attr
    if to_remove.name() == attribute:
        transformation.remove_slot_transformation(to_remove)
    to_remove_att = Slot("", "")
    for attr in transformation.required():
        if attr.name() == attribute:
            to_remove_att = attr
    if to_remove_att.name() == attribute:
        transformation.remove_required_attribute(to_remove_att)


def generate_reflection(transformation, a, b):
    t = copy.deepcopy(transformation)
    hor, ver = has_reflection(a, b)

    if hor:
        t.set_horizontal_reflection()
    if ver:
        t.set_vertical_reflection()

    return t


def generate_any_rotation(transformation):
    t = copy.deepcopy(transformation)
    t.set_any_rotation()
    return t


def generate_rotations(transformation, a, b, shape):
    result = []
    t1 = copy.deepcopy(transformation)
    t2 = copy.deepcopy(transformation)
    diff = calculate_angle_diff(a, b)

    if shape in ["octagon"]:
        diff %= 45

    # Rotate both ways
    d1 = diff % 360
    t1.set_rotation(d1)
    result.append(t1)

    if shape not in ["triangle", "Pac-Man", "half-arrow", "arrow"]:
        d2 = (-diff) % 360
        t2.set_rotation(d2)
        result.append(t2)

    return result


def add_object(obj):
    attr = []
    for a in obj.slots():
        a_t = SlotTransformation(a.name(), "", a.filler())
        attr.append(a_t)
    return ObjectTransformation(obj.name(), False, True, [], attr, [], [], "", "")


def delete_object(obj):
    requirements = get_required_slots(obj)
    shape = obj.get_slot_filler(get_slot_name_shape())
    size = obj.get_slot_filler(get_slot_name_size())
    return ObjectTransformation(obj.name(), True, False, [], [], [], requirements, shape, size)


def get_required_slots(obj):
    requirements = []
    for attribute_a in obj.slots():
        if attribute_a.name() != get_slot_name_shape():
            requirements.append(attribute_a)
    return requirements


def has_multi_valued_fill_slots(object_a, object_b):
    filler_a = object_a.get_slot_filler(get_slot_name_fill())
    filler_b = object_b.get_slot_filler(get_slot_name_fill())

    if len(filler_a) > 0 and len(filler_b) > 0 and filler_a != filler_b:
        values = ["yes", "no"]
        if (filler_a not in values) or (filler_b not in values):
            return True

    return False


def calculate_fill_add_and_rotate(fill_a, fill_b):
    a = sorted(fill_a.split(","), key=str.lower)
    b = sorted(fill_b.split(","), key=str.lower)
    add = 0
    rotate = 0
    if a == ["top-left"]:
        if b == ["top-left", "top-right"]:
            add = 1
        elif b == ["bottom-left", "top-left"]:
            add = -1
        elif b == ["top-right"]:
            rotate = 1
        elif b == ["bottom-left"]:
            rotate = -1
    elif a == ["top-right"]:
        if b == ["bottom-right", "top-right"]:
            add = 1
        elif b == ["top-left", "top-right"]:
            add = -1
        elif b == ["bottom-right"]:
            rotate = 1
        elif b == ["top-left"]:
            rotate = -1
    elif a == ["bottom-right"]:
        if b == ["bottom-left", "bottom-right"]:
            add = 1
        elif b == ["bottom-right", "top-right"]:
            add = -1
        elif b == ["bottom-left"]:
            rotate = 1
        elif b == ["top-right"]:
            rotate = -1
    elif a == ["bottom-left"]:
        if b == ["bottom-left", "top-left"]:
            add = 1
        elif b == ["bottom-left", "bottom-right"]:
            add = -1
        elif b == ["top-left"]:
            rotate = 1
        elif b == ["bottom-right"]:
            rotate = -1
    elif a == ["top-left", "top-right"]:
        if b == ["bottom-right", "top-left", "top-right"]:
            add = 1
        elif b == ["bottom-left", "top-left", "top-right"]:
            add = -1
        elif b == ["bottom-right", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "top-left"]:
            rotate = -1
    elif a == ["bottom-right", "top-right"]:
        if b == ["bottom-left", "bottom-right", "top-right"]:
            add = 1
        elif b == ["bottom-right", "top-left", "top-right"]:
            add = -1
        elif b == ["top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right"]:
            rotate = -1
    elif a == ["bottom-left", "bottom-right"]:
        if b == ["bottom-left", "bottom-right", "top-left"]:
            add = 1
        elif b == ["bottom-left", "bottom-right", "top-right"]:
            add = -1
        elif b == ["bottom-left", "top-left"]:
            rotate = 1
        elif b == ["bottom-right", "top-right"]:
            rotate = -1
    elif a == ["bottom-left", "top-left"]:
        if b == ["bottom-left", "top-left", "top-right"]:
            add = 1
        elif b == ["bottom-left", "bottom-right", "top-left"]:
            add = -1
        elif b == ["top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right"]:
            rotate = -1

    elif a == ["bottom-right", "top-left", "top-right"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-left", "bottom-right", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "top-left", "top-right"]:
            rotate = -1
    elif a == ["bottom-left", "bottom-right", "top-right"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-left", "bottom-right", "top-left"]:
            rotate = 1
        elif b == ["bottom-right", "top-left", "top-right"]:
            rotate = -1

    elif a == ["bottom-left", "bottom-right", "top-left"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-left", "top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right", "top-right"]:
            rotate = -1

    elif a == ["bottom-left", "top-left", "top-right"]:
        if b == ["yes"]:
            add = 1
        elif b == ["yes"]:
            add = -1
        elif b == ["bottom-right", "top-left", "top-right"]:
            rotate = 1
        elif b == ["bottom-left", "bottom-right", "top-left"]:
            rotate = -1
    return add, rotate


def has_side_attributes(object_a, object_b):
    shape_a = object_a.get_slot_filler(get_slot_name_shape())
    shape_b = object_b.get_slot_filler(get_slot_name_shape())
    if "side-object" in shape_a and "side-object" in shape_b:
        return True
    else:
        return False


def transform_object(object_in_a, object_in_b):
    attribute_transformations = []

    shape = object_in_a.get_slot_filler(get_slot_name_shape())
    size = object_in_a.get_slot_filler(get_slot_name_size())

    requirements = get_required_slots(object_in_a)

    for attribute_a in object_in_a.slots():
        for attribute_b in object_in_b.slots():
            if attribute_a.name() == attribute_b.name():
                if attribute_a.filler() != attribute_b.filler():
                    if attribute_a.filler() == "no" and "," in attribute_b.filler():
                        at = SlotTransformation(attribute_a.name(),
                                                "",
                                                attribute_b.filler())
                        at.set_append_value()
                    else:
                        at = SlotTransformation(attribute_a.name(),
                                                attribute_a.filler(),
                                                attribute_b.filler())
                    attribute_transformations.append(at)
    attributes_to_add = []
    for attribute_b in object_in_b.slots():
        found = False
        for attribute_a in object_in_a.slots():
            if attribute_a.name() == attribute_b.name():
                found = True

        if not found:
            at = SlotTransformation(attribute_b.name(),
                                    "",
                                    attribute_b.filler())
            attributes_to_add.append(at)
    attributes_to_remove = []
    for attribute_a in object_in_a.slots():
        found = False
        for attribute_b in object_in_b.slots():
            if attribute_a.name() == attribute_b.name():
                found = True

        if not found:
            attributes_to_remove.append(attribute_a.name())

    o = ObjectTransformation(object_in_a.name(), False, False, attribute_transformations, attributes_to_add,
                             attributes_to_remove, requirements, shape, size)

    trans = []

    added = False
    if has_angle_attribute(object_in_a, object_in_b):
        remove_attribute(o, get_slot_name_angle())

        if find_shape(object_in_a) == get_slot_filler_circle() and find_shape(object_in_b) == get_slot_filler_circle():
            r = generate_any_rotation(o)
            trans.append(r)
            added = True
        else:
            hor, ver = has_reflection(object_in_a, object_in_b)

            if hor or ver:
                r = generate_reflection(o, object_in_a, object_in_b)
                trans.append(r)
                added = True

            ts = generate_rotations(o, object_in_a, object_in_b, shape)
            for t in ts:
                trans.append(t)
                added = True
    if has_multi_valued_fill_slots(object_in_a, object_in_b):
        fill_a = object_in_a.get_slot_filler(get_slot_name_fill())
        fill_b = object_in_b.get_slot_filler(get_slot_name_fill())
        add, rotate = calculate_fill_add_and_rotate(fill_a, fill_b)
        if add != 0 or rotate != 0:
            if add != 0:
                t = copy.deepcopy(o)
                remove_attribute(t, get_slot_name_fill())
                t.set_fill_add(add)
                trans.append(t)
            if rotate != 0:
                t = copy.deepcopy(o)
                remove_attribute(t, get_slot_name_fill())
                t.set_fill_rotate(rotate)
                trans.append(t)
        else:
            trans.append(o)
        added = True
    if has_side_attributes(object_in_a, object_in_b):
        t = copy.deepcopy(o)
        remove_attribute(t, get_slot_name_shape())

        shape_a = object_in_a.get_slot_filler(get_slot_name_shape())
        [_, sides_a] = shape_a.split(":")

        shape_b = object_in_b.get_slot_filler(get_slot_name_shape())
        [_, sides_b] = shape_b.split(":")

        i_sides_b = int(sides_b)
        i_sides_a = int(sides_a)

        if i_sides_a % i_sides_b == 0:
            divider = int(i_sides_a / i_sides_b)
            if divider > 1:
                t_div = copy.deepcopy(t)
                t_div.set_sides_transformation("div:{0}".format(divider))
                trans.append(t_div)

        if i_sides_b % i_sides_a == 0:
            multiplier = int(i_sides_b / i_sides_a)
            if multiplier > 1:
                t_div = copy.deepcopy(t)
                t_div.set_sides_transformation("mult:{0}".format(multiplier))
                trans.append(t_div)

        diff = i_sides_b - i_sides_a
        t.set_sides_transformation("add:{0}".format(diff))
        trans.append(t)
        added = True

    if not added:
        trans.append(o)
    return trans