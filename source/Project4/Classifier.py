import copy
import cv2
import math
import numpy as np
import sys
from Common import get_slot_filler_circle, get_slot_filler_square, get_slot_name_angle, get_slot_name_shape, get_slot_name_size, \
    get_slot_name_fill


def visual_to_textual(img_file):
    fig = {}
    obj_count = 0
    img_gray = cv2.imread(img_file, cv2.CV_LOAD_IMAGE_GRAYSCALE)
    height, width = img_gray.shape
    # inverted threshold - OpenCV expects blackness to mean "nothing"
    ret, img_bw = cv2.threshold(img_gray, thresh=200, maxval=255, type=cv2.THRESH_BINARY_INV)

    contours, hierarchy = cv2.findContours(img_bw, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_SIMPLE)

    skip = []
    names = {}
    not_filled_objects = []
    for i in range(len(contours)):
        if i in skip:
            continue
        contour = contours[i]
        obj = {}
        circumference = cv2.arcLength(contour, True)
        approx = cv2.approxPolyDP(contour, epsilon=0.01 * circumference, closed=True)

        area = cv2.contourArea(contour)

        # identify SHAPE
        num_sides = len(approx)
        shape = find_shape(contour)
        obj["shape"] = shape
        obj["size_relative_to_total"] = get_size(contour, width, height)

        moment = cv2.moments(contour)
        cx = int(moment['m10'] / moment['m00'])
        cy = int(moment['m01'] / moment['m00'])
        rect_bounding = cv2.boundingRect(contour)

        # identify FILLED
        if i in not_filled_objects:
            obj["fill"] = 'no'
        else:
            if hierarchy[0][i][2] != -1:
                # we have a child - determine if it's the same shape
                child_contour = contours[hierarchy[0][i][2]]

                child_moment = cv2.moments(child_contour)
                child_cx = int(child_moment['m10'] / child_moment['m00'])
                child_cy = int(child_moment['m01'] / child_moment['m00'])

                child_area = cv2.contourArea(child_contour)
                child_shape = find_shape(child_contour)

                if is_same_object(area, cx, cy, child_area, child_cx, child_cy, shape, child_shape):
                    skip += [hierarchy[0][i][2]]
                    obj["fill"] = "no"
                else:
                    # shape filled other object
                    not_filled_objects += [hierarchy[0][i][2]]
                    obj["fill"] = "yes"
            else:
                # object filled
                obj["fill"] = "yes"

        # INSIDE
        if hierarchy[0][i][3] != -1:
            # a parent exists
            parent = hierarchy[0][i][3]
            while not (parent in names):
                parent = hierarchy[0][parent][3]
                if parent == -1:
                    break
            if parent != -1 and parent in names:
                obj['inside'] = names[parent]

        if obj["shape"] == "triangle":
            shape, angle = get_triangle_info(approx)
            obj["angle"] = str(angle)
            obj["shape"] = shape
        elif obj["shape"] == "plus":
            angle = get_plus_angle(contour)
            obj["angle"] = str(angle)
        elif obj["shape"] == "rectangle":
            angle = get_rectangle_angle(contour)
            obj["angle"] = str(angle)
        elif obj["shape"] == "Pac-Man":
            angle = get_pac_man_angle(approx, cx, cy)
            obj["angle"] = str(angle)
        else:
            # ANGLE
            min_rect = cv2.minAreaRect(contour)
            angle = min_rect[2]
            if abs(angle) > 1.0:
                ang = int(angle) % 360
                obj["angle"] = str(ang)

        if obj["shape"] == "octagon":
            if "angle" in obj:
                obj["angle"] = correct_octagon_angle(obj["angle"])

        name = chr(ord('Z') - obj_count)  # generate a name, starting at Z and working backwards
        names[i] = name
        obj_count += 1
        obj["area"] = area
        obj["cx"] = cx
        obj["cy"] = cy
        obj["num_edges"] = num_sides

        obj["center"] = calculate_center(contour, obj["shape"], rect_bounding)

        fig[name] = obj

    # identify LEFT-OF
    if len(fig) >= 2:
        for object_to_update in fig:
            x1, _y1 = fig[object_to_update]["center"]
            for object_to_test in fig:
                x2, _y2 = fig[object_to_test]["center"]
                if x1 < x2:
                    dist = x2 - x1
                    dist2 = float(dist) / float(width)
                    if dist2 >= 0.02:
                        if "left-of" in fig[object_to_update]:
                            fig[object_to_update]["left-of"] += "," + object_to_test
                        else:
                            fig[object_to_update]["left-of"] = object_to_test

    # identify ABOVE
    if len(fig) >= 2:
        for object_to_update in fig:
            _x1, y1 = fig[object_to_update]["center"]
            for object_to_test in fig:
                _x2, y2 = fig[object_to_test]["center"]
                if y1 < y2:
                    dist = y2 - y1
                    dist2 = float(dist) / float(height)
                    if dist2 >= 0.02:
                        if "above" in fig[object_to_update]:
                            fig[object_to_update]["above"] += "," + object_to_test
                        else:
                            fig[object_to_update]["above"] = object_to_test

    # Build hierarchy for INSIDE all the way up the chain
    for k in fig:
        f = fig[k]
        if "inside" in f:
            f["inside"] = get_all_inside(k, fig)

    for k in fig:
        f = fig[k]
        if f["shape"] == get_slot_filler_circle():
            f["angle"] = "0"

    for k in fig:
        f = fig[k]
        if get_slot_name_angle() in f:
            if f[get_slot_name_shape()] == get_slot_filler_square():
                angle = int(float(f[get_slot_name_angle()]))
                angle %= 90
                f[get_slot_name_angle()] = str(angle)
        else:
            f[get_slot_name_angle()] = "0"
    return fig


def get_all_inside(object_name, objects):
    me = objects[object_name]
    inside = []

    if "inside" in me:
        inside_of = me["inside"].split(",")
        for i in inside_of:
            inside.append(i)
            new_inside = get_all_inside(i, objects). split(",")
            if len(new_inside) > 0:
                for n in new_inside:
                    if len(n) > 0:
                        inside.append(n)

    if len(inside) > 0:
        distinct = list(set(inside))
        sorted_list = sorted(distinct, key=str.lower)
        string = ""
        for s in sorted_list:
            if len(string) > 0:
                string += ","
            string += s
        return string
    else:
        return ""


def calculate_center_old(rect):
    x, y, w, h = rect
    x2 = x + int(w / 2)
    y2 = y + int(h / 2)
    return x2, y2


def calculate_center(contour, shape, rect):
    if shape in ["triangle"]:
        x, y, w, h = rect
        x2 = x + int(w / 2)
        y2 = y + int(h / 2)
        return x2, y2
    else:
        (x, y), _radius = cv2.minEnclosingCircle(contour)
        return x, y


def get_size(contour, width, height):
    total_area = width * height
    (_center, (w, h), _th) = cv2.minAreaRect(contour)
    bounding_area = w * h
    diff = bounding_area / total_area
    return diff


def find_shape(contour):
    approx = cv2.approxPolyDP(contour, 0.01 * cv2.arcLength(contour, True), True)
    # if len(approx) == 5 and not cv2.isContourConvex(contour):
    #     return "half-arrow"
    # elif len(approx) == 7:
    #     return "arrow"
    if len(approx <= 5) and is_triangle(contour):
        return "triangle"
    elif len(approx) == 3:
        return "triangle"
    elif is_rectangle(approx, contour):
        if is_square(contour):
            return "square"
        else:
            return "rectangle"
    elif len(approx) == 5:
        return "pentagon"
    elif len(approx) == 6:
        return "hexagon"
    elif len(approx) == 8:
        return "octagon"
    elif is_plus(contour):
        return "plus"
    elif is_pac_man(approx, contour):
        return "Pac-Man"
    elif is_circle(contour):
        return "circle"

    else:
        return "Unknown"


def is_rectangle(approx, contour):
    if len(approx) >= 4:
        area_contour = cv2.contourArea(contour)
        (_center, (w, h), _theta) = cv2.minAreaRect(contour)
        area_rectangle = float(w) * float(h)

        diff = abs(1.0 - (float(area_contour) / float(area_rectangle)))
        if diff <= 0.1:
            return True
        else:
            return False


def is_square(contour):
    (_center, (w, h), _theta) = cv2.minAreaRect(contour)
    diff = abs(1.0 - (float(w) / float(h)))
    if diff <= 0.1:
        return True
    else:
        return False


def is_pac_man(approx, cnt):
    length = len(approx)
    convex = cv2.isContourConvex(cnt)
    circle = is_circle(cnt)

    _, radius = cv2.minEnclosingCircle(cnt)
    area = cv2.contourArea(cnt)
    circle_area = (np.pi * radius ** 2)
    diff = abs(1 - (area / circle_area))

    if length in [11, 12, 13] and not convex and not circle and diff > 0.25:
        return True
    else:
        return False


def is_plus(contour):
    convex = cv2.isContourConvex(contour)
    circle = is_circle(contour)
    hull = cv2.convexHull(contour, returnPoints=False)
    defects = cv2.convexityDefects(contour, hull)

    if defects is None:
        return False

    if not convex and not circle and len(defects) in [4, 5, 6]:
        return True
    else:
        return False


def is_triangle(contour):
    area = cv2.contourArea(contour)
    (_center, (w, h), _th) = cv2.minAreaRect(contour)
    half_rect = w * h / 2.0

    if half_rect == 0.0:
        return False

    diff = abs(1 - (area / half_rect))
    if diff <= 0.14:
        return True
    else:
        return False


def is_circle(cnt):
    _, radius = cv2.minEnclosingCircle(cnt)
    area = cv2.contourArea(cnt)
    circle_area = np.pi * (radius ** 2.0)
    if circle_area != 0.0:
        diff = abs(1 - (area / circle_area))
        return diff <= 0.113
    else:
        return False


def remove_too_many_point_for_triangle(approx):
    a = approx.tolist()
    while len(a) > 3:
        to_delete = None
        min_distance = None
        for p1 in a:
            for p2 in a:
                point1 = p1[0]
                point2 = p2[0]
                if point1 != point2:
                    x1 = float(point1[0])
                    x2 = float(point2[0])
                    y1 = float(point1[1])
                    y2 = float(point2[1])
                    dist = math.sqrt(((x1 - x2) ** 2) + ((y1 - y2) ** 2))
                    if min_distance is not None:
                        if dist < min_distance:
                            to_delete = p2
                            min_distance = dist
                    else:
                        to_delete = p2
                        min_distance = dist

        if to_delete is not None:
            a.remove(to_delete)

    return np.array(a)


def get_triangle_info(approx_input):
    approx = copy.copy(approx_input)
    approx = remove_too_many_point_for_triangle(approx)
    length = len(approx)
    is_right_triangle = False
    vertex = -1
    for i in range(0, length):
        ax = approx[i][0][0]
        ay = approx[i][0][1]
        bx = approx[(i + 1) % 3][0][0]
        by = approx[(i + 1) % 3][0][1]
        cx = approx[(i + 2) % 3][0][0]
        cy = approx[(i + 2) % 3][0][1]
        v0x = ax - bx
        v0y = ay - by
        v1x = cx - bx
        v1y = cy - by
        angle1 = cv2.fastAtan2(v0y, v0x)
        angle2 = cv2.fastAtan2(v1y, v1x)
        if abs(abs(angle1 - angle2) - 90) < 10 or abs(abs(angle1 - angle2) - 270) < 10:
            is_right_triangle = True
            vertex = (i + 1) % 3
            break

    if is_right_triangle:
        mid_point_x = (approx[(vertex + 1) % 3][0][0] + approx[(vertex + 2) % 3][0][0]) / 2
        mid_point_y = (approx[(vertex + 1) % 3][0][1] + approx[(vertex + 2) % 3][0][1]) / 2
        v1x = approx[vertex][0][0] - mid_point_x
        v1y = approx[vertex][0][1] - mid_point_y
        angle = cv2.fastAtan2(v1y, v1x)
        angle += 225
        if angle > 360:
            angle -= 360
        if abs(angle) < 10 or abs(angle - 360) < 10:
            angle = 0
        elif abs(angle - 45) < 10:
            angle = 45
        elif abs(angle - 90) < 10:
            angle = 90
        elif abs(angle - 135) < 10:
            angle = 135
        elif abs(angle - 180) < 10:
            angle = 180
        elif abs(angle - 225) < 10:
            angle = 225
        elif abs(angle - 270) < 10:
            angle = 270
        elif abs(angle - 315) < 10:
            angle = 315

        return "right-triangle", int(angle)

    else:
        vertex = -1
        angle = 0
        for i in range(0, length):
            p0x = approx[i][0][0]
            p0y = approx[i][0][1]
            p1x = approx[(i + 1) % 3][0][0]
            p1y = approx[(i + 1) % 3][0][1]
            if abs(p0x - p1x) < 10 or abs(p0y - p1y) < 10:
                vertex = (i + 2) % 3
                break

        if vertex != -1:
            mid_point_opp_x = (approx[(vertex + 1) % 3][0][0] + approx[(vertex + 2) % 3][0][0]) / 2
            mid_point_opp_y = (approx[(vertex + 1) % 3][0][1] + approx[(vertex + 2) % 3][0][1]) / 2
            vX = approx[vertex][0][0] - mid_point_opp_x
            vY = approx[vertex][0][1] - mid_point_opp_y
            angle = cv2.fastAtan2(vY, vX)
            angle += 90
            if angle > 360:
                angle -= 360
            if abs(angle) < 10 or abs(angle - 360) < 10:
                angle = 0
            elif abs(angle - 45) < 10:
                angle = 45
            elif abs(angle - 90) < 10:
                angle = 90
            elif abs(angle - 135) < 10:
                angle = 135
            elif abs(angle - 180) < 10:
                angle = 180
            elif abs(angle - 225) < 10:
                angle = 225
            elif abs(angle - 270) < 10:
                angle = 270
            elif abs(angle - 315) < 10:
                angle = 315
        return "triangle", int(angle)


def get_rectangle_angle(contour):
    (_center, (w, h), angle_float) = cv2.minAreaRect(contour)
    if h > w:
        angle_float += 90.0
    angle = int(angle_float) % 360

    if abs(angle) < 10 or abs(angle - 360) < 10:
        angle = 0
    elif abs(angle - 45) < 10:
        angle = 45
    elif abs(angle - 90) < 10:
        angle = 90
    elif abs(angle - 135) < 10:
        angle = 135
    elif abs(angle - 180) < 10:
        angle = 180
    elif abs(angle - 225) < 10:
        angle = 225
    elif abs(angle - 270) < 10:
        angle = 270
    elif abs(angle - 315) < 10:
        angle = 315

    return int(angle)


def get_plus_angle(contour):
    min_rect = cv2.minAreaRect(contour)
    angle_float = min_rect[2]
    angle_float += 45.0
    angle = int(angle_float) % 90

    if abs(angle) < 10 or abs(angle - 90) < 10:
        angle = 0
    elif abs(angle - 45) < 10:
        angle = 45

    return int(angle)


def get_pac_man_angle(approx, cx, cy):
    closest_to_center = -1
    min_distance = sys.float_info.max
    for i in range(0, len(approx)):
        current_distance = math.sqrt(math.pow(approx[i][0][0] - cx, 2) + math.pow(approx[i][0][1] - cy, 2))
        if current_distance < min_distance:
            closest_to_center = i
            min_distance = current_distance
    next_point = (closest_to_center + 1) % len(approx)
    prev = (closest_to_center - 1) % len(approx)
    p0x = approx[closest_to_center][0][0]
    p0y = approx[closest_to_center][0][1]

    p1x = float(approx[next_point][0][0] + approx[prev][0][0]) / 2
    p1y = float(approx[next_point][0][1] + approx[prev][0][1]) / 2

    angle = cv2.fastAtan2(p1y - p0y, p1x - p0x)
    if abs(angle) < 10 or abs(angle - 360) < 10:
        angle = 0
    elif abs(angle - 45) < 10:
        angle = 45
    elif abs(angle - 90) < 10:
        angle = 90
    elif abs(angle - 135) < 10:
        angle = 135
    elif abs(angle - 180) < 10:
        angle = 180
    elif abs(angle - 225) < 10:
        angle = 225
    elif abs(angle - 270) < 10:
        angle = 270
    elif abs(angle - 315) < 10:
        angle = 315
    return int(angle)


def correct_octagon_angle(angle):
    a = int(angle)
    temp = a % 45
    return str(temp)


def is_same_object(o1_area, o1_cx, o1_cy, o2_area, o2_cx, o2_cy, shape, child_shape):
    if shape != child_shape:
        return False

    area_diff = abs(1 - o1_area/o2_area)
    if abs(o1_cx - o2_cx) < 5 and abs(o1_cy - o2_cy) < 5 and area_diff < 1.0:
        return True

    else:
        return False


def inside_object_with_fill(obj, objects):
    inside = obj.get_slot_filler("inside")
    if len(inside) == 0:
        return False

    outside = get_object(objects, inside)

    if outside is None:
        return False

    if outside.get_slot_filler(get_slot_name_fill()) == "yes":
        return True
    else:
        return False


def get_object(objects, name):
    for obj in objects:
        if obj.name() == name:
            return obj
    return None


def calculate_size_levels(figures):
    diffs = []
    for figure_name in figures:
        figure = figures[figure_name]
        for obj in figure.objects():
            diff = obj.get_slot_filler("size_relative_to_total")
            diffs.append(diff)

    for figure_name in figures:
        figure = figures[figure_name]
        for obj in figure.objects():
            diff = obj.get_slot_filler("size_relative_to_total")
            shape = obj.get_slot_filler(get_slot_name_shape())
            factor = 1.0

            if inside_object_with_fill(obj, figure.objects()):
                factor *= 1.05

            if shape in ["triangle"]:
                factor *= 0.9
            # elif shape in ["circle"]:
            #     factor *= 0.85

            weighted_diff = diff * factor

            if weighted_diff > 0.55:
                category = "very-large"
            elif weighted_diff > 0.45:
                category = "large"
            elif weighted_diff > 0.29:
                category = "medium"
            elif weighted_diff > 0.15:
                category = "small"
            elif weighted_diff > 0.08:
                category = "very-small"
            else:
                category = "tiny"

            obj.set_slot_filler(get_slot_name_size(), category)


def remove_extra_info(figures):
    # Remove additional attributes
    for figure_name in figures:
        fig = figures[figure_name]
        for o in fig.objects():
            o.remove_slot("area")
            o.remove_slot("cx")
            o.remove_slot("cy")
            o.remove_slot("num_edges")
            o.remove_slot("center")
            o.remove_slot("size_relative_to_total")