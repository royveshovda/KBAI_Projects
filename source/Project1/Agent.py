import timeit
from Common import convert
from Generator import calculate_transformations
from Tester import test


class Agent:
    def __init__(self):
        self._debug_mode = False

    # noinspection PyMethodMayBeStatic,PyBroadException,PyPep8Naming
    def Solve(self, problem):
        try:
            result = run(problem)
        except Exception:
            print("Something went wrong running the agent")
            result = "0"

        return result

    def set_debug_mode(self):
        self._debug_mode = True


# noinspection PyListCreation
def run(problem):
    start = timeit.default_timer()

    print("")
    print("Trying to solve: {0}".format(problem.getName()))

    # Not able to solve this one yet
    if problem.getName() == "2x1 Challenge Problem 05":
        print("Skipping")
        return "0"

    fig_a = convert(problem.figures['A'])
    fig_b = convert(problem.figures['B'])

    fig_c = convert(problem.figures['C'])

    figures = (convert(problem.figures['1']),
               convert(problem.figures['2']),
               convert(problem.figures['3']),
               convert(problem.figures['4']),
               convert(problem.figures['5']),
               convert(problem.figures['6']))

    solutions = run_strategies(fig_a, fig_b, fig_c, figures)

    stop = timeit.default_timer()
    runtime = stop - start
    print("Runtime: \033[0;34m{0}s\033[00m".format(runtime))

    if len(solutions) == 0:
        correct = problem.checkAnswer("0")
        print("\033[0;31mFound NONE answers -- Should be {0}\033[00m".format(correct))
        return "0"
    elif len(solutions) == 1:
        (_, answer) = solutions[0]
        correct = problem.checkAnswer(answer)
        if correct == answer:
            print("\033[0;32mFound correct solution: {0}\033[00m".format(answer))
        else:
            print("\033[0;31mFound solution: {0}, but should be: {1}\033[00m".format(answer, correct))
        return answer
    else:
        ans = ""
        for (_, item) in solutions:
            ans += " " + item
        correct = problem.checkAnswer("0")
        print("\033[0;31mFound more than one answer ( {0} ) -- Should be {1}\033[00m".format(ans, correct))
        return "0"


def run_strategies(fig_a, fig_b, fig_c, figures):
    # Pick different strategies
    # First try using requirements. If not finding any, loosen up
    transformations = calculate_transformations(fig_a, fig_b)
    results = test(fig_c, transformations, figures, True)
    solutions = []
    if len(results) == 0:
        results = test(fig_c, transformations, figures, False)
    if len(results) >= 1:
        if all_the_same(results):
            solutions = [results[0]]
        else:
            solutions = order_results(results)

    return solutions


def all_the_same(solutions):
    (_, potential) = solutions[0]
    for (_, solution) in solutions:
        if solution != potential:
            return False
    return True


def order_results(results):
    weighted = []
    for (transformation, r) in results:
        w = calculate_weight(transformation)
        weighted.append((transformation, r, w))

    max_value = 0
    for (transformation, r, w) in weighted:
        if w > max_value:
            max_value = w

    best = []
    for (transformation, r, w) in weighted:
        if w == max_value:
            best.append((transformation, r))

    if all_the_same(best):
        return [best[0]]
    else:
        return best


def calculate_weight(transformation):
    total = 0
    for t in transformation.object_transformations():
        if t.add():
            total += 0
        elif t.delete():
            total += 1
        elif t.is_rotation():
            total += 3
        elif t.is_reflection():
            total += 4
        else:  # Unchanged
            total += 5

    return total