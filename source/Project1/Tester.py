import copy
from Common import generate_permutations, get_objects_in_order, rename_object_reference_in_object_list, Object, \
    Slot, Figure, generate_combinations, convert_attributes_to_dictionary, find_shape, find_size, find_fill


def test(figure_c, transformations, figures, use_requirements):
    combinations_to_solve_the_problem = []
    for idx, transformation in enumerate(transformations):
        solutions = transform(figure_c, transformation, use_requirements)
        for solution in solutions:
            if compare_figures(solution, figures[0], transformation.is_general()):
                combinations_to_solve_the_problem.append((transformation, "1"))

            if compare_figures(solution, figures[1], transformation.is_general()):
                combinations_to_solve_the_problem.append((transformation, "2"))

            if compare_figures(solution, figures[2], transformation.is_general()):
                combinations_to_solve_the_problem.append((transformation, "3"))

            if compare_figures(solution, figures[3], transformation.is_general()):
                combinations_to_solve_the_problem.append((transformation, "4"))

            if compare_figures(solution, figures[4], transformation.is_general()):
                combinations_to_solve_the_problem.append((transformation, "5"))

            if compare_figures(solution, figures[5], transformation.is_general()):
                combinations_to_solve_the_problem.append((transformation, "6"))
    return combinations_to_solve_the_problem


def transform(figure_c, figure_transformation, use_requirements):
    results = []

    if figure_transformation.is_general():
        c_s = figure_c.objects()
        transformation = []
        for i in range(len(c_s)):
            transformation.append(figure_transformation.object_transformations()[0])

        try:
            res = transform_ordered(c_s, transformation, use_requirements)
            results.append(res)
        except TypeError:
            pass
    else:
        len_c = len(figure_c.objects())
        combinations = generate_permutations(len_c)
        for order in combinations:
            c_s = get_objects_in_order(figure_c.objects(), order)
            try:
                res = transform_ordered(c_s, figure_transformation.object_transformations(), use_requirements)
                results.append(res)
            except TypeError:
                pass

    return results


def transform_ordered(objects, transformation, use_requirements):
    local_objects = copy.deepcopy(objects)

    length_o = len(local_objects)
    length_t = len(transformation)
    length = max(length_o, length_t)

    # rename objects
    object_index = 0
    for i in range(length):
        if i >= len(transformation):
            raise TypeError("Not enough transformations to fit figure")
        t = transformation[i]
        if not t.add():
            o = local_objects[object_index]
            object_index += 1
            rename_object_reference_in_object_list(local_objects, o.name(), t.name())

    positions_to_delete = []
    to_add = []

    object_index = 0
    for i in range(length):

        if i >= len(transformation):
            raise TypeError("Not enough transformations to fit figure")

        t = transformation[i]
        if t.add():
            r = Object(t.name(), [])
            for a in t.to_add():
                r.slots().append(Slot(a.name(), a.filler_to()))
                to_add.append(r)
        else:
            o = local_objects[object_index]
            object_index += 1
            if t.delete():
                positions_to_delete.append(i)
            else:
                if use_requirements:
                    if not verify_object(o, t.required()):
                        raise TypeError("Not correct object to transform")

                for trans in t.slot_transformations():
                    found_attribute_to_transform = False
                    for attribute in o.slots():
                        if attribute.name() == trans.name():
                            if trans.is_append_values():
                                found_attribute_to_transform = True
                                attribute.append_values(trans.filler_to())
                            else:
                                if attribute.filler() == trans.filler_from():
                                    found_attribute_to_transform = True
                                    attribute.new_filler(trans.filler_to())
                    if found_attribute_to_transform is False:
                        raise TypeError("Not able find attribute to transform: {0}".format(trans.name()))
                for add in t.to_add():
                    o.slots().append(Slot(add.name(), add.filler_to()))
                for remove in t.to_delete():
                    found = False
                    to_remove = Slot("", "")
                    for a in o.slots():
                        if a.name() == remove:
                            to_remove = a
                            found = True

                    if found is True:
                        o.slots().remove(to_remove)
                    else:
                        raise TypeError("Not able to remove attribute: {0}".format(remove))
                add_reflection(t, o)
                add_rotation(t, o)

    positions_to_delete.sort(reverse=True)
    for p in positions_to_delete:
        obj = local_objects[p]
        local_objects.remove(obj)

    for o in to_add:
        local_objects.append(o)

    f = Figure("X", local_objects)
    return f


def add_reflection(transformation, obj):
    if transformation.is_vertical_reflection() or transformation.is_horizontal_reflection():
        shape = ""
        for a in obj.slots():
            if a.name() == "shape":
                shape = a.filler()

        for a in obj.slots():
            if a.name() == "angle":
                if transformation.is_horizontal_reflection():
                    apply_horizontal_reflection(shape, a)
                elif transformation.is_vertical_reflection():
                    apply_vertical_reflection(shape, a)

        if transformation.is_vertical_reflection():
            for a in obj.slots():
                if a.name() == "vertical-flip":
                    if a.filler() == "yes":
                        a.new_filler("no")
                    else:
                        a.new_filler("yes")

        if transformation.is_horizontal_reflection():
            for a in obj.slots():
                if a.name() == "horizontal-flip":
                    if a.filler() == "yes":
                        a.new_filler("no")
                    else:
                        a.new_filler("yes")


def add_rotation(transformation, obj):
    if transformation.is_rotation():
        for a in obj.slots():
            if a.name() == "angle":
                v = int(a.filler())
                new_v = (v + transformation.rotation()) % 360
                a.new_filler(str(new_v))


def apply_horizontal_reflection(shape, attribute):
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if attribute.filler() == "0":
            attribute.new_filler("0")
        elif attribute.filler() == "90":
            attribute.new_filler("270")
        elif attribute.filler() == "180":
            attribute.new_filler("180")
        elif attribute.filler() == "270":
            attribute.new_filler("90")
    else:
        if attribute.filler() == "0":
            attribute.new_filler("180")
        elif attribute.filler() == "90":
            attribute.new_filler("90")
        elif attribute.filler() == "180":
            attribute.new_filler("0")
        elif attribute.filler() == "270":
            attribute.new_filler("270")


def apply_vertical_reflection(shape, attribute):
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if attribute.filler() == "0":
            attribute.new_filler("180")
        elif attribute.filler() == "90":
            attribute.new_filler("90")
        elif attribute.filler() == "180":
            attribute.new_filler("0")
        elif attribute.filler() == "270":
            attribute.new_filler("270")
    else:
        if attribute.filler() == "0":
            attribute.new_filler("0")
        elif attribute.filler() == "90":
            attribute.new_filler("270")
        elif attribute.filler() == "180":
            attribute.new_filler("180")
        elif attribute.filler() == "270":
            attribute.new_filler("90")


def verify_object(obj, attributes):
    for a1 in attributes:
        found = False
        for a2 in obj.slots():
            if a1.name() == a2.name() and a1.filler() == a2.filler():
                found = True
        if not found:
            return False

    return True


def compare_figures(figure_a, figure_b, is_general_transformation):
    objects_in_a = figure_a.objects()
    objects_in_b = figure_b.objects()

    if not quick_check(objects_in_a, objects_in_b):
        return False

    length = len(objects_in_a)
    if not is_general_transformation:
        combinations = generate_combinations(length, length)
    else:
        combinations = [(list(range(length)), list(range(length)))]

    # Candidate for improvements
    # Maybe hints on where to start?

    for (order_a, order_b) in combinations:
        a_s = get_objects_in_order(figure_a.objects(), order_a)
        b_s = copy.deepcopy(get_objects_in_order(figure_b.objects(), order_b))

        for i in range(length):
            a = a_s[i]
            b = b_s[i]
            rename_object_reference_in_object_list(b_s, b.name(), a.name())

        same = True
        for i in range(length):
            res = compare_objects(a_s[i], b_s[i])
            if not res:
                same = False
        if same is True:
            return True

    return False


def quick_check(objects_in_a, objects_in_b):
    if len(objects_in_a) != len(objects_in_b):
        return False

    equal = True
    for a in objects_in_a:
        a_shape = find_shape(a)
        a_size = find_size(a)
        a_fill = find_fill(a)
        found = False
        for b in objects_in_b:
            b_shape = find_shape(b)
            b_size = find_size(b)
            b_fill = find_fill(b)
            if a_shape == b_shape and a_size == b_size:
                if "," in a_fill or "," in b_fill:
                    if compare_as_lists(a_fill, b_fill):
                        found = True
                else:
                    if a_fill == b_fill:
                        found = True
        if not found:
            equal = False
    return equal


def compare_objects(object_a, object_b):
    attributes_in_a = convert_attributes_to_dictionary(object_a.slots())
    attributes_in_b = convert_attributes_to_dictionary(object_b.slots())

    attributes_to_ignore = []

    if attributes_in_a["shape"] == "circle":
        attributes_to_ignore.append("angle")

    if len(attributes_in_a) != len(attributes_in_b):
        return False

    keys = list(attributes_in_a.keys())

    for key in keys:
        if key not in attributes_to_ignore:
            if key not in attributes_in_b:
                return False
            else:
                if "," in attributes_in_a[key] or "," in attributes_in_b[key]:
                    if not compare_as_lists(attributes_in_a[key], attributes_in_b[key]):
                        return False
                else:
                    if attributes_in_a[key] != attributes_in_b[key]:
                        return False

    return True


def compare_as_lists(value_a, value_b):
    a_s = sorted(value_a.split(","), key=str.lower)
    b_s = sorted(value_b.split(","), key=str.lower)

    if len(a_s) != len(b_s):
        return False

    length = len(a_s)

    for i in range(length):
        if a_s[i] != b_s[i]:
            return False

    return True