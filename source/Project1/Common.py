import itertools


def generate_combinations(a, b):
    a_s = list(range(a))
    b_s = list(range(b))

    while len(a_s) < len(b_s):
        a_s.append(False)

    while len(a_s) > len(b_s):
        b_s.append(False)

    b_lists = list(itertools.permutations(b_s))

    for y in b_lists:
        yield (a_s, list(y))


def generate_object_names(prefix=""):
    base = "OBJECT_"
    if len(prefix) > 0:
        base = prefix + "_"
    counter = 0
    while True:
        counter += 1
        yield base + str(counter)


def rename_object_in_figure(figure, from_name, to_name):
    rename_object_reference_in_object_list(figure.objects(), from_name, to_name)


def rename_object_reference_in_object_list(objects, from_name, to_name):
    for obj in objects:
        if obj is not False:
            if obj.name() == from_name:
                obj.new_name(to_name)
            for attribute in obj.slots():
                rename_object_reference_in_attribute(attribute, from_name, to_name)


def rename_object_reference_in_attribute(attribute, from_name, to_name):
    if from_name in attribute.filler():
        new_value = rename_reference_in_value(attribute.filler(), from_name, to_name)
        attribute.new_filler(new_value)


def rename_reference_in_value(value, from_name, to_name):
    if "," in value:
        new_value = []
        old_value = value.split(",")
        for v in old_value:
            if v == from_name:
                new_value.append(to_name)
            else:
                new_value.append(v)
        string = ""
        for s in new_value:
            if len(string) > 0:
                string += ","
            string += s
        value = string
    else:
        if value == from_name:
            value = to_name

    return value


def generate_permutations(c):
    c_s = list(range(c))
    c_lists = list(itertools.permutations(c_s))
    return [list(x) for x in c_lists]


def convert_attributes_to_dictionary(attributes):
    d = {}
    for attribute in attributes:
        d[attribute.name()] = attribute.filler()

    return d


def get_objects_in_order(objects, order):
    ordered_objects = []
    for o in order:
        if o is not False:
            ordered_objects.append(objects[o])
        else:
            ordered_objects.append(False)
    return ordered_objects


def convert(raven_figure):
    name = raven_figure.getName()
    objects = raven_figure.getObjects()
    converted_objects = []
    for obj in objects:
        o = convert_object(obj)
        converted_objects.append(o)

    return Figure(name, converted_objects)


def convert_object(raven_object):
    name = raven_object.getName()
    attributes = raven_object.getAttributes()
    converted_attributes = []
    for attr in attributes:
        a = convert_attribute(attr)
        converted_attributes.append(a)

    return Object(name, converted_attributes)


def convert_attribute(attribute):
    name = attribute.getName()
    value = attribute.getValue()
    return Attribute(name, value)


class Figure:
    def __init__(self, name, objects):
        self._name = name
        self._objects = objects

    def name(self):
        return self._name

    def objects(self):
        return self._objects


class Object:
    def __init__(self, name, attributes):
        self._name = name
        self._attributes = attributes

    def name(self):
        return self._name

    def attributes(self):
        return self._attributes

    def new_name(self, name):
        self._name = name


class Attribute:
    def __init__(self, name, value):
        self._name = name
        self._value = value

    def name(self):
        return self._name

    def value(self):
        return self._value

    def new_value(self, value):
        self._value = value

    def append_values(self, values):
        old_values = self._value.split(",")
        new_values = values.split(",")
        combined = old_values + new_values
        distinct = list(set(combined))
        sorted_list = sorted(distinct, key=str.lower)
        string = ""
        for s in sorted_list:
            if len(string) > 0:
                string += ","
            string += s
        self._value = string


class FigureTransformation:
    def __init__(self, object_transformations):
        self._object_transformations = object_transformations
        self._is_general = False

    def is_general(self):
        return self._is_general

    def object_transformations(self):
        return self._object_transformations

    def set_general(self):
        self._is_general = True
        if len(self.object_transformations()) > 0:
            while len(self.object_transformations()) > 1:
                del self.object_transformations()[-1]

    def check_general(self):
        verification = self.object_transformations()[0]

        for t in self.object_transformations():
            if t.delete() is not verification.delete():
                return False
            if t.add() is not verification.add():
                return False

            if len(t.slot_transformations()) != len(verification.slot_transformations()):
                return False

            for i in range(len(t.slot_transformations())):
                a = t.slot_transformations()[i]
                b = verification.slot_transformations()[i]
                if a.name() != b.name():
                    return False
                if a.filler_from() != b.filler_from():
                    return False
                if a.filler_to() != b.filler_to():
                    return False

            if len(t.to_add()) != len(verification.to_add()):
                return False

            for i in range(len(t.to_add())):
                a = t.to_add()[i]
                b = verification.to_add()[i]
                if a.name() != b.name():
                    return False
                if a.filler_from() != b.filler_from():
                    return False
                if a.filler_to() != b.filler_to():
                    return False

            if len(t.to_delete()) != len(verification.to_delete()):
                return False

            for i in range(len(t.to_delete())):
                a = t.to_delete()[i]
                b = verification.to_delete()[i]
                if a != b:
                    return False

        return True


class ObjectTransformation:
    def __init__(self, name, delete, add, attribute_transformations, to_add, to_delete, required):
        self._name = name
        self._delete = delete
        self._add = add
        self._attribute_transformations = attribute_transformations
        self._to_add = to_add
        self._to_delete = to_delete
        self._is_general = False
        self._required = required
        self._is_vertical_reflection = False
        self._is_horizontal_reflection = False
        self._rotation = 0
        self._is_rotation = False

    def name(self):
        return self._name

    def delete(self):
        return self._delete

    def add(self):
        return self._add

    def attribute_transformations(self):
        return self._attribute_transformations

    def to_add(self):
        return self._to_add

    def to_delete(self):
        return self._to_delete

    def is_general(self):
        return self._is_general

    def required(self):
        return self._required

    def is_reflection(self):
        return self._is_vertical_reflection or self._is_horizontal_reflection

    def is_vertical_reflection(self):
        return self._is_vertical_reflection

    def is_horizontal_reflection(self):
        return self._is_horizontal_reflection

    def set_vertical_reflection(self):
        self._is_vertical_reflection = True

    def set_horizontal_reflection(self):
        self._is_horizontal_reflection = True

    def remove_attribute_transformation(self, transformation):
        self._attribute_transformations.remove(transformation)

    def remove_required_attribute(self, attribute):
        self._required.remove(attribute)

    def is_rotation(self):
        return self._is_rotation

    def rotation(self):
        return self._rotation

    def set_rotation(self, value):
        self._is_rotation = True
        self._rotation = value


class AttributeTransformation:
    def __init__(self, name, from_value, to_value):
        self._name = name
        self._from_value = from_value
        self._to_value = to_value
        self._is_append_values = False

    def name(self):
        return self._name

    def from_value(self):
        return self._from_value

    def to_value(self):
        return self._to_value

    def is_append_values(self):
        return self._is_append_values

    def set_append_value(self):
        self._is_append_values = True