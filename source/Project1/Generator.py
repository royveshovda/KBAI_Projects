import copy
from Common import generate_combinations, get_objects_in_order, FigureTransformation, generate_object_names, \
    rename_object_reference_in_object_list, convert_attributes_to_dictionary, SlotTransformation, Slot, \
    ObjectTransformation


def calculate_transformations(figure_a, figure_b):
    len_a = len(figure_a.objects())
    len_b = len(figure_b.objects())
    combinations = generate_combinations(len_a, len_b)

    transformations = []

    for (order_a, order_b) in combinations:
        a_s = copy.deepcopy(get_objects_in_order(figure_a.objects(), order_a))
        b_s = copy.deepcopy(get_objects_in_order(figure_b.objects(), order_b))
        trans = calculate_transformation(a_s, b_s)

        for t in trans:
            transformation = FigureTransformation(t)
            if transformation.check_general():
                transformation.set_general()
            transformations.append(transformation)
    # TODO: Consider to order transformations before returning
    # Group transformations based on weight-values. The tester then can test one group at the time,
    # starting with the highest valued group

    # weighted = []
    # for transformation in transformations:
    #     w = calculate_weight(transformation)
    #     weighted.append((w, transformation))
    #
    # max_value = 0
    # for (w, _) in weighted:
    #     if w > max_value:
    #         max_value = w
    #
    # count = 0
    # for (w, _) in weighted:
    #     if w == max_value:
    #         count += 1

    return transformations


def calculate_transformation(ordered_objects_from_a, ordered_objects_from_b):
    object_transformations = []
    length = len(ordered_objects_from_a)

    # Rename objects
    object_names = generate_object_names()
    for i in range(length):
        object_in_a = ordered_objects_from_a[i]
        object_in_b = ordered_objects_from_b[i]
        name = next(object_names)

        if object_in_a is not False:
            rename_object_reference_in_object_list(ordered_objects_from_a, object_in_a.name(), name)

        if object_in_b is not False:
            rename_object_reference_in_object_list(ordered_objects_from_b, object_in_b.name(), name)

    for i in range(length):
        object_in_a = ordered_objects_from_a[i]
        object_in_b = ordered_objects_from_b[i]

        if object_in_a is False:
            o = add_object(object_in_b)
            object_transformations = add_one_to_list(object_transformations, o)
        elif object_in_b is False:
            o = delete_object(object_in_a)
            object_transformations = add_one_to_list(object_transformations, o)
        else:
            o_s = transform_object(object_in_a, object_in_b)
            object_transformations = add_choices_to_list(object_transformations, o_s)

    return object_transformations


def add_choices_to_list(list_of_transformations, choices):
    new_list = []
    if len(list_of_transformations) == 0:
        for c in choices:
            new_list.append([c])
    else:
        for c in choices:
            for t in list_of_transformations:
                combination = copy.deepcopy(t)
                combination.append(c)
                new_list.append(combination)
    return new_list


def add_one_to_list(list_of_transformations, transformation):
    if len(list_of_transformations) == 0:
        list_of_transformations.append([transformation])
    else:
        for t in list_of_transformations:
            t.append(transformation)

    return list_of_transformations


def has_angle_attributes(objects_in_a, objects_in_b):
    length = len(objects_in_a)
    for i in range(length):
        object_in_a = objects_in_a[i]
        object_in_b = objects_in_b[i]
        if has_angle_attribute(object_in_a, object_in_b):
            return True
    return False


def has_angle_attribute(object_in_a, object_in_b):
    if object_in_a is False:
        pass
    elif object_in_b is False:
        pass
    else:
        attributes_in_a = convert_attributes_to_dictionary(object_in_a.slots())
        attributes_in_b = convert_attributes_to_dictionary(object_in_b.slots())

        angle_attr = "angle"

        if angle_attr in attributes_in_a and angle_attr in attributes_in_b:
            return True
        else:
            return False


def has_reflection(object_in_a, object_in_b):
    if object_in_a is False:
        pass
    elif object_in_b is False:
        pass
    else:
        attributes_in_a = convert_attributes_to_dictionary(object_in_a.slots())
        attributes_in_b = convert_attributes_to_dictionary(object_in_b.slots())

        angle_attr = "angle"
        shape_attr = "shape"
        shape = attributes_in_a[shape_attr]

        if angle_attr in attributes_in_a and angle_attr in attributes_in_b:
            from_angle = attributes_in_a[angle_attr]
            to_angle = attributes_in_b[angle_attr]
            return (has_horizontal_reflection(shape, from_angle, to_angle),
                    has_vertical_reflection(shape, from_angle, to_angle))

    return False, False


def calculate_angle_diff(a, b):
    attributes_in_a = convert_attributes_to_dictionary(a.slots())
    attributes_in_b = convert_attributes_to_dictionary(b.slots())

    angle_attr = "angle"

    from_angle = int(attributes_in_a[angle_attr])
    to_angle = int(attributes_in_b[angle_attr])

    return to_angle - from_angle


def has_horizontal_reflection(shape, from_angle, to_angle):
    # Add more shapes here
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if from_angle == "0" and to_angle == "0":
            return True
        if from_angle == "90" and to_angle == "270":
            return True
        if from_angle == "180" and to_angle == "180":
            return True
        if from_angle == "270" and to_angle == "90":
            return True
        return False
    else:
        if from_angle == "0" and to_angle == "180":
            return True
        if from_angle == "90" and to_angle == "90":
            return True
        if from_angle == "180" and to_angle == "0":
            return True
        if from_angle == "270" and to_angle == "270":
            return True
        return False


def has_vertical_reflection(shape, from_angle, to_angle):
    # Add more shapes here
    if shape in ["Pac-Man", "arrow", "half-arrow"]:
        if from_angle == "0" and to_angle == "180":
            return True
        if from_angle == "90" and to_angle == "90":
            return True
        if from_angle == "180" and to_angle == "0":
            return True
        if from_angle == "270" and to_angle == "270":
            return True
        return False
    else:
        if from_angle == "0" and to_angle == "0":
            return True
        if from_angle == "90" and to_angle == "270":
            return True
        if from_angle == "180" and to_angle == "180":
            return True
        if from_angle == "270" and to_angle == "90":
            return True
        return False


def generate_reflections(transformation, objects_in_a, objects_in_b):

    transformations = []
    t = copy.deepcopy(transformation)

    length = len(t.object_transformations())

    for i in range(length):
        a = objects_in_a[i]
        b = objects_in_b[i]
        hor, ver = has_reflection(a, b)

        if hor or ver:
            trans = t.object_transformations()[i]
            if hor:
                trans.set_horizontal_reflection()
            if ver:
                trans.set_vertical_reflection()

            to_remove = SlotTransformation("", "", "")
            for attr in trans.slot_transformations():
                if attr.name() == "angle":
                    to_remove = attr

            if to_remove.name() == "angle":
                trans.remove_slot_transformation(to_remove)

            to_remove_att = Slot("", "")
            for attr in trans.required():
                if attr.name() == "angle":
                    to_remove_att = attr

            if to_remove_att.name() == "angle":
                trans.remove_required_attribute(to_remove_att)

    transformations.append(t)
    return transformations


def remove_attribute(transformation, attribute):
    to_remove = SlotTransformation("", "", "")
    for attr in transformation.slot_transformations():
        if attr.name() == attribute:
            to_remove = attr
    if to_remove.name() == attribute:
        transformation.remove_slot_transformation(to_remove)
    to_remove_att = Slot("", "")
    for attr in transformation.required():
        if attr.name() == attribute:
            to_remove_att = attr
    if to_remove_att.name() == attribute:
        transformation.remove_required_attribute(to_remove_att)


def generate_reflection(transformation, a, b):
    t = copy.deepcopy(transformation)
    hor, ver = has_reflection(a, b)

    if hor:
        t.set_horizontal_reflection()
    if ver:
        t.set_vertical_reflection()

    return t


def generate_rotations(transformation, a, b, shape):
    result = []
    t1 = copy.deepcopy(transformation)
    t2 = copy.deepcopy(transformation)
    diff = calculate_angle_diff(a, b)

    # Rotate both ways
    d1 = diff % 360
    t1.set_rotation(d1)
    result.append(t1)

    if shape not in ["triangle"]:
        d2 = (-diff) % 360
        t2.set_rotation(d2)
        result.append(t2)

    return result


def add_object(obj):
    attr = []
    for a in obj.slots():
        a_t = SlotTransformation(a.name(), "", a.filler())
        attr.append(a_t)
    return ObjectTransformation(obj.name(), False, True, [], attr, [], [])


def delete_object(obj):
    return ObjectTransformation(obj.name(), True, False, [], [], [], [])


def transform_object(object_in_a, object_in_b):
    attribute_transformations = []

    shape = ""

    requirements = []
    for attribute_a in object_in_a.slots():
        if attribute_a.name() == "shape":
            shape = attribute_a.filler()
        else:
            requirements.append(attribute_a)

    for attribute_a in object_in_a.slots():
        for attribute_b in object_in_b.slots():
            if attribute_a.name() == attribute_b.name():
                if attribute_a.filler() != attribute_b.filler():
                    if attribute_a.filler() == "no" and "," in attribute_b.filler():
                        at = SlotTransformation(attribute_a.name(),
                                                     "",
                                                     attribute_b.filler())
                        at.set_append_value()
                    else:
                        at = SlotTransformation(attribute_a.name(),
                                                     attribute_a.filler(),
                                                     attribute_b.filler())
                    attribute_transformations.append(at)
    attributes_to_add = []
    for attribute_b in object_in_b.slots():
        found = False
        for attribute_a in object_in_a.slots():
            if attribute_a.name() == attribute_b.name():
                found = True

        if not found:
            at = SlotTransformation(attribute_b.name(),
                                         "",
                                         attribute_b.filler())
            attributes_to_add.append(at)
    attributes_to_remove = []
    for attribute_a in object_in_a.slots():
        found = False
        for attribute_b in object_in_b.slots():
            if attribute_a.name() == attribute_b.name():
                found = True

        if not found:
            attributes_to_remove.append(attribute_a.name())

    o = ObjectTransformation(object_in_a.name(), False, False, attribute_transformations, attributes_to_add,
                             attributes_to_remove, requirements)

    trans = []

    if has_angle_attribute(object_in_a, object_in_b):
        remove_attribute(o, "angle")
        hor, ver = has_reflection(object_in_a, object_in_b)

        if hor or ver:
            r = generate_reflection(o, object_in_a, object_in_b)
            trans.append(r)

        ts = generate_rotations(o, object_in_a, object_in_b, shape)
        for t in ts:
            trans.append(t)
    else:
        trans.append(o)

    return trans